import java.util.PriorityQueue;

/**
 * Created by L.jp
 * Description:
 * User: 86189
 * Date: 2022-07-12
 * Time: 11:28
 */
//使用优先级队列
public class Solution2 {
    public int findKth(int[] a, int n, int K) {
        PriorityQueue<Integer> pq = new PriorityQueue<>();
        //遍历数组，维护大小为k的小根堆，当堆的大小大于k时，如果元素大于堆顶的值，就加入堆，弹出堆顶元素，最后返回堆顶元素即是第k大
        for(int i = 0; i < n; i++){
            if(pq.size()<K){
                pq.add( a[i] );
            }else{
                if(pq.peek()<a[i]){
                    pq.add( a[i] );
                    pq.poll();
                }
            }
        }
        return pq.peek();
    }
}
