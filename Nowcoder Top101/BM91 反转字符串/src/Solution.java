/**
 * Created by L.jp
 * Description:写出一个程序，接受一个字符串，然后输出该字符串反转后的字符串。
 * User: 86189
 * Date: 2022-09-28
 * Time: 22:52
 */
public class Solution {
    public String solve (String str) {
        //左右双指针
        char[] s = str.toCharArray();
        int left = 0;
        int right = str.length() - 1;
        while(left < right){ //两指针往中间靠
            char c = s[left];
            s[left] = s[right];
            s[right] = c;
            left++;
            right--;
        }
        return new String(s);
    }
}
