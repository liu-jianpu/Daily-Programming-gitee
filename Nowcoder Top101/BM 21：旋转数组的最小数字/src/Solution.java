/**
 * Created by L.jp
 * Description:有一个长度为 n 的非降序数组，比如[1,2,3,4,5]，
 * 将它进行旋转，即把一个数组最开始的若干个元素搬到数组的末尾，变成一个旋转数组，
 * 比如变成了[3,4,5,1,2]，或者[4,5,1,2,3]这样的。请问，给定这样一个旋转数组，求数组中的最小值。
 * User: 86189
 * Date: 2022-05-29
 * Time: 10:23
 */
public class Solution {
    public static int minNumberInRotateArray(int [] array) {
        int left=0;
        int right=array.length - 1;
        while ( left<right ){
            int mid=(left+right)/2;
            //如果旋转后数组中间节点的值大于右边界的值，那么最小值一定在mid右边
            if(array[mid]>array[right]){
                left=mid+1;
            }else if(array[mid]<array[right]){
                //如果中间的值小于右边的值，那么最小值一定在mid左边
                right=mid;
            }else{
                //如果中间的值和右边的值相等，那么不确定最小值在哪边，需要缩小边界
                right--;
            }
            
        }
        return array[left];
    }
    
    public static void main ( String[] args ) {
        int[] array={4,5,1,2,3};
        System.out.println( minNumberInRotateArray( array ) );
    }
}
