/**
 * Created by L.jp
 * Description:给定一个长度为 n 的字符串，请编写一个函数判断该字符串是否回文。如果是回文请返回true，否则返回false。
 *
 * 字符串回文指该字符串正序与其逆序逐字符一致。
 * User: 86189
 * Date: 2022-09-27
 * Time: 23:47
 */
public class Solution {
    public boolean judge (String str) {
        int left = 0; //⾸
        int right = str.length() - 1; //尾
        while(left < right){ //⾸尾往中间靠
            if(str.charAt(left) != str.charAt(right)) //⽐较前后是否相同
                return false;
            left++;
            right--;
        }
        return true;
    }
}
