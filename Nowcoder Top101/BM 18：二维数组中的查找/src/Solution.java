/**
 * Created by L.jp
 * Description:在一个二维数组array中（每个一维数组的长度相同），
 * 每一行都按照从左到右递增的顺序排序，每一列都按照从上到下递增的顺序排序。
 * 请完成一个函数，输入这样的一个二维数组和一个整数，判断数组中是否含有该整数。
 * [
 * [1,2,8,9],
 * [2,4,9,12],
 * [4,7,10,13],
 * [6,8,11,15]
 * ]
 * 给定 target = 7，返回 true。
 *
 * 给定 target = 3，返回 false。
 * User: 86189
 * Date: 2022-05-25
 * Time: 10:59
 */
public class Solution {
    public static boolean Find(int target, int [][] array) {
        int i=array.length-1;
        int j=0;
        //从左下角开始遍历
        while (i>=0 && j<array[0].length){
            if(target>array[i][j]){
                j++;
            }else if(target<array[i][j]){
                i--;
            }else{
                return true;
            }
        }
        return false;
    }
    
    public static void main(String[] args) {
        int[][] array={{1,2,8,9},{2,4,9,12},{4,7,10,13},{6,8,11,15}};
        int target=0;
        System.out.println(Find(target, array));
    }
}
