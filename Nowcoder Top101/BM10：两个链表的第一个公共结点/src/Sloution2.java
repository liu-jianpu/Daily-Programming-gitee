/**
 * Created by L.jp
 * Description:
 * User: 86189
 * Date: 2022-05-17
 * Time: 9:40
 */
class ListNode {
    int val;
    ListNode next = null;
    
    ListNode(int val) {
        this.val = val;
    }
}
public class Sloution2 {
    public ListNode FindFirstCommonNode(ListNode pHead1, ListNode pHead2) {
        /*法二:使用双指针同时遍历两个链表，当短的链表遍历完之后，就从另一个链表的头结点开始遍历
        * 长的链表遍历完之后就短的链表头开始遍历，之后他们相遇的第一个点就是公共节点*/
        ListNode cur1=pHead1;
        ListNode cur2=pHead2;
        while (cur1!=cur2){
            cur1=cur1==null ? pHead2:cur1.next;
            cur2=cur2==null ? pHead1:cur2.next;
        }
        return cur1;
    }
}
