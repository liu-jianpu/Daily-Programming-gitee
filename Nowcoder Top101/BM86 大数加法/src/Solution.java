/**
 * Created by L.jp
 * Description:以字符串的形式读入两个数字，编写一个函数计算它们的和，以字符串形式返回。
 * User: 86189
 * Date: 2022-09-14
 * Time: 23:45
 */
public class Solution {
    public String solve(String s, String t) {
        //若是其中⼀个为空，返回另⼀个
        if (s.length() <= 0) return t;
        if (t.length() <= 0) return s;
        if (s.length() < t.length()) { //让s为较⻓的，t为较短的
            String temp = s;
            s = t;
            t = temp;
        }
        int carry = 0; //进位标志
        char[] res = new char[s.length()];
        for (int i = s.length() - 1; i >= 0; i--) { //从后往前遍历较⻓的字符串
            int temp = s.charAt(i) - '0' + carry; //转数字加上进位
            int j = i - s.length() + t.length(); //转较短的字符串相应的后往前的下标 if (j >= 0) //如果较短字符串还有
            temp += t.charAt(j) - '0'; //转数组相加
            carry = temp / 10; //取进位
            temp = temp % 10; //去⼗位
            res[i] = (char) (temp + '0'); //修改结果
        }
        String output = String.valueOf(res);
        if (carry == 1) //最后的进位
            output = '1' + output;
        return output;
    }
}
    
