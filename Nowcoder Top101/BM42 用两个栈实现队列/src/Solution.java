import java.util.Stack;

/**
 * Created by L.jp
 * Description:用两个栈来实现一个队列，使用n个元素来完成 n 次在队列尾部插入整数(push)和n次在队列头部删除整数(pop)的功能。 队列中的元素为int类型。保证操作合法，即保证pop操作时队列内已有元素。
 *
 * 数据范围： n\le1000n≤1000
 * 要求：存储n个元素的空间复杂度为 O(n) ，插入与删除的时间复杂度都是 O(1)
 * User: 86189
 * Date: 2022-07-02
 * Time: 11:14
 */
//思路就是最先进栈的要变成最先出来，显然一个栈是不可以的，但是我们可以借助第二个栈，
// 把栈1的里面的元素全部弹出，加到栈2，然后弹出栈2的元素就实现了先进先出
public class Solution {
    Stack<Integer> stack1 = new Stack<Integer>();
    Stack<Integer> stack2 = new Stack<Integer>();
    
    public void push(int node) {
        stack1.push(node);
    }
    
    public int pop() {
        //如果栈2为空就把栈1的元素全部添加进来
        if(stack2.isEmpty()){
            while (!stack1.isEmpty()) {
                stack2.push( stack1.pop() );
            }
        }
        return stack2.pop();
    }
}
