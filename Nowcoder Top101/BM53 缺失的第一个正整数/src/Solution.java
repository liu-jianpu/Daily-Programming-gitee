import java.util.HashMap;

/**
 * Created by L.jp
 * Description:给定一个未排序的整数数组nums，请你找出其中没有出现的最小的正整数
 *
 * 进阶： 空间复杂度 O(1)，时间复杂度 O(n)
 * User: 86189
 * Date: 2022-07-20
 * Time: 23:53
 */
//这些正整数最小就是1，我们只需找出排序后没有出现的正整数
public class Solution {
    public static int minNumberDisappeared (int[] nums) {
        HashMap<Integer,Integer> map=new HashMap<>();
        for(int i=0;i<nums.length;i++){
            map.put(nums[i],1);
        }
        //从最小的1开始找
        int res=1;
        while (map.containsKey(res)){
            res++;
        }
        return res;
    }
    
    public static void main(String[] args) {
        int[] num={-2,2,3,5,6,7};
        System.out.println(minNumberDisappeared(num));
    }
}
