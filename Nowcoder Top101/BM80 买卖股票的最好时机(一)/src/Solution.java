/**
 * Created by L.jp
 * Description:假设你有一个数组prices，长度为n，其中prices[i]是股票在第i天的价格，
 * 请根据这个价格数组，返回买卖股票能获得的最大收益
 * 1.你可以买入一次股票和卖出一次股票，并非每天都可以买入或卖出一次，
 * 总共只能买入和卖出一次，且买入必须在卖出的前面的某一天
 * 2.如果不能获取到任何利润，请返回0
 * 3.假设买入卖出均无手续费
 * User: 86189
 * Date: 2022-09-05
 * Time: 10:45
 */
public class Solution {
    public int maxProfit (int[] prices) {
        int n= prices.length;
        //像这种买卖股票只能一次的情况，其实要使收益最大，那就是每次使用当前值减去最小值即可，不断更新最小值，那么收益也会不断变化
        int min=prices[0];
        int max=0;
        for(int i=0;i<n;i++){
            min=Math.min(min,prices[i]);
            //找到当前值与最小值的差值最大的即可
            max=Math.max(max,prices[i]-min);
        }
        return max;
    }
}
