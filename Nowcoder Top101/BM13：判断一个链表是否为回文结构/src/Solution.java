import java.util.ArrayList;

/**
 * Created by L.jp
 * Description:给定一个链表，请判断该链表是否为回文结构。
 * 回文是指该字符串正序逆序完全一致。
 * User: 86189
 * Date: 2022-05-23
 * Time: 9:55
 */
class ListNode {
    int val;
    ListNode next = null;
    public ListNode(int val){
        this.val=val;
    }
  }
public class Solution {
    public boolean isPail (ListNode head) {
        //方法一：数组复制，双指针
        /*  时间复杂度：O(N),n为链表的长度，遍历链表转化数组为O(n),双指针遍历半个数组为O(n)
            空间复杂度：O(n),记录链表元素的辅助数组
         */
        ArrayList<Integer> arrayList=new ArrayList<>();
        while (head!=null){
            arrayList.add(head.val);
            head=head.next;
        }
        int left=0;
        int right = arrayList.size()-1;
        while (left<=right){
            int x=arrayList.get(left);
            int y=arrayList.get(right);
            if(x!=y){
                return false;
            }
            left++;
            right--;
        }
        return true;
    }

}
