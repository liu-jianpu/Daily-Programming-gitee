/**
 * Created by L.jp
 * Description:给定一个整数数组 cost \cost  ，其中 cost[i]\cost[i]  是从楼梯第i \i 个台阶向上爬需要支付的费用，下标从0开始。一旦你支付此费用，即可选择向上爬一个或者两个台阶。
 *
 * 你可以选择从下标为 0 或下标为 1 的台阶开始爬楼梯。
 *
 * 请你计算并返回达到楼梯顶部的最低花费。
 * User: 86189
 * Date: 2022-08-25
 * Time: 23:18
 */
public class Solution {
    public int minCostClimbingStairs (int[] cost) {
        int n =cost.length;
        //定义，dp表示爬到第i层所需的最底费用，初始值下标0和1的都为0元
        int[] dp=new int[n+1];
        //从下标为2的层数开始计算费用
        for(int i=2;i<=n;i++){
            //cost[i]表示第i层向上爬一层或者两层需要的费用
            //dp[i]表示爬到第i层的最低费用
            //选取最小的方案
            dp[i]=Math.min(cost[i-1]+dp[i-1],cost[i-2]+dp[i-2]);
        }
        //要注意的是跳到n的位置，就是下标当于数组长度的时候
        return dp[n];
    }
}
