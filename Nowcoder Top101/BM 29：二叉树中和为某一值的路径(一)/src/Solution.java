/**
 * Created by L.jp
 * Description:给定一个二叉树root和一个值 sum ，判断是否有从根节点到叶子节点的节点值之和等于 sum 的路径。
 * 1.该题路径定义为从树的根结点开始往下一直到叶子结点所经过的结点
 * 2.叶子节点是指没有子节点的节点
 * 3.路径只能从父节点到子节点，不能从子节点到父节点
 * 4.总节点数目为n
 * User: 86189
 * Date: 2022-06-05
 * Time: 23:38
 */
class TreeNode {
    int val = 0;
    TreeNode left = null;
    TreeNode right = null;
  }
/**
 * @author 86189
 */
public class Solution {
    public boolean hasPathSum (TreeNode root, int sum) {
        if(root==null){
            return false;
        }
        //到了子节点的话，sum值应该是减到等于了叶子节点的值，那么就找到了路径，返回true
        if(root.left==null && root.right == null && sum==root.val){
            return true;
        }
        return hasPathSum( root.left, sum-root.val)
                || hasPathSum( root.right,sum- root.val);
        
    }

}
