/**
 * Created by L.jp
 * Description:有一个NxN整数矩阵，请编写一个算法，将矩阵顺时针旋转90度。
 *
 * 给定一个NxN的矩阵，和矩阵的阶数N,请返回旋转后的NxN矩阵。
 * User: 86189
 * Date: 2022-11-02
 * Time: 23:52
 */
public class Solution {
    public int[][] rotateMatrix(int[][] mat, int n) {
        int length = mat.length;
        //矩阵转置
        for(int i = 0; i < length; ++i){
            for(int j = 0; j < i; ++j){
                //交换上三角与下三角对应的元素
                int temp = mat[i][j];
                mat[i][j] = mat[j][i];
                mat[j][i] = temp;
            }
        }
        //每行翻转
        for (int i = 0; i < length; i++) {
            for (int j = 0; j < length/2; j++){
                int temp = mat[i][j];
                mat[i][j] = mat[i][length - j - 1];
                mat[i][length - j - 1] = temp;
            }
        }
        return mat;
    }
}
