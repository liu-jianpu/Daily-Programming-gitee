import java.util.Stack;

/**
 * Created by L.jp
 * Description:定义栈的数据结构，请在该类型中实现一个能够得到栈中所含最小元素的 min 函数，
 * 输入操作时保证 pop、top 和 min 函数操作时，栈中一定有元素。
 *
 * 此栈包含的方法有：
 * push(value):将value压入栈中
 * pop():弹出栈顶元素
 * top():获取栈顶元素
 * min():获取栈中最小元素
 * User: 86189
 * Date: 2022-07-02
 * Time: 13:17
 */
//1.使用两个栈，一个专门存放最小值的，一个用来push,top,pop()
    //2.入栈时对于两个栈都要操作，普通的那个栈，就进行正常的push,对于最小值的栈，如果有元素入栈时最小值的栈为空，那么加让元素入栈1和栈2
    //如果元素大于栈2的栈顶元素，那个就重复把栈2的栈顶元素入栈，如果元素小于栈2的栈顶元素，那么把元素入栈1和栈2
    //对于pop函数，栈1要pop,栈2也得pop,因为有的元素是重复入了
    //对于top,就是栈1的peek
    
public class Solution {
    Stack<Integer> commonStack=new Stack<>();
    Stack<Integer> minStack = new Stack<>();
    public void push(int node) {
        commonStack.push(node);
        if(minStack.isEmpty() || node<minStack.peek()){
            minStack.push(node);
        }else{
            //要反复加入栈2顶元素的目的是始终保证栈顶元素是最小的，也就说最小元素是实时更新的，
            // 如果栈1有元素被弹走了，如果恰好是最小值，那么这时候得到的最小值就不正确了
            //也就是说这个操作的目的是使得min和pop是同步的，只有这样才能得到准确的值
            //如果弹出的元素不是最小值，那么就不会影响最小值求解，如果弹出的是最小值，那么对应另一个栈
            //就不能该元素了，又因为弹出只能从栈顶弹，所以必须要重复加入最小值
            minStack.push( minStack.peek() );
        }
    }
    public void pop() {
        commonStack.pop();
        minStack.pop();
    
    }
    
    public int top() {
        return  commonStack.peek();
    }
    public int min() {
        return minStack.peek();
    }
}
