/**
 * Created by L.jp
 * Description:假设你有一个数组prices，长度为n，其中prices[i]是某只股票在第i天的价格，请根据这个价格数组，返回买卖股票能获得的最大收益
 * 1. 你可以多次买卖该只股票，但是再次购买前必须卖出之前的股票
 * 2. 如果不能获取收益，请返回0
 * 3. 假设买入卖出均无手续费
 * User: 86189
 * Date: 2022-09-05
 * Time: 17:01
 */
public class Solution {
    //贪心算法
    //与买卖股票最好时机（一）的区别就是可以多次买入卖出
    //所以贪心的思想就是只要是数组时递增的，那么最大收益就是相邻的差值累加
    public int maxProfit (int[] prices) {
        int n=prices.length;
        int ret=0;
        for(int i=0;i<n;i++){
            if(prices[i+1]>prices[i]){
                ret+=prices[i+1]-prices[i];
            }
        }
        return ret;
    }
}
