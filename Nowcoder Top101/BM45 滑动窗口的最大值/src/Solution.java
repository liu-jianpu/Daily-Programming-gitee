import java.util.ArrayList;
import java.util.Comparator;
import java.util.PriorityQueue;
/**
 * Created by L.jp
 * Description:给定一个长度为 n 的数组 nums 和滑动窗口的大小 size ，
 * 找出所有滑动窗口里数值的最大值。
 * 例如，如果输入数组{2,3,4,2,6,2,5,1}及滑动窗口的大小3，
 * 那么一共存在6个滑动窗口，他们的最大值分别为{4,4,6,6,6,5}；
 * 针对数组{2,3,4,2,6,2,5,1}的滑动窗口有以下6个：
 * {[2,3,4],2,6,2,5,1}，
 * {2,[3,4,2],6,2,5,1}，
 * {2,3,[4,2,6],2,5,1}，
 * {2,3,4,[2,6,2],5,1}，
 * {2,3,4,2,[6,2,5],1}，
 * {2,3,4,2,6,[2,5,1]}。
 * User: 86189
 * Date: 2022-07-03
 * Time: 11:05
 */
public class Solution {
    //方法可以这么做，但是有的平台会超时,因为堆的删除效率太低了，是O(N)
    public ArrayList<Integer> maxInWindows(int [] num, int size) {
        if(size> num.length || size==0){
            return new ArrayList<>();
        }
        PriorityQueue<Integer> pq = new PriorityQueue<Integer>( new Comparator<Integer>() {
            @Override
            public int compare (Integer o1 , Integer o2) {
                return o2-o1;
            }
        });
        ArrayList<Integer> list=new ArrayList<>();
        int start=0;
        int end=size;
        //固定一个窗口，把元素加入堆
        for(int i = 0; i < size; i++){
            pq.add( num[i] );
        }
        //把窗口内的最大值放入数组，并移动窗口到下一个
        while ( end< num.length ){
            //不能弹出，然后又删除，这样的话堆的元素个数就不对了
            list.add( pq.peek());
            pq.remove(num[start++]);
            pq.add( num[end++] );
        }
        list.add( pq. peek());
        return list;
    }
}
