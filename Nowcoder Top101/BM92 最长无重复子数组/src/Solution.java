import java.util.HashMap;

/**
 * Created by L.jp
 * Description:给定一个长度为n的数组arr，返回arr的最长无重复元素子数组的长度，无重复指的是所有数字都不相同。
 * 子数组是连续的，比如[1,3,5,7,9]的子数组有[1,3]，[3,5,7]等等，但是[1,3,7]不是子数组
 * User: 86189
 * Date: 2022-10-01
 * Time: 23:47
 */
public class Solution {
    public int maxLength (int[] arr) {
        HashMap<Integer, Integer> mp = new HashMap<>(); //哈希表记录窗口内非重复的数字、
        int res = 0;
        for(int left = 0, right = 0; right < arr.length; right++){ //设置窗⼝左右边界
            if(mp.containsKey(arr[right])){
                mp.put(arr[right],mp.get(arr[right])+1); //窗⼝右移进⼊哈希表统计出现次数
            }else{
                mp.put(arr[right],1);
            }
            while(mp.get(arr[right]) > 1) //出现次数⼤于1，则窗⼝内有重复
                mp.put(arr[left],mp.get(arr[left++])-1); //窗⼝左移，同时减去该数字的出现次数
                    res = Math.max(res, right - left + 1); //维护⼦数组⻓度最⼤值
        }
        return res;
    }
}
