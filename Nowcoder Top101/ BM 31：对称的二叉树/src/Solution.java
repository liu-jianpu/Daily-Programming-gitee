/**
 * Created by L.jp
 * Description:给定一棵二叉树，判断其是否是自身的镜像（即：是否对称）
 * User: 86189
 * Date: 2022-06-19
 * Time: 21:34
 */
class TreeNode {
    int val = 0;
    TreeNode left = null;
    TreeNode right = null;
    
    public TreeNode(int val) {
        this.val = val;
        
    }
}
public class Solution {
    //递归
    boolean isSymmetrical(TreeNode pRoot) {
        if(pRoot==null){
            return true;
        }
        return isSymmetricalHelper(pRoot.left,pRoot.right);
    }
    //两个节点从左右两边开始遍历
    private boolean isSymmetricalHelper(TreeNode left, TreeNode right) {
        if(left==null && right==null){
            return true;
        }
        if(left==null || right==null){
            return false;
        }
        //左边节点的左孩子节点和右边节点的右孩子值相等，还有左边节点的右孩子节点等于右边节点的左孩子节点相等
        return left.val== right.val && isSymmetricalHelper(left.left,right.right)
                && isSymmetricalHelper(left.right,right.left);
    }
}
