import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by L.jp
 * Description:给一个长度为n链表，若其中包含环，请找出该链表的环的入口结点，否则，返回null。
 * User: 86189
 * Date: 2022-05-15
 * Time: 11:41
 */
class ListNode {
    int val;
    ListNode next = null;
    
    ListNode(int val) {
        this.val = val;
    }
}
public class Solution {
    public ListNode EntryNodeOfLoop(ListNode pHead) {
        //时间复杂度为O(N),空间复杂度为O(1)的做法
//        if(pHead == null){
//            return  pHead;  //为空则返回空，只有一个节点就是没有环，那么就返回null
//        }
//        ListNode fast=pHead;
//        ListNode slow=pHead;
//        while (fast!=null && fast.next != null){
//            slow=slow.next;
//            fast = fast.next.next;
//            if(slow==fast){
//                fast=pHead;
//                while (slow!=fast){
//                    fast=fast.next;
//                    slow=slow.next;
//                }
//                return slow;
//            }
//        }
//        return null;
        
        //时间复杂度为O(n）空间复杂度为O(n）的做法
        //使用哈希表存储链表每个节点，如果出现了重复的节点，那么第一次出现的就是入口节点
        Set<ListNode> set=new HashSet<>();
        ListNode cur=pHead;
        while (cur != null){
            if(!set.contains(cur)){
                set.add(cur);
            }else{
                return cur;
            }
            cur=cur.next;
        }
        return null;
    }
}
