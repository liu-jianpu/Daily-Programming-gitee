import javax.swing.tree.TreeNode;

/**
 * Created by L.jp
 * Description:输入一棵二叉搜索树，将该二叉搜索树转换成一个排序的双向链表。
 * User: 86189
 * Date: 2022-06-18
 * Time: 23:34
 */
//class TreeNode {
//    int val = 0;
//    TreeNode left = null;
//    TreeNode right = null;
//
//    public TreeNode(int val) {
//        this.val = val;
//
//    }
//
//}
//public class Solution {
//    //定义链表的头结点，也要借助一个前驱节点，用于链接链表
//    TreeNode head = null;
//    TreeNode pre = null;
//
//    public TreeNode Convert(TreeNode pRootOfTree) {
//        /*二叉搜索树的升序排列结果可以使用二叉树的中序遍历来实现，我们先使用递归来实现*/
//        if (pRootOfTree == null) {
//            return null;
//        }
//        //先递归找到最小的就是双链表的头结点
//        Convert(pRootOfTree.left);
//        //如果是第一次拼接,那么当前节点就是头结点也是前驱节点
//        if(pre==null){
//            head=pRootOfTree;
//            pre=pRootOfTree;
//        }else{
//            //不是第一次拼接，那么就要拼接左右指针的节点
//            pre.right=pRootOfTree; //后继节点
//            pRootOfTree.left=pre; //前驱节点
//            pre=pRootOfTree; //让前驱节点指向头节点
//        }
//        Convert(pRootOfTree.right);
//        return head;
//    }
//}
