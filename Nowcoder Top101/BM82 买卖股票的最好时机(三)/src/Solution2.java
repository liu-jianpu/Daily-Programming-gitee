/**
 * Created by L.jp
 * Description:
 * User: 86189
 * Date: 2022-09-05
 * Time: 20:32
 */
public class Solution2 {
    public int maxProfit (int[] prices) {
        //可以对动态数组进行优化，因为当前的收益只和前面一天的收益有关，所以只需要利用4个变量代替即可
        int  n=prices.length;
        //第一次买入后的最大收益
        int buy1=-prices[0];
        //第一次卖出后的最大收益
        int sell1=0;  //-prices[0]+prices[0];
        //第二次买入后的最大收益
        int buy2=-prices[0];
        //第二次卖出后的最大收益
        int sell2=0; //-prices[0]+prices[0];
        for(int i=1;i<n;i++){
            //求前i天的第一次买入的收益，可能是在前面已经买入了，或者是前面没有买入当天刚刚买入，两者收益取最大值
            buy1=Math.max(buy1,-prices[i]);
            //求前i天第一次卖出的收益，可能是前面已经买出了，或者是前面没有卖出只有第一次买入然后当天第一次卖出，两者取最大值
            sell1=Math.max(sell1,buy1+prices[i]);
            //求前i天的第二次买入的收益，可能是在前面已经买入了，或者是前面没有买入当天刚刚买入，两者收益取最大值
            buy2=Math.max(buy2,sell1-prices[i]);
            //求前i天第二次卖出的收益，可能是前面已经买出了，或者是前面没有卖出只有第二次买入然后当天第二次卖出，两者取最大值
            sell2=Math.max(sell2,buy2+prices[i]);
        }
        return sell2;
    }

}
