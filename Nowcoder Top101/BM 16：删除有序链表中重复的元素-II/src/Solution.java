/**
 * Created by L.jp
 * Description:给出一个升序排序的链表，删除链表中的所有重复出现的元素，只保留原链表中只出现一次的元素。
 * 例如：
 * 给出的链表为1→2→3→3→4→4→5, 返回1→2→5.
 * 给出的链表为1→1→1→2→3, 返回2→3.
 * User: 86189
 * Date: 2022-05-24
 * Time: 11:45
 */
class ListNode {
    int val;
    ListNode next = null;
    public ListNode(int val){
        this.val=val;
    }
}
public class Solution {
    public ListNode deleteDuplicates (ListNode head) {
        if(head==null){
            return null;
        }
        //定义一个新的表头，防止开头几个都是重复的节点
        ListNode res=new ListNode(-1);
        res.next=head;
        //定义一个节点来判断当前节点和下一个节点的值是否相等
        ListNode cur=head;
        //定义一个前驱节点，用来连接好去掉的重复的节点，也就是连接第一个不重复的节点
        ListNode pre=res;
        while (cur!=null && cur.next!=null){
            //分为两种情况，一个是当前的节点和下一个节点相等，另一个是当前的节点和下一个节点不相等
            if(cur.val==cur.next.val){
                //当两个相邻的节点相等时，需要循环，找到第一个不重复的节点，把这些重复的都去掉
                while (cur.next!=null && cur.val==cur.next.val){
                    cur=cur.next;
                }
                //循环找到了第一个不重复的节点,就是当前节点的下一个节点，让前驱结点连接上他
                pre.next=cur.next;
                cur=cur.next;  //此时作为连接的前驱节点还没那么快跟上来，要等下一次判断到当前节点和下一个节点不相等时才能跟上来
            }else{
                pre=cur;
                cur=cur.next;
            }
        }
        return res.next;
    }
}
