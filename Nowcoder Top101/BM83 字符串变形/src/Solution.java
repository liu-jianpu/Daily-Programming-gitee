/**
 * Created by L.jp
 * Description:对于一个长度为 n 字符串，我们需要对它做一些变形。
 *
 * 首先这个字符串中包含着一些空格，就像"Hello World"一样，然后我们要做的是把这个字符串中由空格隔开的单词反序，同时反转每个字符的大小写。
 *
 * 比如"Hello World"变形后就变成了"wORLD hELLO"。
 * User: 86189
 * Date: 2022-09-09
 * Time: 23:01
 */
public class Solution {
        public static String trans(String s, int n) {
            char[] chs = s.toCharArray();
            //先逆置整个字符串
            reverse(chs,0,chs.length - 1);
            int i=0;
            while (i<chs.length){
                int j=i;
                while(j< chs.length && chs[j]!=' '){
                    if(chs[j]>='a' && chs[j]<='z'){
                        chs[j]-=32;
                    }else{
                        chs[j]+=32;
                    }
                    j++;
                }
                //遇到了空格就逆置整个单词
                //i在每一个单词原来的开头位置，j是空格的位置
                reverse(chs,i,j-1);
                //继续翻转下一个单词
                i=j+1;
            }
            return new String(chs);
        }
        public static void reverse(char[] a, int l, int r){
            while(l < r){
                char temp = a[l];
                a[l++] = a[r];
                a[r--] = temp;
            }
        }
    
    public static void main(String[] args) {
        String str="Hello World";
        System.out.println(trans(str, str.length()));
    }
}
