/**
 * Created by L.jp
 * Description:给你一个大小为 n 的字符串数组 strs ，其中包含n个字符串 ,
 * 编写一个函数来查找字符串数组中的最长公共前缀，返回这个公共前缀。
 * User: 86189
 * Date: 2022-09-12
 * Time: 21:19
 */
public class Solution {
    public String longestCommonPrefix (String[] strs) {
        //就拿着第一个字符串跟后面的字符串一个一个字符比较，使用stringbuilder拼接即可
        int n=strs.length;
        if(n==0){
            return "";
        }
        for(int i=0;i<strs[0].length();i++){
            char ch=strs[0].charAt(i);
            //从后面的字符串中开始找
            for(int j=1;j<n;j++){
                //要求找每一个字符串的公共字符串，那么两个字符串如果有一个字符不一样，那么就可以返回公共字符串了
                //i==strs[j].length()表示第一个字符串的0~i位置和后面的字符串相等，那么就可以返回了
                if(i==strs[j].length() || ch!=strs[j].charAt(i)){
                    return strs[0].substring(0,i);
                }
            }
        }
        //否则返回整个字符串
        return strs[0];
    }

}
