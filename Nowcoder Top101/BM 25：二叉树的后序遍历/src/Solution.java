//import java.util.ArrayList;
//
///**
// * Created by L.jp
// * Description:
// * User: 86189
// * Date: 2022-05-30
// * Time: 15:56
// */
//class TreeNode {
//    int val = 0;
//    TreeNode left = null;
//    TreeNode right = null;
//    public TreeNode(int val) {
//      this.val = val;
//    }
//  }
//public class Solution {
//    public void postOrder(TreeNode root, ArrayList<Integer> list){
//        if(root==null){
//            return;
//        }
//        postOrder( root.left,list );
//        postOrder( root.right,list );
//        list.add(  root.val);
//    }
//    public int[] postorderTraversal (TreeNode root) {
//        ArrayList<Integer> list=new ArrayList<>();
//        postOrder( root,list );
//        int[] ret=new int[list.size()];
//        for(int i=0;i < list.size(); i++){
//            ret[i] = list.get(i);
//        }
//        return ret;
//    }
//}
