import javax.swing.tree.TreeNode;
import java.util.ArrayList;

/**
 * Created by L.jp
 * Description:给你二叉树的根节点 root ,返回它节点值的前序遍历。
 * User: 86189
 * Date: 2022-05-30
 * Time: 10:41
 */
//class TreeNode {
//    int val = 0;
//    TreeNode left = null;
//    TreeNode right = null;
//    public TreeNode(int val) {
//      this.val = val;
//    }
//}
public class Solution {
    //递归
//    public void preOrder(TreeNode root, ArrayList<Integer> list){
//        if(root==null){
//            return;
//        }
//        list.add( root.val );
//        preOrder( root.left,list );
//        preOrder( root.right,list );
//    }
//    public int[] preorderTraversal (TreeNode root) {
//        if(root==null){
//            return new int[0];
//        }
//        ArrayList<Integer> list=new ArrayList<>();
//        preOrder( root,list );
//        int[] ret=new int[list.size()];
//        for(int i = 0; i < ret.length; i++){
//            ret[i] = list.get(i);
//        }
//        return ret;
//    }
}
