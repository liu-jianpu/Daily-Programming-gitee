/**
 * Created by L.jp
 * Description:操作给定的二叉树，将其变换为源二叉树的镜像。
 * User: 86189
 * Date: 2022-06-21
 * Time: 23:40
 */
class TreeNode {
    int val = 0;
    TreeNode left = null;
   TreeNode right = null;
    public TreeNode(int val) {
      this.val = val;
   }
  }
public class Solution {
    public TreeNode Mirror (TreeNode pRoot) {
        //递归，就是交换每个子树的左右节点
        if(pRoot==null){
            return null;
        }
        if(pRoot.left==null && pRoot.right==null){
            return pRoot;
        }
        TreeNode tmp=pRoot.left;
        pRoot.left=pRoot.right;
        pRoot.right=tmp;
        if(pRoot.left!=null)
            Mirror(pRoot.left);
        if(pRoot.right!=null)
            Mirror(pRoot.right);
        return pRoot;
    }
}
