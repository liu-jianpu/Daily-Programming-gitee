import java.util.Stack;
import java.util.TreeMap;

/**
 * Created by L.jp
 * Description:
 * User: 86189
 * Date: 2022-06-22
 * Time: 22:47
 */
public class Solution2 {
    public TreeNode Mirror (TreeNode pRoot) {
        //非递归，使用栈来解决
        if(pRoot==null){
            return null;
        }
        Stack<TreeNode> stack=new Stack<>();
        stack.push(pRoot);
        while (!stack.isEmpty()){
            TreeNode cur=stack.pop();
            //交换左右节点
            TreeNode tmp=cur.left;
            cur.left=cur.right;
            cur.right=tmp;
            if(cur.left!=null)
                stack.push(cur.left);
            if(cur.right!=null)
                stack.push(cur.right);
        }
        return pRoot;
    }
}
