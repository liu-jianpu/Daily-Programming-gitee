import javax.swing.plaf.IconUIResource;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by L.jp
 * Description:
 * User: 86189
 * Date: 2022-08-16
 * Time: 16:06
 */
//方法二：使用两种哈希表计数
public class Solution2 {
    public static int[] FindNumsAppearOnce (int[] array) {
        HashMap<Integer,Integer> ret=new HashMap<>();
        for (int j : array) {
            if (!ret.containsKey(j)) {
                ret.put(j, 1);
            } else {
                ret.put(j, ret.get(j) + 1);
            }
        }
        ArrayList<Integer> list=new ArrayList<>();
        for(int i=0;i<array.length;i++){
            if(ret.get(array[i])==1){
                list.add(array[i]);
            }
        }
        //调整大小顺序
        if(list.get(0)>list.get(1)) {
            return new int[]{list.get(1), list.get(0)};
        }else{
            return new int[]{list.get(0), list.get(1)};
        }
    }
    public static void main(String[] args) {
        int[] array={1,2,3,3,2,4};
        int[] result=FindNumsAppearOnce(array);
        for(int i:result){
            System.out.print(i+" ");
        }
    }
}
