/**
 * Created by L.jp
 * Description:一个整型数组里除了两个数字只出现一次，其他的数字都出现了两次。请写程序找出这两个只出现一次的数字。
 * User: 86189
 * Date: 2022-07-17
 * Time: 23:53
 */
public class Solution {
    public static int[] FindNumsAppearOnce(int[] array) {
       int tmp=0;
       int ret1=0;
       int ret2=0;
       /*
       先得到异或结果
       */
        for (int j : array) {
            tmp ^= j;
        }
       /*
       然后去找两个数出现不同的第一位k，然后按照这个位数k去给整个数组分组，让它异或每一个数，
        按照结果第一位是0还是1分组，分别让两个数去异或两个组的每一个数，最终就会得到两个不同的目标数
        */
        
       int k=1;
       //找到两个目标数第一个不同的二进制位即可
       while ((k&tmp)==0){  //二进制位相同就继续找
           k<<=1;
       }
       //分组异或得到两个目标数
        for (int j : array) {
            if ((k & j) == 0) {
                ret1 ^= j;
            } else {
                ret2 ^= j;
            }
        }
       if(ret1<ret2) {
           return new int[]{ret1, ret2};
       }else {
           return new int[]{ret2, ret1};
       }
    }
    public static void main(String[] args) {
        int[] array={1,2,3,3,2,4};
        int[] result=FindNumsAppearOnce(array);
        for(int i:result){
            System.out.print(i+" ");
        }
    }
}
