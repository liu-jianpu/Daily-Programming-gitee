/**
 * Created by L.jp
 * Description:给一个01矩阵，1代表是陆地，0代表海洋， 如果两个1相邻，那么这两个1属于同一个岛。我们只考虑上下左右为相邻。
 * 岛屿: 相邻陆地可以组成一个岛屿（相邻:上下左右） 判断岛屿个数。
 * 例如：
 * 输入
 * [
 * [1,1,0,0,0],
 * [0,1,0,1,1],
 * [0,0,0,1,1],
 * [0,0,0,0,0],
 * [0,0,1,1,1]
 * ]
 * 对应的输出为3
 * (注：存储的01数据其实是字符'0','1')
 * User: 86189
 * Date: 2022-08-17
 * Time: 11:47
 */
//使用回溯法的dfs,只要有一个1就可以把岛屿数量+1，访问完后把这个岛改为海洋，表示已经遍历过，然后再遍历它的四周
public class Solution {
    
    public  int solve (char[][] grid) {
        int n= grid.length;
        int m=grid[0].length;
        int count=0;
        for(int i=0;i<n;i++){
            for(int j=0;j<m;j++){
                if(grid[i][j]=='1'){
                    count++;
                    dfs(grid,i,j);
                }
            }
        }
        return count;
    }
    
    private void dfs(char[][] grid,int i,int j) {
        //先标记为遍历过，变为海洋
        grid[i][j]='0';
        int n = grid.length;
        int m = grid[0].length;
        //然后遍历这个点四周的点
        //如果四周有陆地那就继续遍历
        if(i-1>=0 && grid[i-1][j]=='1'){
            dfs(grid,i-1,j);
        }
        if(j+1<m && grid[i][j+1]=='1'){
            dfs(grid,i,j+1);
        }
        if(i+1<n && grid[i+1][j]=='1'){
            dfs(grid,i+1,j);
        }
        if(j-1 >=0 && grid[i-1][j]=='1'){
            dfs(grid,i-1,j);
        }
    }
}
