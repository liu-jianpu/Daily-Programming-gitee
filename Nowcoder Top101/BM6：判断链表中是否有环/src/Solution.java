/**
 * Created by L.jp
 * Description:判断给定的链表中是否有环。如果有环则返回true，否则返回false。
 * User: 86189
 * Date: 2022-05-14
 * Time: 23:04
 */
class ListNode {
      int val;
      ListNode next;
      ListNode(int x) {
          val = x;
          next = null;
      }
  }
public class Solution {
    public boolean hasCycle(ListNode head) {
        if (head == null) {
            return false;
        }
        ListNode fast=head;
        ListNode slow=head;
        while (fast != null && fast.next != null){
            fast = fast.next.next;
            slow=slow.next;
            if(slow==fast){
                return true;
            }
        }
        return false;
    }
}
