
/**
 * Created by L.jp
 * Description:输入一棵节点数为 n 二叉树，判断该二叉树是否是平衡二叉树。
 * 在这里，我们只需要考虑其平衡性，不需要考虑其是不是排序二叉树
 * 平衡二叉树（Balanced Binary Tree），具有以下性质：
 * 它是一棵空树或它的左右两个子树的高度差的绝对值不超过1，并且左右两个子树都是一棵平衡二叉树。
 * User: 86189
 * Date: 2022-06-26
 * Time: 11:56
 */
class  TreeNode{
    int val;
    TreeNode left=null;
    TreeNode right=null;
    public TreeNode(int val){
        this.val=val;
    }
}
public class Solution {
    public int getHeight(TreeNode head){
       if(head==null){
           return 0;
       }
       //有左右子树就可以统计左右子树的高度
       int leftHeight=getHeight(head.left);
       int rightHeight=getHeight(head.right);
       //左右子树的高度是本级的高度1+子树的高度
       return 1+(Math.max(leftHeight,rightHeight));
    }
    public boolean IsBalanced_Solution(TreeNode root) {
        if(root==null){
            return true;
        }
        //得到左右子树的高度，看看两边的高度是否符合平衡二叉树条件
        int leftHeight=getHeight(root.left);
        int rightHeight=getHeight(root.right);
        if(Math.abs(leftHeight-rightHeight)>1){
            return false;
        }
        return IsBalanced_Solution(root.left) && IsBalanced_Solution(root.right);
    }
}
