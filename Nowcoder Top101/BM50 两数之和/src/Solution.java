import com.sun.org.glassfish.gmbal.Impact;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by L.jp
 * Description:给出一个整型数组 numbers 和一个目标值 target，请在数组中找出两个加起来等于目标值的数的下标，返回的下标按升序排列。
 * （注：返回的数组下标从1开始算起，保证target一定可以由数组里面2个数字相加得到）
 * User: 86189
 * Date: 2022-07-15
 * Time: 23:41
 */
public class Solution {
    public int[] twoSum (int[] numbers, int target) {
//        int[] result = new int[0];
//        Map<Integer,Integer> map=new HashMap<Integer, Integer>();
//        for(int i = 0; i < numbers.length; i++){
//            int other=target-numbers[i];
//            //没有包含另一半就把对应的值放进去
//            if(!map.containsKey(other)){
//                map.put( numbers[i],i );
//            }else{
//                //包含了就把两个结果加入结果集,另一半一定是在前面出现过的，所以下标在前面
//                return new int[]{map.get(other)+1,i+1};
//            }
//        }
//        return result;
        int[] sum = new int[2];
        for (int i=0;i<numbers.length;i++){
            if (numbers[i] > target) continue;
            for (int j=i+1;j<numbers.length;j++){
                if (numbers[i]+numbers[j] == target){
                    sum[0] = i+1;
                    sum[1] = j+1;
                    return sum;
                }
            }
        }
        return sum;
    }
}
