/**
 * Created by L.jp
 * Description:求给定二叉树的最大深度，
 * 深度是指树的根节点到任一叶子节点路径上节点的数量。
 * 最大深度是所有叶子节点的深度的最大值。
 * （注：叶子节点是指没有子节点的节点。）
 * User: 86189
 * Date: 2022-06-04
 * Time: 23:33
 */
//class TreeNode {
//    int val = 0;
//    TreeNode left = null;
//    TreeNode right = null;
//  }
/**
 * @author 86189
 */
public class Solution {
    public int maxDepth (TreeNode root) {
        if (root == null) {
            return 0;
        }
        return Math.max(maxDepth( root.left ),maxDepth( root.right ))+1;
    }

}
