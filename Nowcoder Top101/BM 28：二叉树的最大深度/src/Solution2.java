import java.util.ArrayDeque;
import java.util.Queue;

/**
 * Created by L.jp
 * Description:
 * User: 86189
 * Date: 2022-06-04
 * Time: 23:43
 */
class TreeNode {
    int val = 0;
    TreeNode left = null;
    TreeNode right = null;
}
public class Solution2 {
    public int maxDepth (TreeNode root) {
        //使用队列计录每一层的节点数，每进入一次队列就是新的一层，直到队列没有元素了就是最大深度
        if(root==null){
            return 0;
        }
        Queue<TreeNode> queue=new ArrayDeque<>();
        queue.add(root);
        int ret=0;
        while ( !queue.isEmpty() ){
            int n=queue.size();
            for (int i = 0; i < n; i++){
                TreeNode cur=queue.poll();
                if ( cur.left!=null ){
                    queue.add( cur.left );
                }
                if(cur.right != null){
                    queue.add(cur.right);
                }
            }
            ret++;
        }
        return ret;
    }
    

}
