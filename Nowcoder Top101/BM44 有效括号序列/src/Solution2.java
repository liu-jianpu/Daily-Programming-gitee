import java.util.Stack;

/**
 * Created by L.jp
 * Description:
 * User: 86189
 * Date: 2022-07-03
 * Time: 10:43
 */
//这个方法则是遇到左括号就把对应的右括号入栈，下一个遇到右括号是就弹出栈顶元素看是不是和当前的右括号匹配
public class Solution2 {
    public boolean isValid (String s) {
        Stack<Character> stack=new Stack<>();
        for(int i = 0; i <s.length(); i++){
            char ch=s.charAt(i);
            if(ch=='{'){
                stack.push( '}' );
            }else if(ch=='['){
                stack.push( ']' );
            }else if(ch=='('){
                stack.push( ')' );
            }else if(stack.isEmpty() || stack.pop()!=ch){
                return false;
            }
        }
        return stack.isEmpty();
    }
}
