import java.util.ArrayList;
import java.util.Vector;

/**
 * Created by L.jp
 * Description:
 * User: 86189
 * Date: 2022-06-01
 * Time: 18:32
 */
class TreeNode {
    int val = 0;
    TreeNode left = null;
    TreeNode right = null;
}
/**
 * @author 86189
 */
public class Solution2 {
    ArrayList<ArrayList<Integer>> ret=new ArrayList<>();
    public void levelOrderHelper(TreeNode root,int level){
        if(root==null){
            return;
        }else{
            //新的一层
            if(ret.size() < level ){
                //加入临时结果集
                ArrayList<Integer> tmp=new ArrayList<>();
                ret.add( tmp );
                tmp.add( root.val );
            }else{
                //遍历到同一层新的节点，就取出上次的结果集，然后在它的结尾加入该节点
                //下标是从0开始的，0下标表示第一层
                ArrayList<Integer> tmp=ret.get( level-1 );
                tmp.add( root.val );
            }
        }
        //递归处理下一层
        levelOrderHelper( root.left,level+1 );
        levelOrderHelper( root.right,level+1 );
    }
    public ArrayList<ArrayList<Integer>> levelOrder (TreeNode root) {
        //递归，关键是怎么记录深度，就是使用一个变量level,每进入新的一层，level就+1
        //怎么判断进入新的一层，就是当结果集的长度小于深度时就是进入了新的一层
        //进入结果集首先是定义一个新的临时结果集存储当前层的节点值，然后把这个临时结果集加入结果集
        //将节点值加入临时结果集
        //如果不是新的一层也就是结果集的长度等于大于深度，那么就说明遍历到了兄弟节点，然后就是获取到当前的临时结果集，将此兄弟节点的值加入临时结果集就可以了
        if ( root == null ){
            return null;
        }
         levelOrderHelper(root,1);
        return ret;
    }
    
}
