import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Queue;
/**
 * Created by L.jp
 * Description:给定一个二叉树，返回该二叉树层序遍历的结果，（从左到右，一层一层地遍历）
 * User: 86189
 * Date: 2022-05-31
 * Time: 18:46
 */
//class TreeNode {
//    int val = 0;
//    TreeNode left = null;
//    TreeNode right = null;
//}
//public class Solution {
//    public ArrayList<ArrayList<Integer>> levelOrder (TreeNode root) {
//        //非递归，使用队列
//        Queue<TreeNode> queue=new ArrayDeque<TreeNode>();
//        ArrayList<ArrayList<Integer>> ret=new ArrayList<>();
//        if(root==null){
//            return ret;
//        }
//        queue.add( root );
//        while ( !queue.isEmpty() ){
//            ArrayList<Integer> tmp=new ArrayList<>();
//            int n=queue.size();
//            for(int i=0;i<n;i++){
//                TreeNode cur=queue.poll();
//                tmp.add( cur.val );
//                if(cur.left!=null){
//                    queue.add( cur.left );
//                }
//                if(cur.right!=null){
//                    queue.add(cur.right);
//                }
//            }
//            ret.add( tmp );
//        }
//        return ret;
//    }
//}
