/**
 * Created by L.jp
 * Description:请实现无重复数字的升序数组的二分查找
 *
 * 给定一个 元素升序的、无重复数字的整型数组 nums 和一个目标值 target ，
 * 写一个函数搜索 nums 中的 target，如果目标值存在返回下标（下标从 0 开始），否则返回 -1
 * User: 86189
 * Date: 2022-05-25
 * Time: 10:47
 */
public class Solution {
    public static  int search (int[] nums, int target) {
        //二分法
//        int left=0;
//        int right= nums.length-1;
//        while (left<=right){
//            int mid=(left+right)/2;
//            if(target<nums[mid]){
//                right=mid-1;
//            }else if(target>nums[mid]){
//                left=mid+1;
//            }else{
//                return mid;
//            }
//        }
//        return -1;
        
        //遍历法
        for(int i=0;i<nums.length;i++){
            if(target==nums[i])
                return i;
        }
        return -1;
    }
    
    public static void main(String[] args) {
        int[] nums={};
        int target=2;
        System.out.println(search(nums, target));
    }

}
