import java.util.Arrays;

/**
 * Created by L.jp
 * Description:给定一个长度为 n 的数组 arr，求它的最长严格上升子序列的长度。
 * 所谓子序列，指一个数组删掉一些数（也可以不删）之后，形成的新数组。例如 [1,5,3,7,3] 数组，
 * 其子序列有：[1,3,3]、[7] 等。但 [1,6]、[1,3,5] 则不是它的子序列。
 * 我们定义一个序列是 严格上升 的，当且仅当该序列不存在两个下标 i 和 j满足 i<j且 arr_i >= arr_j
 * User: 86189
 * Date: 2022-09-01
 * Time: 15:37
 */
public class Solution {
    public int LIS (int[] arr) {
        if(arr.length==0){
            return 0;
        }
        int len=arr.length;
        //dp[i]表示第i个位置的最长上升子序列
        int[] dp=new int[len];
        //最大值
        int max=0;
        //初始化。每一个位置初始的上升子序列是他本身
        Arrays.fill(dp,1);
        //构造数组，每一个位置的上升子序列需要从头开始看起
        //所以需要两层循环
        for(int i=1;i<len;i++){
            for(int j=0;j<i;j++){
                if(arr[j]<arr[i]){
                    //每一个位置的最长上升子序列
                    dp[i]=Math.max(dp[i],dp[j]+1);
                }
            }
            //最终的最长上升子序列
            max=Math.max(max,dp[i]);
        }
        return  max;
    }
}
