import java.util.ArrayList;
import java.util.Stack;
import java.util.TreeMap;

/**
 * Created by L.jp
 * Description:
 * User: 86189
 * Date: 2022-06-23
 * Time: 23:50
 */
public class Solution2 {
    public boolean isValidBST(TreeNode root) {
        //也可以使用栈来完成非递归，判断是不是二叉搜索树就可以按照中序遍历的方法，把遍历到的数字加入到一个数组中
        //判断是不是升序数组就行了
        Stack<TreeNode> stack=new Stack<>();
        TreeNode head=root;
        ArrayList<Integer> arrayList=new ArrayList<>();
        while (head!=null || !stack.isEmpty()){
            while (head!=null){
                //找到最左边
                stack.push(head);
                head=head.left;
            }
            //为空之后就说遍历到了最左边的节点，然后按照左根右的顺序去把节点的值加到数中
            head=stack.pop();
            arrayList.add(head.val);
            head=head.right;
        }
        for(int i=1;i<arrayList.size();i++){
            if(arrayList.get(i)<arrayList.get(i-1)){
                return false;
            }
        }
        return true;
    }
}
