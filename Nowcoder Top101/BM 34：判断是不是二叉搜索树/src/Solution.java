
/**
 * Created by L.jp
 * Description:给定一个二叉树根节点，请你判断这棵树是不是二叉搜索树。
 *
 * 二叉搜索树满足每个节点的左子树上的所有节点均严格小于当前节点且右子树上的所有节点均严格大于当前节点。
 * User: 86189
 * Date: 2022-06-23
 * Time: 23:50
 */
class TreeNode {
    int val = 0;
    TreeNode left = null;
    TreeNode right = null;
    public TreeNode(int val) {
      this.val = val;
    }
  }
public class Solution {
    int pre=Integer.MIN_VALUE;
    public boolean isValidBST(TreeNode root) {
        //递归的方法，按照中序遍历的方法去遍历二叉搜索树
        //如果没有根节点，那么空节点也是二叉搜索树
        if(root==null){
            return true;
        }
        //左
        //如果左边不是二叉搜索树
        if(!isValidBST(root.left)){
            return false;
        }
        //根
        //判断是不是二叉搜索树，在左子树中如果跟当前节点的值小于小于是上一个节点的值
        if(root.val<pre){
            return false;
        }
        //如果左边是二叉搜索树，那么更新前一个节点，也就是上一次访问的节点
        pre=root.val;
        //右
        return isValidBST(root.right);
    }
}
