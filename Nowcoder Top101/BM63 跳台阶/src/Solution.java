/**
 * Created by L.jp
 * Description:
 * User: 86189
 * Date: 2022-08-24
 * Time: 23:41
 */
public class Solution {
    public int jumpFloor(int target) {
        if (target < 2) {
            return 1;
        }
        int ret = 0;
        int a = 0;
        int b = 1;
        for (int i = 2; i <= target; i++) {
            ret = (a + b);
            a = b;
            b = ret;
        }
        return ret;
        //递归
//        if(target<2){
//            return 1;
//        }
//        return Fibonacci(target-1)+Fibonacci(target-1);
//    }
        //动归
//    int[] rets=new int[target+1];
//    rets[0]=1;
//    rets[1]=1;
//    for(int i=2;i<=target;i++){
//        rets[i]=rets[i-2]+rets[i-1];
//    }
//    return rets[target];
    }
}
