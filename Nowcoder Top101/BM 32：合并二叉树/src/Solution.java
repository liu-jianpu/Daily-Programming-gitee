import java.util.TreeMap;

/**
 * Created by L.jp
 * Description:已知两颗二叉树，将它们合并成一颗二叉树。合并规则是：都存在的结点，就将结点值加起来，否则空的位置就由另一个树的结点来代替。
 * User: 86189
 * Date: 2022-06-20
 * Time: 22:30
 */
class TreeNode {
    int val = 0;
    TreeNode left = null;
    TreeNode right = null;
    public TreeNode(int val){
        this.val=val;
    }
  }
public class Solution {
    public TreeNode mergeTrees (TreeNode t1, TreeNode t2) {
        //推荐使用递归的方式，采用前序遍历的方式
        if(t1==null){
            return t2;
        }
        if(t2==null){
            return t1;
        }
        TreeNode root=new TreeNode(t1.val+t2.val);
        root.left=mergeTrees(t1.left,t2.left);
        root.right=mergeTrees(t1.right,t2.right);
        return root;
    }
}
