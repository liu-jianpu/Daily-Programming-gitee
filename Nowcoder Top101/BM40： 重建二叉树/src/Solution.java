import java.util.Arrays;

/**
 * Created by L.jp
 * Description:
 * User: 86189
 * Date: 2022-06-28
 * Time: 22:23
 */
class TreeNode {
      int val;
      TreeNode left;
      TreeNode right;
      TreeNode(int x) { val = x; }
  }
public class Solution {
    public TreeNode reConstructBinaryTree(int [] pre,int [] vin){
        //这个就是我们平常推导构建二叉树的一个模拟的方法，把这个方法用代码写出来，主要是要搞清楚逻辑
        //我们找每一个子树的根节点的时候都是找到根节点在中序数组中的位置，然后把数组分为左右两个数组
        //然后在分割出来的数组中找到根节点，然后拼接左右子树，记住每一次分割的根节点的位置，
        //根节点的前面就是左子树，根节点的后面就是右子树，就这样重复分割，直到子数组长度为0
        int n=pre.length;
        int m=vin.length;
        if(n==0 || m==0){
            return null;
        }
        TreeNode root=new TreeNode(pre[0]);
        for(int i=0;i<vin.length;i++){
            //分割左右子树、
            if(pre[0]==vin[i]){
                root.left=reConstructBinaryTree( Arrays.copyOfRange( pre,1,i+1 ),
                        Arrays.copyOfRange( vin,0,i ));
                root.right=reConstructBinaryTree(Arrays.copyOfRange(pre, i+1, pre.length),
                        Arrays.copyOfRange(vin,i+1,vin.length));
                break;
            }
        }
        return root;
    }
}
