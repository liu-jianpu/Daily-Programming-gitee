/**
 * Created by L.jp
 * Description:
 * User: 86189
 * Date: 2022-05-12
 * Time: 18:09
 */
class ListNode {
    int val;
    ListNode next = null;
    
    ListNode(int val) {
        this.val = val;
    }
    
    @Override
    public String toString() {
        return "ListNode{" + "val=" + val + ", next=" + next + '}';
    }
}
public class Solution2 {
    public static ListNode Merge(ListNode list1,ListNode list2) {
        if(list1==null){
            return list2;
        }
        if(list2==null){
            return list1;
        }
        //递归，每一次都是比较大小，然后把小的节点赋值给新节点
        ListNode result=new ListNode(-1);
        //第一次想递归的时候，就想到时这样写，第一次判断两个链表的头结点，谁小就拼接在新的链表的后面，然后再往下走
        //其实这是递归的一小步，每一步都是这样
        if(list1.val<list2.val){
            result=list1;
            list1=list1.next;
        }else{
            result=list2;
            list2=list2.next;
        }
        //然后第一次拼接完就让新的链表拼接后面的
        result.next=Merge(list1,list2);
        return result;
    }
    
    public static void main(String[] args) {
        ListNode list1=new ListNode(1);
        ListNode list2=new ListNode(2);
        int val2= list2.val;
        int val=list1.val;
        for(int i = 0;i<3;i++){
            list1.next = new ListNode(val+2);
        }
        for(int i = 0; i < 3; i++){
            list2.next=new ListNode(val2+2);
        }
        System.out.println(Merge(list1, list2));
    }
}
