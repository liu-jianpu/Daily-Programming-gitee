/**
 * Created by L.jp
 * Description:大家都知道斐波那契数列，现在要求输入一个正整数 n ，请你输出斐波那契数列的第 n 项。
 * User: 86189
 * Date: 2022-08-24
 * Time: 23:23
 */
public class Solution {
    //迭代
    public int Fibonacci(int n) {
        if (n < 2) {
            return n;
        }
        int ret = 0;
        int a = 0;
        int b = 1;
        for (int i = 2; i <= n; i++) {
            ret = (a + b);
            a = b;
            b = ret;
        }
        return ret;
        //递归
//    public int Fibonacci(int n) {
//        if(n<2){
//            return 1;
//        }
//        return Fibonacci(n-1)+Fibonacci(n-1);
//    }
        //动归
//    int[] rets=new int[n+1];
//    rets[0]=1;
//    rets[1]=1;
//    for(int i=2;i<=n;i++){
//        rets[i]=rets[i-2]+rets[i-1];
//    }
//    return rets[n];
    }
}
