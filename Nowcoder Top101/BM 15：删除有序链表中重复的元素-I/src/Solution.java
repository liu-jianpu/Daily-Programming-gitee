/**
 * Created by L.jp
 * Description:删除给出链表中的重复元素（链表中元素从小到大有序），使链表中的所有元素都只出现一次
 * User: 86189
 * Date: 2022-05-24
 * Time: 9:01
 */
class ListNode {
    int val;
    ListNode next = null;
}
public class Solution {
    public ListNode deleteDuplicates (ListNode head) {
        if(head==null){
            return null;
        }
        //法一：
       /* ListNode cur=head;
        while(cur!=null && cur.next!=null){
            if(cur.val==cur.next.val){
                cur.next=cur.next.next;
            }else{
                cur=cur.next;
            }
        }
        return  head;*/
        
        //法二：更优的解法
        ListNode cur=head;
        ListNode pre=head.next;
        while (pre!=null){
            if(pre.val== cur.val){
                cur.next=pre.next;
                pre=pre.next;
            }else{
                pre=pre.next;
                cur=cur.next;
                
            }
        }
        return head;
    }
}
