import java.util.LinkedList;
import java.util.Queue;

/**
 * Created by L.jp
 * Description:
 * User: 86189
 * Date: 2022-02-14
 * Time: 22:41
 */
public class MyStack2 {
    /*  一个队列实现栈更简单，要使一个队列的元素排布跟栈一样，其实本来是相反的，那么就只有在元素入队后，把元素之前的所有元素
        出队再入队，就实现了栈的元素排布
    * */
    Queue<Integer> q=new LinkedList<>();
    public MyStack2() {

    }
   //主要操作就是在push操作
    public void push(int x) {
        //先获取队的大小，知道在这个元素之前有多少个元素需要弹出然后又入队
        int size=q.size();
       //入队
        q.offer(x);
        //把元素x之前的所有元素弹出再入队
        for(int i = 0; i < size; i++){
            q.offer(q.poll());
        }
    }

    public int pop() {
        //实现了栈的元素排布，直接弹出队头元素
        return  q.poll();

    }

    public int top() {
        return q.peek();

    }

    public boolean empty() {
        return  q.isEmpty();

    }
}
