import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;

/**
 * Created by L.jp
 * Description:
 * 链接：https://www.nowcoder.com/questionTerminal/05f97d9b29944c018578d98d7f0ce56e
 * 来源：牛客网
 *
 * Web系统通常会频繁地访问数据库，如果每次访问都创建新连接，性能会很差。为了提高性能，架构师决定复用已经创建的连接。当收到请求，并且连接池中没有剩余可用的连接时，系统会创建一个新连接，当请求处理完成时该连接会被放入连接池中，供后续请求使用。
 *
 * 现在提供你处理请求的日志，请你分析一下连接池最多需要创建多少个连接。
 *
 * 输入描述:
 * 输入包含多组数据，每组数据第一行包含一个正整数n（1≤n≤1000），表示请求的数量。
 *
 * 紧接着n行，每行包含一个请求编号id（A、B、C……、Z）和操作（connect或disconnect）。
 *
 *
 * 输出描述:
 * 对应每一组数据，输出连接池最多需要创建多少个连接。
 * 示例1
 * 输入
 * 6
 * A connect
 * A disconnect
 * B connect
 * C connect
 * B disconnect
 * C disconnect
 * 输出
 * 2
 * User: 86189
 * Date: 2022-04-10
 * Time: 22:23
 */
public class Main {
    //这个题目就是计算一个集合里的最大个数
    //只要是connect就加入哈希表，不是就从哈希表里删除
    //最后计算哈希表里的最大元素个数就可以
    public static void main(String[] args) {
        Scanner scanner=new Scanner(System.in);
        while (scanner.hasNext()) {
            Set<String> set=new HashSet<>();
            int n = scanner.nextInt();
            int max=0;
            for(int i = 0; i < n; i++){
                String id=scanner.next();
                String order = scanner.next();
                if(order.equals("connect")){
                    set.add(id);
                }else{
                    set.remove(id);
                }
                 max=Math.max(max, set.size());
            }
            System.out.println(max);
        }
        
    }
}
