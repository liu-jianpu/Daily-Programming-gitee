<<<<<<< HEAD
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;

/**
 * Created by L.jp
 * Description:链接：https://www.nowcoder.com/questionTerminal/2baa6aba39214d6ea91a2e03dff3fbeb
 * 来源：牛客网
 * 开发一个简单错误记录功能小模块，能够记录出错的代码所在的文件名称和行号。
 * 处理
 * 1、 记录最多8条错误记录，循环记录，最后只用输出最后出现的八条错误记录。对相同的错误记录只记录一条，但是错误计数增加。最后一个斜杠后面的带后缀名的部分（保留最后16位）和行号完全匹配的记录才做算是“相同”的错误记录。
 * 2、 超过16个字符的文件名称，只记录文件的最后有效16个字符；
 * 3、 输入的文件可能带路径，记录文件名称不能带路径。也就是说，哪怕不同路径下的文件，如果它们的名字的后16个字符相同，也被视为相同的错误记录
 * 4、循环记录时，只以第一次出现的顺序为准，后面重复的不会更新它的出现时间，仍以第一次为准
 *
 * 数据范围：错误记录数量满足 1 \le n \le 100 \1≤n≤100  ，每条记录长度满足 1 \le len \le 100 \1≤len≤100
 *
 * 输入描述:
 * 每组只包含一个测试用例。一个测试用例包含一行或多行字符串。每行包括带路径文件名称，行号，以空格隔开。
 *
 *
 *
 * 输出描述:
 * 将所有的记录统计并将结果输出，格式：文件名 代码行数 数目，一个空格隔开，
 * User: 86189
 * Date: 2022-04-22
 * Time: 18:17
 */
public class Main {
    //关键信息：
    // 1.记录最多8条错误记录，循环记录，最后只用输出最后出现的八条错误记录
    // 2.对相同的错误记录只记录一条，但是错误计数增加。最后一个斜杠后面的带后缀名的部分（保留最后16位）和行号完全匹配的记录才做算是“相同”的错误记录。
    // 3.超过16个字符的文件名称，只记录文件的最后有效16个字符
    // 4.哪怕不同路径下的文件，如果它们的名字的后16个字符相同，也被视为相同的错误记录
    // 5.循环记录时，只以第一次出现的顺序为准
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        //使用一个顺序表记录最后的8个结果
        ArrayList<String> list = new ArrayList<>();
        //使用一个哈希表记录文件字符串和行号字符串匹配时出现的次数
        HashMap<String, Integer> map = new HashMap<>();
        while (scan.hasNext()) {
            //先输入文件路径
//            String file = scan.next();//不能读取我输入的空格
//            //再输入行号
//            String lineNum = scan.next();//都是在同一行
//            //以\分割路径字符串
//            String[] fileStr = file.split("\\\\");  //注意：这里的四个反斜杠是转义字符，第一个和最后一个是转义双引号的，中间两个时转义一个反斜杠的
//            //获取文件名称，就是最后字符串数组的最后一个元素
//            file = fileStr[fileStr.length - 1];
//            //如果文件名的长度大于16，那么只需要最后16位
//            if (file.length() > 16) {
//                file = file.substring(file.length() - 16);
//            }
//            //然后文件名和行号配对成一个完整的字符串
//            file += " " + lineNum;
//            //如果文件名不同，而只是最后16个字符相同，媕娿吗
//            //对文件进行判断和计数
//            if (!map.containsKey(file)) {
//                list.add(file);
//                map.put(file, 1);
//            } else {
//                map.put(file, map.get(file) + 1);
//            }
            
            //法二：
            //输入一行
            String file=scan.nextLine();
            String[] str=file.split(" ");
            String filePath=str[0];
            String rowStr=str[1];
            //先找到最后一个\的位置
            int index=filePath.lastIndexOf("\\");
            //如果最后一个\后面的字符串长度超过16，那么就直接截取后面16个字符作为文件名
            //如果没有没有超过16，那么就直接从最后一个\后面的字符串
            filePath=filePath.substring(index+1);  //也适用于文件名不超过16个字符的情况
            if(filePath.length()>16){
                filePath=filePath.substring(filePath.length()-16);
            }
            //让文件名和行号重新构成一个字符串，因为判断错误记录，必须是文件名和行号完全相同才是错误的记录
            filePath+=" "+rowStr;
            if(!map.containsKey(filePath)){
                list.add(filePath);
                map.put(filePath,1);
            }else{
                map.put(filePath,map.get(filePath)+1);
            }
        }
        //顺序输出8个
        int start = 0;
        if (list.size() > 8) {
            start = list.size() - 8;
        }
        for (; start < list.size(); start++) {
            //除了输出文件名和行号，还要输出次数
            System.out.println(list.get(start) + " " + map.get(list.get(start)));
        }
    }
}

=======
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;

/**
 * Created by L.jp
 * Description:链接：https://www.nowcoder.com/questionTerminal/67df1d7889cf4c529576383c2e647c48
 * 来源：牛客网
 *
 * 开发一个简单错误记录功能小模块，能够记录出错的代码所在的文件名称和行号。
 * 处理:
 * 1.记录最多8条错误记录，对相同的错误记录(即文件名称和行号完全匹配)只记录一条，错误计数增加；(文件所在的目录不同，文件名和行号相同也要合并)
 * 2.超过16个字符的文件名称，只记录文件的最后有效16个字符；(如果文件名不同，而只是文件名的后16个字符和行号相同，也不要合并)
 * 3.输入的文件可能带路径，记录文件名称不能带路径
 *
 * 数据范围：输入错误记录数量满足 1 \le n \le 1000 \1≤n≤1000  ，每条记录的长度满足 1 \le len \le 50 \1≤len≤50
 *
 * 输入描述:
 * 一行或多行字符串。每行包括带路径文件名称，行号，以空格隔开。
 *
 *     文件路径为windows格式
 *
 *     如：E:\V1R2\product\fpgadrive.c 1325
 *
 *
 * 输出描述:
 * 将所有的记录统计并将结果输出，格式：文件名代码行数数目，一个空格隔开，如: fpgadrive.c 1325 1
 *
 *     结果根据数目从多到少排序，数目相同的情况下，按照输入第一次出现顺序排序。
 *
 *     如果超过8条记录，则只输出前8条记录.
 *
 *     如果文件名的长度超过16个字符，则只输出后16个字符
 * User: 86189
 * Date: 2022-04-22
 * Time: 18:17
 */
public class Main {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        //使用一个顺序表记录最后的8个结果
        ArrayList<String> list = new ArrayList<>();
        //使用一个哈希表记录文件字符串和行号字符串匹配时出现的次数
        HashMap<String, Integer> map = new HashMap<>();
        while (scan.hasNext()) {
            //先输入文件路径
            String file = scan.next();//不能读取我输入的空格
            //再输入行号
            String lineNum = scan.next();//都是在同一行
            //以\分割路径字符串
            String[] fileStr = file.split("\\\\");  //注意：这里的四个反斜杠是转义字符，第一个和最后一个是转义双引号的，中间两个时转义一个反斜杠的
            //获取文件名称，就是最后字符串数组的最后一个元素
            file = fileStr[fileStr.length - 1];
            //如果文件名的长度大于16，那么只需要最后16位
            if (file.length() > 16) {
                file = file.substring(file.length() - 16);
            }
            //然后文件名和行号配对成一个完整的字符串
            file += " " + lineNum;
            //如果文件名不同，而只是最后16个字符相同，媕娿吗
            //对文件进行判断和计数
            if (!map.containsKey(file)) {
                list.add(file);
                map.put(file, 1);
            } else {
                map.put(file, map.get(file) + 1);
            }
        }
        //顺序输出8个
        int start = 0;
        if (list.size() > 8) {
            start = list.size() - 8;
        }
        for (; start < list.size(); start++) {
            //除了输出文件名和行号，还要输出次数
            System.out.println(list.get(start) + " " + map.get(list.get(start)));
        }
    }
}

>>>>>>> c05036c30c3813a20699f5fd4d6909daf6b17e23
