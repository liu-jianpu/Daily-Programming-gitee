import java.util.HashSet;

/**
 * Created by L.jp
 * Description:
 * User: 86189
 * Date: 2021-10-28
 * Time: 12:23
 */
public class GemstonesAndStones {
    public int numJewelsInStones(String jewels, String stones) {
        HashSet<Character> set=new HashSet<>();
        for(int i=0;i<jewels.length();i++){
            char ch=jewels.charAt(i);//获取jwels的每个字符
            set.add(ch);//将jewels的每个字符放入set
        }
        int count=0;
        for(int i=0;i<stones.length();i++){
            char ch1=stones.charAt(i);
            if(set.contains(ch1)){
                count++;
            }
        }
        return count;
    }
}
