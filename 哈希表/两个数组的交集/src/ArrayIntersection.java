import java.util.*;

/**
 * Created by L.jp
 * Description:
 * User: 86189
 * Date: 2021-10-29
 * Time: 19:25
 */
public class ArrayIntersection {
    public static int[] intersection(int[] nums1, int[] nums2){
        Set<Integer> set=new HashSet<>();//存放其中一个数组的元素
        for(int num:nums1){
            set.add(num);
        }
        Set<Integer> retSet=new HashSet<>();//存放交集元素
        //List<Integer> list=new ArrayList<>();//也可以使用list存放交集元素
        for(int num:nums2){
            if(set.contains(num)){
               retSet.add(num);//list没有去重的功能，所以会有重复的，但是最终只能输出一个，所以在加入一遍元素后，就要在set中把那个元素删了
               //set.remove(num);
            }
        }
        int[]arr=new int[retSet.size()];//将交集集合转换为数组
        int i=0;
        for(int num:retSet){
            arr[i++]=num;
        }
        return arr;

    }

    public static void main(String[] args) {
        int[]nums1={1,2,2,1};
        int[]nums2={2,2};
        int[]ret=intersection(nums1,nums2);
        System.out.println(Arrays.toString(ret));
    }
}
