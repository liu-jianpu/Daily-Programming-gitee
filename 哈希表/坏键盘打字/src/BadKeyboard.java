import java.util.HashSet;
import java.util.Locale;
import java.util.Scanner;
import java.util.Set;

/**
 * Created by L.jp
 * Description:输入在2行中分别给出应该输入的文字、以及实际被输入的文字。每段文字是不超过80个字符的串，由字母A-Z（包括大、小写）、数字0-9、
 * 以及下划线“_”（代表空格）组成。
 *按照发现顺序，在一行中输出坏掉的键。其中英文字母只输出大写，每个坏键只输出一次
 * User: 86189
 * Date: 2021-10-28
 * Time: 18:37
 */
public class BadKeyboard {
    public static void func(String str1, String str2) {
        HashSet<Character> setAct = new HashSet<>();//存放实际输入的键，每个键都是字符，是没坏的键
        for (char ch1 : str2.toUpperCase().toCharArray()) {//转化为大写字母的数组
            if (!setAct.contains(ch1)) {
                setAct.add(ch1);//存放的不会有重复的
            }
        }
        HashSet<Character> setBroken = new HashSet<>();//存放坏了的键
        for (char ch2 : str1.toUpperCase().toCharArray()) {
            if (!setAct.contains(ch2) && !setBroken.contains(ch2)) {//当实际输入集合里面不包含坏的键以及坏的键集合里面还没有放元素时
                setBroken.add(ch2);
                System.out.print(ch2);//根据题目要求按照顺序打印输出坏掉的键，所以不能直接打印setBroken集合，而是当setBroken里面增加一个时就打印一个
            }
        }
    }
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        String str1 = scan.nextLine();
        String str2 = scan.nextLine();
        func(str1, str2);
    }
}
