import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by L.jp
 * Description:
 * User: 86189
 * Date: 2021-11-06
 * Time: 20:20
 */
public class DuplicateElements1 {
    public static boolean containsDuplicate(int[] nums) {
        //哈希表
        /*
        Set<Integer> set=new HashSet<>();
        for(int i=0;i<nums.length;i++){
            if(set.contains(nums[i])){
                return true;
            }
            set.add(nums[i]);
        }
        return false;

         */
        //数组排序,对数组排顺序，如果数组已经有序，那么相同的元素一定是在相邻的位置
        Arrays.sort(nums);
        for(int i=0;i< nums.length;i++){
            if(nums[i]==nums[i+1]){
                return true;
            }
        }
        return false;

    }

    public static void main(String[] args) {
        int[] nums={1,2,3,2};
        System.out.println(containsDuplicate(nums));
    }
}
