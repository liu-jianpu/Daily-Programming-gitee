import java.util.Scanner;

/**
 * Created by L.jp
 * Description:给定一个仅由小写字母组成的字符串。现在请找出一个位置，删掉那个字母之后，字符串变成回文。请放心总会有一个合法的解。如果给定的字符串已经是一个回文串，那么输出-1。
 * 输入描述：
 * 第一行包含T，测试数据的组数。后面跟有T行，每行包含一个字符串。
 * 输出描述：
 * 如果可以删去一个字母使它变成回文串，则输出任意一个满足条件的删去字母的位置（下标从0开始）。
 * User: 86189
 * Date: 2022-01-11
 * Time: 23:42
 */

public class Solution {
    //法一：利用字符串拼接来删除一个字符来达到求删除一个字符使得回文的坐标

    /**
     * @param str: 字符串
     * @param start: 左边需要删除的字符坐标
     * @param end: 右边需要删除的字符坐标
     * [str, start, end]
     * @return boolean  回文的真值
     * @description TODO    判断字符串是否回文并且利用数组两个数组来存储需要删除字符的坐标
     */

    public static boolean isPalindrome(StringBuilder str,int [] start,int[] end){
        //两个坐标用于遍历坐标判断是否回文
        int i=0;
        int j=str.length()-1;
        //标记字符串回文的真值
        boolean flag=true;
        while(i<=j){
            if(str.charAt(i)==str.charAt(j)){
                i++;
                j--;
            }else{
                //不相等的话说明找到了一个要删除的字符的位置
                flag=false;//不是回文
                break;//退出循环
            }
        }
        //可以删除前后字符的任意一个，把要删除的坐标放入数组中
        if(start!=null){
            start[0]=i;
        }
        if(end!=null){
            end[0]=j;
        }
        return flag;
    }
    public static void main(String[] args){
        Scanner scan=new Scanner(System.in);
        //输入字符串的个数
        int n=scan.nextInt();
        //输入n个字符串
        while(n>0){
            //next函数会实现换行，nextline函数不实现换行，因为要找到删除掉的字符可以使字符串变成回文字符串的位置，所以需要可以改变的字符串类型
            StringBuilder str= new StringBuilder(scan.next());
            //接下来对每一行进行判断是不是回文
            //判断是不是回文我们需要从字符串两边去判断，所以删除的字符是有两种可能的，可以用两个数组来分别存储两边的坐标
            int[] start=new int[1];//只需要存储一个下标,存储左边的
            int[] end=new int[1];//存储右边的
            //判断回文
            if(isPalindrome(str,start,end)){
                System.out.println(-1);
            }else{
                //不是回文就需要删除一个字符，如果我们删除后面的字符
                str.deleteCharAt(end[0]);
                //导致这个字符串是回文的，那么这个坐标就是满足要求的
                if(isPalindrome(str,null,null)){  //这里设置为null,是因为只需要删除一次字符，不需要再用数组存储要删除的字符了
                    //输出这个坐标
                    System.out.println(end[0]);
                }else{
                    //如果删除了后面的字符导致他还不是回文的，那么就表示应该删除前面的字符它才是回文的
                    //输出该坐标
                    System.out.println(start[0]);
                }
            }
            n--;
        }
    }

}
