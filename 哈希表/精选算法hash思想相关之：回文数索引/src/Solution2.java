import sun.nio.cs.ext.ISO2022_CN_CNS;

import java.util.Scanner;

/**
 * Created by L.jp
 * Description:
 * User: 86189
 * Date: 2022-01-14
 * Time: 21:32
 */
public class Solution2 {
    //法二:直接利用字符串判断，不用删除字符，将字符串分成两部分，一个遍历左边部分，一个遍历右边部分
    //利用相邻字符是否相等来确定要删除字符的坐标
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int num = scanner.nextInt();
        while(num>0){
            String str= scanner.next();
            int len = str.length();
            //存储最终要删除的字符的坐标
            int ret=-1;
            //两个坐标，一个判断坐标部分，一个判断右边部分
            for(int i=0;i<len/2;i++){
                int j=len-1-i;//j坐标的求法
                //判断回文
                //不用再判断等于的情况了，直接判断不相等的情况，for循环，相等的情况坐标他会自己跳过去
                if(str.charAt(i)!=str.charAt(j)){
                    //当前左边字符不等于右边字符，不是回文，那么就需要找删除的字符
                    if(str.charAt(i+1)==str.charAt(j)){
                        //如果当前字符的下一个字符等于右边字符，那么就表示当前字符是需要删去的，删除他
                        ret=i;
                    }else if(str.charAt(i)==str.charAt(j-1)){
                        //当前字符等于右边字符的前一个字符，说明当前右边字符是需要删除的
                        ret=j;
                    }
                    //找到了，退出
                    break;

                }
            }
            //判断完一个字符串，需要输出对应要删除的字符坐标
            System.out.println(ret);
            num--;
        }
        return;
    }
}
