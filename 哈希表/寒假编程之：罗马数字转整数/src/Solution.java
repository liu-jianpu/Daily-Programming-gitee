import java.util.HashMap;
import java.util.Map;

/**
 * Created by L.jp
 * Description:罗马数字包含以下七种字符:I，V，X，L，C，D和M。
 *
 * 字符          数值
 * I             1
 * V             5
 * X             10
 * L             50
 * C             100
 * D             500
 * M             1000
 * 例如， 罗马数字 2 写做II，即为两个并列的 1 。12 写做XII即为X+II。 27 写做XXVII, 即为XX+V+II。
 *
 * 通常情况下，罗马数字中小的数字在大的数字的右边。但也存在特例，例如 4 不写做IIII，而是IV。数字 1 在数字 5 的左边，所表示的数等于大数 5 减小数 1 得到的数值 4 。同样地，数字 9 表示为 IX。这个特殊的规则只适用于以下六种情况：
 *
 * I可以放在V(5) 和X(10) 的左边，来表示 4 和 9。
 * X可以放在L(50) 和C(100) 的左边，来表示 40 和90。
 * C可以放在D(500) 和M(1000) 的左边，来表示400 和900。
 * 给定一个罗马数字，将其转换成整数。
 * User: 86189
 * Date: 2022-02-03
 * Time: 11:26
 */
public class Solution {
    public static int romanToInt(String s) {
        //存放罗马数字和对应的整数
        Map<Character,Integer> map=new HashMap<>();
        map.put('I',1);
        map.put('V',5);
        map.put('X',10);
        map.put('L',50);
        map.put('C',100);
        map.put('D',500);
        map.put('M',1000);
        int i=0;
        int ret=0;
        int len=s.length();
        for(;i<len;i++){
            //先拿到当前字符对应的数字
            int val=map.get(s.charAt(i));
            //到底是相加还是相减看的是当前值和后一个值的大小关系。i<len-1是为了防止越界
            if(i<len-1 && val<map.get(s.charAt(i+1))){
                ret-=val;//这里防止越界i<len-1,只能到计算到倒数第二个元素
            }else{
                //所以最后一个元素一定要加上，而不是减去，所以就把小于的条件放在了上面
                ret+=val;
            }
        }
        return  ret;
    }
    public static void main(String[] args) {
        System.out.println(romanToInt("MCMXCIV"));
    }
}
