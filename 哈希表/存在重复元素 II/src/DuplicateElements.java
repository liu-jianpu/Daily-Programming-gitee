import java.util.HashSet;
import java.util.Set;

/**
 * Created by L.jp
 * Description:给定一个整数数组和一个整数k，判断数组中是否存在两个不同的索引i和j，使得nums [i] = nums [j]，并且 i 和 j的差的 绝对值 至多为 k。
 *
 * User: 86189
 * Date: 2021-11-06
 * Time: 19:08
 */
public class DuplicateElements {
    public static boolean containsNearbyDuplicate(int[] nums, int k){
        //暴力破解法，循环
        /*
        for(int i=0;i<nums.length;i++){
            for(int j=i+1;j< nums.length && j<=i+k;j++){
                if(nums[i]==nums[j]){
                    return true;
                }
            }
        }
        return false;

         */
        //哈希表，维护一个只有k大小的哈希表，如果超过哈希表元素个数k个，那么就删除第一个元素
        ////假设k==2,也就是说，两个相同的元素之间隔了一个元素，那么我就可以把这个元素和中间隔的元素放入哈希表，
        // 哈希表长度始终为两个元素，多的删除，遍历第三个元素时，如果set里面已经有了，那么表示存在
        Set<Integer> set=new HashSet<>();
        for(int i=0;i< nums.length;i++){
            if(set.contains(nums[i])){
                return true;
            }
            set.add(nums[i]);
            if(set.size()>k){
                set.remove(nums[i-k]);//删除旧的元素

            }
        }
        return false;

    }

    public static void main(String[] args) {
        int[]nums={1,2,3,1,2,3};
        System.out.println(containsNearbyDuplicate(nums, 2));

    }
}
