import java.util.*;

/**
 * Created by L.jp
 * Description:链接：https://www.nowcoder.com/questionTerminal/ceb89f19187b4de3997d9cdef2d551e8
 * 来源：牛客网
 *  1       2       3
 *         ABC     DEF
 *  4       5       6
 * GHI     JKL     MNO
 *  7       8       9
 * PQRS    TUV     WXYZ
 *          0
 *
 * 上图是一个电话的九宫格，如你所见一个数字对应一些字母，因此在国外企业喜欢把电话号码设计成与自己公司名字相对应。例如公司的Help Desk号码是4357，因为4对应H、3对应E、5对应L、7对应P，因此4357就是HELP。同理，TUT-GLOP就代表888-4567、310-GINO代表310-4466。
 * NowCoder刚进入外企，并不习惯这样的命名方式，现在给你一串电话号码列表，请你帮他转换成数字形式的号码，并去除重复的部分。
 *
 * 输入描述:
 * 输入包含多组数据。
 *
 * 每组数据第一行包含一个正整数n（1≤n≤1024）。
 *
 * 紧接着n行，每行包含一个电话号码，电话号码仅由连字符“-”、数字和大写字母组成。
 * 没有连续出现的连字符，并且排除连字符后长度始终为7（美国电话号码只有7位）。
 *
 *
 * 输出描述:
 * 对应每一组输入，按照字典顺序输出不重复的标准数字形式电话号码，即“xxx-xxxx”形式。
 *
 * 每个电话号码占一行，每组数据之后输出一个空行作为间隔符。
 * User: 86189
 * Date: 2022-04-19
 * Time: 18:44
 */
public class Main {
    /*  这是一个字符和数字匹配的问题，我们可以利用哈希表存储这个映射关系，把字符作为key,把数字作为value,遍历到字符的时候就取出对应的数字
        或者是给出字母表和数字两个对应的字符串，让号码表的字母和数字对应，指的是同一组字母和数字的下标是一样的，
        然后遍历到字符时从字母表字符串中去找这个字符出现的下标，根据这个下标找到对应的数字
    *
    * */
    public static void main(String[] args) {
        //法一：
//        Scanner scanner=new Scanner(System.in);
//        String alph="ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
//        String numb="222333444555666777788899990123456789";
//        while (scanner.hasNext()) {
//            int n=scanner.nextInt();
//            ArrayList<String> ret=new ArrayList<>();
//            for(int i = 0; i < n; i++){
//                String phoneNumber=scanner.next();
//                //先把输入的“-”给全部剔除，这样就剩下了7位号码
//                phoneNumber=phoneNumber.replace("-","");
//                //存储临时合法的号码
//                String tmp="";
//                for(int j = 0; j < 7; j++){
//                    //找到对应字母在字母表出现的下标，对应下标找到对应的数字号码
//                    tmp+=numb.charAt(alph.indexOf(phoneNumber.charAt(j)+""));
//                }
//                //拼接之后都是不带“-”号的，输出时需要，所以就加上
//                tmp=tmp.substring(0,3)+"-"+tmp.substring(3,7);
//                //然后加入到最终的结果集顺序表,然后去重
//                if(!ret.contains(tmp)){
//                    ret.add(tmp);
//                }
//            }
//            //题目要求需要按照字典序的结果输出，所以就对顺序表的进行排序在输出
//            Collections.sort(ret);
//            for(String string:ret){
//                System.out.println(string);
//            }
//            //输出完一组需要空行
//            System.out.println();
//        }
//    }
        
        //法二：使用哈希表
        Scanner scanner = new Scanner(System.in);
        //存储字母和数字映射关系
        Map<Character,Character> map=new HashMap<>();
        String alph="ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        String numb="22233344455566677778889999";
        for(int i = 0; i <alph.length(); i++){
            map.put(alph.charAt(i),numb.charAt(i));
        }
        //存储号码,使用set如果不实现比较器，那么就没有比较，就是按照输入顺序输出，但是题目要求按照字典序输出，所以可以使用TreeSet,
        //TreeSet默认实现了自然排序，也就是升序排序，按照对于字符创就是实现了字典排序
        //对于TreeMap也是一样的，实现了compareTo方法，也就是自然排序
        TreeSet<String> set=new TreeSet<>();
        while (scanner.hasNext()) {
            //对于下一组输入，需要清空set表
            set.clear();
            int n=scanner.nextInt();
            for(int i = 0; i < n; i++){
                //输入
                String phoneNum=scanner.next();
                //结果字符串
                StringBuilder sb = new StringBuilder();
                //先去掉“-”
                phoneNum=phoneNum.replace("-","");
                for(int j = 0; j <7;j++){
                    //判断是不是数字，是数字直接拼接
                    if(isDigit(phoneNum.charAt(j))){
                        sb.append(phoneNum.charAt(j));
                    }
                    if(isUpperCase(phoneNum.charAt(j))){
                        sb.append(map.get(phoneNum.charAt(j)));
                    }
                }
                //拼接完之后就是一个完整的7位号码，但是还有一个“-”格式
                String number=sb.toString().substring(0,3)+"-"+sb.substring(3,7);
                set.add(number);
            }
            //输出全部的
            for(String number:set){
                System.out.println(number);
            }
            //输出一个空行，以便下一组输入
            System.out.println();
        }
    }
    public static  boolean isDigit(char ch){
        return  ch>='0' && ch<='9';
    }
    public static  boolean isUpperCase(char ch){
        return  ch>='A' && ch<='Z';
    }
}

