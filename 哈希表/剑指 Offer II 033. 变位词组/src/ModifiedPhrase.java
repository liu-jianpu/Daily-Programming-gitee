import java.util.*;

/**
 * Created by L.jp
 * Description:
 * User: 86189
 * Date: 2021-11-02
 * Time: 22:37
 */
public class ModifiedPhrase {
    public static List<List<String>> groupAnagrams(String[] strs) {
        Map<String,List<String>> map=new HashMap<>();//将字符串入key值，变位词的顺序表入value值
       //for(String str:strs)也可以
        for(int i=0;i<strs.length;i++){
            char[] ch=strs[i].toCharArray();//将字符串数组每一个字符串变成字符数组
            Arrays.sort(ch);//排序
            String str=new String(ch);//将字符串数组变成字符串
            if(!map.containsKey(str)){
                map.put(str,new ArrayList<>());//不包含就把str值入key值，然后新建一个顺序表入value值
            }
            map.get(str).add(strs[i]);//当包含时，就加入其他的变位词
        }
        return new ArrayList<>(map.values());//新建一个顺序表把value值的list表嵌套，返回map表中的value值，是嵌套list类型
    }

    public static void main(String[] args) {
        String[] str={"eat", "tea", "tan", "ate", "nat", "bat"};
        System.out.println(groupAnagrams(str));
    }
}

