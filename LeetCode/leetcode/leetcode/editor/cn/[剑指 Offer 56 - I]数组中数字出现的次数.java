
//leetcode submit region begin(Prohibit modification and deletion)
class Solution {
    public int[] singleNumbers(int[] nums) {
        Map<Integer, Integer> map=new HashMap<>();
        for(int i = 0; i < nums.length; i++){
            if(!map.containsKey( nums[i] )){
                map.put( nums[i],1 );
            }else{
                int count=map.get( nums[i] );
                count++;
                map.put(nums[i], count);
            }
        }
    }
}
//leetcode submit region end(Prohibit modification and deletion)
