import java.util.Arrays;

/**
 * Created by L.jp
 * Description:春节期间小明使用微信收到很多个红包，非常开心。在查看领取红包记录时发现，某个红包金额出现的次数超过了红包总数的一半。请帮小明找到该红包金额。写出具体算法思路和代码实现，要求算法尽可能高效。
 *
 * 给定一个红包的金额数组 gifts 及它的大小 n ，请返回所求红包的金额。
 *
 * 若没有金额超过总数的一半，返回0。
 * User: 86189
 * Date: 2022-03-19
 * Time: 17:13
 */
public class Gift {
    public static  int getValue(int[] gifts, int n) {
        // write code here
        Arrays.sort(gifts);
        int num=gifts[(0+n-1)/2];
        int count=0;
        for(int i = 0; i < n; i++){
            if(gifts[i]==num){
                count++;
            }
        }
        if(count>=n/2){
            return num;
        }
        return 0;
    }

    public static void main(String[] args) {
        int[] gifts={1,2,3,2,2,5,2,4};
        int n=8;
        System.out.println(getValue(gifts, n));
    }
}
