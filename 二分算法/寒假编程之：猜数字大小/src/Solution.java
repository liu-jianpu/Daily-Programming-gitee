import java.util.Random;

/**
 * Created by L.jp
 * Description:
 * User: 86189
 * Date: 2022-02-10
 * Time: 9:51
 */
public class Solution {
    public int guessNumber(int n) {
        //利用二分法，一直利用中间元素的值来猜，也就是将中间元素带入到guess接口中，根据返回值来判断大小关系
        int left=1;
        int right=n;
        while(left<right){
            int mid=left+(right-left)/2;
            //带入接口测试
            if(guess(mid)<=0){
                right=mid;
            }else{
                left=mid+1;
            }
        }
        return left;
    }

}
