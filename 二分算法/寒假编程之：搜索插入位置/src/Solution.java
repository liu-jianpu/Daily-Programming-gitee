/**
 * Created by L.jp
 * Description:给定一个排序数组和一个目标值，在数组中找到目标值，并返回其索引。如果目标值不存在于数组中，返回它将会被按顺序插入的位置。
 *
 * 请必须使用时间复杂度为 O(log n) 的算法。
 *
 * User: 86189
 * Date: 2022-02-10
 * Time: 14:28
 */
public class Solution {
    public static int searchInsert(int[] nums, int target) {
        int left=0;
        int right=nums.length-1;
        //如果比最小的还小，那么就返回下标0
        if(target<nums[0]){
            return 0;
        }
        //如果比最大的还大，那么就返回最大元素下标+1
        if(target>nums[right]){
            return right+1;
        }
        //二分查找
        while(left<right){
            int mid=left+(right-left)/2;
            if(nums[mid]<target){
                left=mid+1;
            }else if(nums[mid]>target){
                right=mid;
            }else{
                //如果要找的元素等于中间位置元素，那么直接返回中间元素下标
                return mid;
            }
        }
        //遍历完了数组，没有找到这个数，此时left和right相遇，直接返回其中一个下标,画图就知道了
        return left;
    }

    public static void main(String[] args) {
        int [] nums={1,2,5,7,8,16};
        System.out.println(searchInsert(nums, 6));
    }
}
