/**
 * Created by L.jp
 * Description:你是产品经理，目前正在带领一个团队开发新的产品。不幸的是，你的产品的最新版本没有通过质量检测。由于每个版本都是基于之前的版本开发的，所以错误的版本之后的所有版本都是错的。
 *
 * 假设你有 n 个版本 [1, 2, ..., n]，你想找出导致之后所有版本出错的第一个错误的版本。
 *
 * 你可以通过调用bool isBadVersion(version)接口来判断版本号 version 是否在单元测试中出错。实现一个函数来查找第一个错误的版本。你应该尽量减少对调用 API 的次数。

 * User: 86189
 * Date: 2022-02-10
 * Time: 15:37
 */
public class Solution {
    public int firstBadVersion(int n) {
        int left=1;
        int right=n;
        while(left<right){
            //通过对每次中间版本的判断来找出第一错误的版本
            int mid=left+(right-left)/2;
            //如果返回值是true,那么表示中间版本是错的，那么就把右区间缩小到原来的中间版本
            if(isBadVersion(mid)){
                right=mid;
            }else {
                //如果返回值是false,那么中间版本是对的，把左区间缩小到中间版本+1的位置
                left=mid+1;
            }
        }
        //当left和right相遇时，说明找到了第一个错误的版本
        return left;
    }
}
