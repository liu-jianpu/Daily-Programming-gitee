import java.util.Scanner;

/**
 * Created by L.jp
 * Description:任意一个偶数（大于2）都可以由2个素数组成，组成偶数的2个素数有很多种情况，
 * 本题目要求输出组成指定偶数的两个素数差值最小的素数对。
 * User: 86189
 * Date: 2022-03-06
 * Time: 18:44
 */
public class Main {
    public static void main(String[] args){
        Scanner scan=new Scanner(System.in);
        while(scan.hasNext()){
            int n=scan.nextInt();
            int half=n/2; //寻找差值最小的素数对，那么就可以选择中间开始判断是否是素数
            for(int i=half;i<=n;i++){
                if(isPrime(i) && isPrime(n-i)){
                    System.out.println(n-i);
                    System.out.println(i);
                    break;
                }
            }
        }
    }
    //判断素数
    public static boolean isPrime(int n){
        for(int i=2;i<=Math.sqrt(n);i++){
            if(n%i==0){
                return false;
            }
        }
        return true;
    }
}
