import java.util.Scanner;

/**
 * Created by L.jp
 * Description:某商店规定：三个空汽水瓶可以换一瓶汽水，
 * 允许向老板借空汽水瓶（但是必须要归还）。
 * 小张手上有n个空汽水瓶，她想知道自己最多可以喝到多少瓶汽水。
 * 数据范围：输入的正整数满足 1 \le n \le 100 \1≤n≤100
 *
 * 注意：本题存在多组输入。输入的 0 表示输入结束，并不用输出结果。
 * User: 86189
 * Date: 2022-03-15
 * Time: 11:49
 */
public class Main {
    public static void main(String[] args) {
        Scanner scanner=new Scanner(System.in);
        while (scanner.hasNextInt()){
            int n=scanner.nextInt();
            //n表示空汽水瓶数
            int ret=0;//可以喝的汽水瓶数
            while (n>1) {   //为了处理n==1这样的特殊情况，n==1时会死循环,而且空瓶数最后不可能等于0，只会等于1
                ret+=(n/3);
                n=n/3+n%3;//空瓶数等于上一轮喝掉的汽水瓶数+上一轮留下的空瓶数
                if(n==2){
                    //表示如果最后的空瓶数是2个时，可以借一个空瓶，然后就可以兑换一瓶汽水，喝完之后就代表不能再喝了，得还给老板，就break表示结
                    ret++;//借一个空瓶就可以兑一瓶汽水了
                    break;//整个过程完毕
                }
            }
            if(ret!=0) {
                System.out.println(ret);
            }
        }
    }
}
