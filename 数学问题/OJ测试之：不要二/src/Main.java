import java.util.Scanner;

/**
 * Created by L.jp
 * Description:二货小易有一个W*H的网格盒子，网格的行编号为0~H-1，网格的列编号为0~W-1。每个格子至多可以放一块蛋糕，
 * 任意两块蛋糕的欧几里得距离不能等于2。
 * 对于两个格子坐标(x1,y1),(x2,y2)的欧几里得距离为:
 * ( (x1-x2) * (x1-x2) + (y1-y2) * (y1-y2) ) 的算术平方根
 * 小易想知道最多可以放多少块蛋糕在网格盒子里。
 * 输入描述：
 * 每组数组包含网格长宽W,H，用空格分割.(1 ≤ W、H ≤ 1000)
 * 输出描述：
 * 输出一个最多可以放的蛋糕数
 * User: 86189
 * Date: 2022-01-22
 * Time: 14:44
 */
//根据题意，可知：(x1-x2)*(x1-x2)+(y1-y2)*(y1-y2)！=4，当这个表达式等于4时，是不能放蛋糕的
    //也就是有四种情况：
    //1+3=4,3+1=4,2+2=4,0+4=4,4+0=4，显然只有后面两种情况才有整数解，才符合要求
    //所以，当(x1-x2)^2=0即x1=x2,y1-y2=2,y1=y2+2时，这个坐标关系是不能放蛋糕的
    //还有就是当x1=x2+2,y1=y2时，也是不能放蛋糕的
public class Main {
    public static void main(String[] args) {
        Scanner scan=new Scanner(System.in);
        int w=scan.nextInt();
        int h=scan.nextInt();
        //定义一个二维数组存放蛋糕
        int[][] array=new int[w][h];
        int count=0;
        for(int i=0;i<w;i++){
            for(int j=0;j<h;j++){
                //我们把可以放蛋糕设置为0，不能放蛋糕设置为1
                if(array[i][j]==0){
                    //如果当前坐标可以放蛋糕，那么他的欧几里得距离的坐标就不能放蛋糕
                    count++;
                    //第一种欧几里得坐标不能放设置为1，当然还要判断坐标合法性
                    if(i+2<w){
                        array[i+2][j]=1;
                    }
                    //第二种欧几里得坐标不能放设置为1，当然还要判断坐标合法性
                    if(j+2<h){
                        array[i][j+2]=1;
                    }
                }
            }
        }
        System.out.println(count);
    }
}
