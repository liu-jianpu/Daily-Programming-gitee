/**
 * Created by L.jp
 * Description: 实现函数 int sqrt(int x).
 * 计算并返回 x 的平方根（向下取整）
 * User: 86189
 * Date: 2022-02-01
 * Time: 22:13
 */
public class Solution {
    public int sqrt (int x) {
        // write code here
        for(int i=0;i<=x;i++){
            if(i*i>x){
                return i-1;
            }
            if(i*i==x){
                return i;
            }
        }
        return 0;
    }
}
