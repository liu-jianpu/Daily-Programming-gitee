import java.util.Scanner;

/**
 * Created by L.jp
 * Description:完全数（Perfect number），又称完美数或完备数，是一些特殊的自然数。
 *
 * 它所有的真因子（即除了自身以外的约数）的和（即因子函数），恰好等于它本身。
 *
 * 例如：28，它有约数1、2、4、7、14、28，除去它本身28外，其余5个数相加，1+2+4+7+14=28。
 *
 * 输入n，请输出n以内(含n)完全数的个数。
 * User: 86189
 * Date: 2022-03-11
 * Time: 18:50
 */
public class Main {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        while (scan.hasNext()) {
            int n = scan.nextInt();
            int ret = 0;
            //循环从1~n的数
            for (int i = 1; i <= n; i++) {
                int sum = 0;//约数累加和,如果累加和等于i本身，那么就有计数器++
                //判断从1~i（不含i）的约数，并且计数
                for (int j = 1; j < i; j++) {
                    if (i % j == 0) {
                        sum += j;//累加约数
                    }
                }
                //约数判断完毕，判断是不是完美数
                if (sum == i) {
                    ret++;
                }
            }
            System.out.println(ret);

        }

    }
}
