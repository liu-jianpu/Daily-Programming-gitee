import java.util.Scanner;

/**
 * Created by L.jp
 * Description:根据输入的日期，计算是这一年的第几天。
 * User: 86189
 * Date: 2022-03-09
 * Time: 18:42
 */
public class Main {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        while (scan.hasNext()) {
            int y = scan.nextInt();
            int m = scan.nextInt();
            int d = scan.nextInt();
            //每个月的天数
            int[] days = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
            int sum = 0;
            for (int i = 0; i < m - 1; i++) {
                sum += days[i];
            }
            sum += d;
            if (m > 2 && (y % 4 == 0 && y % 100 != 0 || y % 400 == 0)) {
                sum += 1;
            }
            System.out.println(sum);
        }
    }
}

