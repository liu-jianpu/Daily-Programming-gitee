/**
 * Created by L.jp
 * Description:在地下室里放着n种颜色的手套，手套分左右手，但是每种颜色的左右手手套个数不一定相同。A先生现在要出门，所以他要去地下室选手套。但是昏暗的灯光让他无法分辨手套的颜色，只能分辨出左右手。所以他会多拿一些手套，然后选出一双颜色相同的左右手手套。现在的问题是，他至少要拿多少只手套(左手加右手)，才能保证一定能选出一双颜色相同的手套。
 *
 * 给定颜色种数n(1≤n≤13),同时给定两个长度为n的数组left,right,分别代表每种颜色左右手手套的数量。数据保证左右的手套总数均不超过26，且一定存在至少一种合法方案。
 *
 * 测试样例：
 * 4,[0,7,1,6],[1,5,0,6]
 * 返回：10(解释：可以左手手套取2只，右手手套取8只)
 * User: 86189
 * Date: 2022-03-10
 * Time: 18:03
 */
public class Gloves {
    public static int findMinimum(int n, int[] left, int[] right) {
        // write code here
        int leftSum=0;
        int rightSum=0;
        int sum=0;
        int leftMin=Integer.MAX_VALUE;
        int rightMin=Integer.MAX_VALUE;
        for(int i=0;i<n;i++){
            //排除手套中有0的情况，如果有手套的个数为0，那么就需要把对应手套的个数加到总数中
            if(left[i]*right[i]==0){
                sum=sum+left[i]+right[i];
            }else{
                leftSum+=left[i];
                rightSum+=right[i];
                //求出两个数组最小值
                if(leftMin>left[i]){
                    leftMin=left[i];
                }
                if(rightMin>right[i]){
                    rightMin=right[i];
                }
            }
        }
        //当有手套的数量为0时，我们再从求出的至少的数量中再取数量为0的对应的手套的数量，这样就能保证至少能取出颜色一致的手套
        //要保证取出的每种颜色手套都覆盖的话，那么就需要先取出所有手套，然后减掉数量最少的那种手套
        //最后再加上一个就能保证至少拿出的手套数量
        //最后结果是求出取出的左右手的数量的最小值，然后在另一只手套中随便取一只，就能保证一定能匹配上
        return sum+Math.min(leftSum-leftMin+1,rightSum-rightMin+1)+1;
    }

    public static void main(String[] args) {
        int [] left={0,7,1,6};
        int[] right={1,5,0,6};
        System.out.println(findMinimum(4, left, right));
    }
}
