import java.util.Scanner;

/**
 * Created by L.jp
 * Description:
 *                       1
 *                  1    1    1
 *               1  2    3    2    1
 *            1  3  6    7    6    3     1
 *         1  4  10 16   19   16   10    4    1
 *
 *
 *
 *

 以上三角形的数阵，第一行只有一个数1，以下每行的每个数，是恰好是它上面的数、左上角数和右上角的数，3个数之和（如果不存在某个数，认为该数就是0）。

 求第n行第一个偶数出现的位置。如果没有偶数，则输出-1。例如输入3,则输出2，输入4则输出3，输入2则输出-1。
 * User: 86189
 * Date: 2022-03-11
 * Time: 22:51
 */
public class Main {
    public static void main(String[] args) {
        Scanner scan=new Scanner(System.in);
        while (scan.hasNext()){
            //要求根据输入的第n行求出第n行的偶数第一次出现在什么位置
            int n=scan.nextInt();
            //根据规律可知，行和列的关系是m=2n-1
            //因为我们需要求的是第n行的偶数的位置，所以我们就构造一个n行的数组
            int m=2*n-1;
            //构造一个二维数组存放n行m列数值
            int[][] arr=new int[n][m];
            //首先我们对每一行的每一列都赋予初始值0，这样方便我们后面计算，因为我们的杨辉三角有的值是没有的，我们就假设他为0
            for(int i = 0; i < n; i++){
                for(int j = 0; j < m; j++){
                    arr[i][i]=0;
                }
            }
            //接下来在数组中构造这个杨辉三角
            arr[0][0]=1;//构造第一行第一列的值
            for(int i = 1; i < n; i++){
                //构造每一行的第一列和每一行的最后一列值为1
                arr[i][0]=arr[i][2*i]=1;
                //接下来从每一行的第二列开始构造
                for(int j = 1; j < 2*i; j++){
                    //注意看清楚杨辉三角，当前行的值等于上面的值（i-1,j-1) + 左上角的值（i-1,j-2)  +右上角的值（i-1,j)
                    //但是第二列的话只有上面的值和右上角的值相加，没有左上角的值，因为j-2会溢出数组
                    if(j == 1){
                        arr[i][j] = arr[i-1][j]+arr[i-1][j-1];
                    }else{
                        arr[i][j]=arr[i-1][j-2]+arr[i-1][j-1]+arr[i-1][j];
                    }
                }
            }
            //构造杨辉三角完了，就在第n行找第一个偶数出现的位置
            int k=0;
            for(;k<m;k++){
                if(arr[n-1][k]%2==0){
                    System.out.println(k+1);
                    break;
                }
            }
            if(k ==m){
                System.out.println(-1);
            }
        }
    }
}
