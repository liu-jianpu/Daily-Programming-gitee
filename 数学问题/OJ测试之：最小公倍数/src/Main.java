import java.util.Scanner;

/**
 * Created by L.jp
 * Description:
 * User: 86189
 * Date: 2022-03-01
 * Time: 18:08
 */
public class Main {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int a=scan.nextInt();
        int tmpa=a;//临时的值用在循环里面求最大公约数
        int b = scan.nextInt();
        int tmpb=b;//临时的值用在循环里面求最大公约数
        while(tmpa%tmpb != 0){
            int c=tmpa %tmpb;
            tmpa=tmpb;
            tmpb=c;
        }
        int tmp=tmpb;//当余数是0时，那么此时的除数就是最大公约数
        System.out.println((a*b)/tmp);//注意这里的a和b要是最开始的值，不能直使用循环里面的
    }
}
