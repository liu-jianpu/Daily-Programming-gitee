import java.util.Scanner;

/**
 * Created by L.jp
 * Description:
 * 一个正整数可以分解成一个或多个数组的积。例如36=2*2*3*3，即包含2和3两个因子。
 * NowCoder最近在研究因子个数的分布规律，现在给出一系列正整数，他希望你开发一个程序输出每个正整数的因子个数
 * User: 86189
 * Date: 2022-03-27
 * Time: 18:22
 * 示例1
 *
 * 输入
 * 30
 * 26
 * 20
 *
 * 输出
 * 3
 * 2
 * 2
 */
//经过示例发现，1不能作为因子之一，这个求解因子的过程比较复杂，我们拿到一个数n,需要从2~sqrt(n)之间求出因子数，首先需要找到因子，如果找到了，需要
    //不断的去除以这个因子，同时这个n也是跟随因子变化的，直到不能被因子整除为止，那么说明找到了一个因子，然后再从2~sqrt(n)，这个n是新的n,在这个
    //区间再去找因子，如果找不到那么就判断n是否为1，如果不为1说明当前的n是一个素数，也可以作为1个因子，如果当前的n是1，那么就说明全部的因子已经被找到
public class Main {
    public static void main(String[] args) {
        Scanner scanner=new Scanner(System.in);
        while (scanner.hasNextInt()){
            int n=scanner.nextInt();
            int count=0;//统计因子数
            for(int i=2;i<Math.sqrt(n);i++){
                //能找到因子
                if(n%i==0) {
                    //循环除以这个因子，判断是不是符合条件的因子
                    while (n % i == 0) {
                        //n也需要循环改变
                        n /= i;
                    }
                    //直到不能整除，说明找到一个因子
                    count++;
                }
            }
            //最终的n如果是素数结果肯定不为1,因子个数也需要++;
            if(n!=1){
                count++;
            }
            //打印数量
            System.out.println(count);

        }
    }
}
