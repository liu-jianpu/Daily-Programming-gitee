/**
 * Created by L.jp
 * Description:对于一个矩阵，请设计一个算法从左上角(mat[0][0])开始，顺时针打印矩阵元素。
 *
 * 给定int矩阵mat,以及它的维数nxm，请返回一个数组，数组中的元素为矩阵元素的顺时针输出。
 * User: 86189
 * Date: 2022-04-24
 * Time: 15:59
 */
public class Printer {
    public static int[] clockwisePrint(int[][] mat, int n, int m) {
        //从数组左上角开始打印，顺序是先打印最上面的一行，然后打印最后一列，然后打印最下面的一行，最后打印最左边的一列
        //按照这个顺序把原数组的每个元素都存放到一个新的数组中，
        //顺时针打印完一轮之后，进入下一轮
        int [] res = new int[n*m];
        int index = 0; //新数组下标
        int x1=0,y1=0; //左上角下标
        int x2=n-1,y2=m-1; //右下角下标
        while(x1<=x2 && y1<=y2){
            //从左到右打印第一行
            for(int i=y1;i<=y2;i++){
                res[index++]=mat[x1][i];
            }
            //从上到下打印最后一列
            for(int i=x1+1;i<=x2;i++){
                res[index++]=mat[i][y2];
            }
            //从右到左打印最下面的一行
            //确保有两行及以上
            if(x1<x2) {
                for (int i = y2 - 1; i >= y1; i--) {
                    res[index++] = mat[x2][i];
                }
            }
            //从下到上打印最左边的一列
            //确保有两列及以上
            if(y1<y2){
                for(int i=x2-1;i>x1;i--){
                    res[index++]=mat[i][y1];
                }
            }
            //下一轮，往里面走
            x1++;
            y1++;
            x2--;
            y2--;
        }
        return res;
    }
    
    public static void main(String[] args) {
        int[][] mat = {{1,2,3,4},{5,6,7,8},{9,10,11,12},{13,14,15,16}};
        int[] res = clockwisePrint(mat,4,4);
        for(int i=0;i<res.length;i++){
            System.out.print(res[i]+" ");
        }
    }
}
