import java.math.BigDecimal;
import java.util.Scanner;

/**
 * Created by L.jp
 * Description:给定三条边，请你判断一下能不能组成一个三角形。
 * 输入描述:
 * 输入包含多组数据，每组数据包含三个正整数a、b、c（1≤a, b, c≤10^100）。
 *
 *
 * 输出描述:
 * 对应每一组数据，如果它们能组成一个三角形，则输出“Yes”；否则，输出“No”。
 * User: 86189
 * Date: 2022-03-24
 * Time: 18:34
 */
//由于n的取值范围较大，使用int和long都不能满足，所以可以使用double
    //取值范围  int   最大 2^31-1           long  最大 2^63-1   float  最大2^128   double最大 2^1024
    //这些的包装类的取值范围也是一样的，他们是lang包中的类
    //还可以使用java.math.BigInteger或者BidDecimal就是用来表示任意大小的整数。BigInteger内部用一个int[]数组来模拟一个非常大的整数：
//    BigInteger bi = new BigInteger("1234567890");
//            System.out.println(bi.pow(5)); // 2867971860299718107233761438093672048294900000
    //BigDecimal和BigDecimal可以表示一个任意大小且精度完全准确的浮点数.
    //对BigInteger做运算的时候，只能使用实例方法
public class Main {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        while (sc.hasNext()) {
//            double a=sc.nextDouble();
//            double b = sc.nextDouble();
//            double c=sc.nextDouble();
//            if(a+b>c&& a+c>b&& b+c>a){
//                System.out.println("Yes");
//            }else {
//                System.out.println("No");
//            }
//        }
            BigDecimal a = sc.nextBigDecimal();
            BigDecimal b = sc.nextBigDecimal();
            BigDecimal c = sc.nextBigDecimal();
            //add表示 加法运算，subtract是减法运算  multiply是乘法运算  divide是除法运算
            //compareTo结果大于0说明前者大，小于0说明后者大，等于0说明一样
            if (a.add(b).compareTo(c) > 0 && b.add(c).compareTo(a) > 0 && a.add(c).compareTo(b) > 0) {
                System.out.println("Yes");
            } else {
                System.out.println("No");
            }
        }
    }
}