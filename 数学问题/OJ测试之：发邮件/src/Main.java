import java.util.Scanner;

/**
 * Created by L.jp
 * Description:链接：https://www.nowcoder.com/questionTerminal/95e35e7f6ad34821bc2958e37c08918b
 * 来源：牛客网
 *
 * NowCoder每天要给很多人发邮件。有一天他发现发错了邮件，把发给A的邮件发给了B，把发给B的邮件发给了A。于是他就思考，要给n个人发邮件，在每个人仅收到1封邮件的情况下，有多少种情况是所有人都收到了错误的邮件？
 * 即没有人收到属于自己的邮件。
 * User: 86189
 * Date: 2022-04-15
 * Time: 18:37
 */
public class Main {
    public static void main(String[] args) {
        //这是一个错排的问题，我们要把n个信封错排，假设n个信封错排的情况是D(n)种
        //那么他有两种情况，假设A的信封给了B，B把信封给了A，那么剩下的n-2个信封就有D（n-2）种情况
        //假设B错装在了其他的信封，那么就与A无关，就有D（n-1)种错装方法
        //对于每一个信封都跟A的情况相同，所以总的错装的情况就是D（n)=(n-1) (D(n-1）+D（n-2));
        /*  就像是给n个元素编号一样，我们把编错的情况称为D(n)
            编错的情况分为两种：
                当n个编号元素放在n个编号位置，元素编号与位置编号各不对应的方法数用D(n)表示，那么D(n-1)就表示n-1个编号元素放在n-1个编号位置，各不对应的方法数，其它类推.
                第一步，把第n个元素放在一个位置，比如位置k，一共有n-1种方法；
                第二步，放编号为k的元素，这时有两种情况：⑴把它放到位置n，那么，对于剩下的n-1个元素，由于第k个元素放到了位置n，剩下n-2个元素就有D(n-2)种方法；⑵第k个元素不把它放到位置n，这时，对于这n-1个元素，有D(n-1)种方法；
                综上得到
                D(n) = (n-1) [D(n-2) + D(n-1)]
                特殊地，D(1) = 0, D(2) = 1.
        *
        * */
        
        long[] D=new long[21];
        D[2]=1;
        for(int i = 3;i<21;i++){
            D[i]=(i-1)*(D[i-1]+D[i-2]);
        }
        Scanner scanner = new Scanner(System.in);
        while (scanner.hasNext()) {
            int n=scanner.nextInt();
            System.out.println(D[n]);
        }
 
    }  
}
