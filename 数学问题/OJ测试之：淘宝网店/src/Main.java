import java.util.Scanner;

/**
 * Created by L.jp
 * Description:
 * owCoder在淘宝上开了一家网店。他发现在月份为素数的时候，当月每天能赚1元；否则每天能赚2元。
 * 现在给你一段时间区间，请你帮他计算总收益有多少。
 *
 * 输入描述:
 * 输入包含多组数据。
 *
 * 每组数据包含两个日期from和to (2000-01-01 ≤ from ≤ to ≤ 2999-12-31)。
 *
 * 日期用三个正整数表示，用空格隔开：year month day。
 *
 *
 * 输出描述:
 * 对应每一组数据，输出在给定的日期范围（包含开始和结束日期）内能赚多少钱。
 * 示例1
 * 输入
 * 2000 1 1 2000 1 31
 * 2000 2 1 2000 2 29
 * 输出
 * 62
 * 29
 * User: 86189
 * Date: 2022-03-30
 * Time: 17:11
 */
/*

      计算收益就是计算天数，如果是润年，那么因为二月本身是素数，所以润年的话，那么收益就要+1,如果对于不是素数的月，那么收益就是天数*2

      这种情况有三种情况：
      1.同年同月 ，那么就本月天数相减
      2.同年不同月，那么就是月数的天数相减
      3.如果是不同年，那么就分成三部分  ：  第一个年剩余的天数 的收益 +  往后一年到目标年前一年的几个整年的收益  +  目标年的几个月的收益


      比如计算 1957- 5-7  到1960 -8-9  的收益 ：  1957整年剩余天数的收益 + 1958—1959一年的收益 +  1960年 8月8日以前的收益
*
*
*
*
* */
public class Main {
    //判断是不是闰年，用于单独区分二月的收益
    public static boolean isLeapYear(int year){
        return  year%4==0 && year%100!=0 || year%400==0;
    }
    //判断是不是素数月，用于区分每个月的收益
    public  static boolean isPrime(int month){
        return month==2 || month==3 || month==5 || month==7 || month==11;
    }
    //计算整年的收益，就是每一的月的收益相加，如果是闰年，收益还要+1
    public static int profitAllYears(int year){
        return 2*31+28+(isLeapYear(year) ?  1 :0) +31+2*30+31+2*30+31+2*31+2*30+2*31+30+2*31;
    }
    //这个函数用于计算本年从1月1日到本月本日（这个日只能到前一天，不算当天的）的收益
    public  static  int profitThisyear(int year,int month,int day){
        //统计收益
        int profit=0;
        //先计算残余天数累计的收益,根据是不是素数月区分
        if(!isPrime(month)){
            profit=2*day;
        }else{
            profit=day;
        }
        //计算前面几个月的收益,月数先-1，然后循环减得到之前的月数
        while (--month>0){
            switch (month) {
                case 1: case 8: case 10: case 12:
                    profit+=62;
                    break;
                case 3: case 5: case 7:
                    profit+=31;
                    break;
                case 4: case 6: case 9:
                    profit+=60;
                    break;
                case 11:
                    profit+=30;
                    break;
                default:
                    profit+=(28+(isLeapYear(year) ? 1:0));
            }
        }
        return profit;
    }
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        while (input.hasNextInt()){
            int y1=input.nextInt();
            int m1=input.nextInt();
            int d1=input.nextInt();
            int y2=input.nextInt();
            int m2=input.nextInt();
            int d2=input.nextInt();
            //先计算第一个年的残余的收益
            int profit=profitAllYears(y1)-profitThisyear(y1,m1,d1-1);
            //再计算第二个年的累计到本月本日的收益
            profit+=profitThisyear(y2,m2,d2);
            //同年收益的计算
            if(y1 ==y2){
                //既然前面已经算了第一个年残余的收益，那么我们就利用这个条件去计算如果同年的收益
                profit-=profitAllYears(y1);     //中间的部分利用前面算出来的两个累加起来减去整个年的收益就是中间的收益
            }
            //然后是计算中间几个年的收益
            for(int i = y1+1;i<y2;i++){
                profit+=profitAllYears(i);
            }
            System.out.println(profit);
        }
    }
}
