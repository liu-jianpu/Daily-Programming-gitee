import java.util.Scanner;

/**
 * Created by L.jp
 * Description:
 * User: 86189
 * Date: 2022-05-17
 * Time: 12:20
 */
public class Main {
    //思路：分三份，对于数目不对三份，那就要向上取整，然后称最重的那一份
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        while (scanner.hasNextInt()){
            int n=scanner.nextInt();
            //0表示输入结束
            if(n==0){
                break;
            }
            int count=0;
            //每一次分摊的次数就是要称的次数，直到两边都剩下一个的时候就不用称了
            //我们的目的就是求出最重的那一份去称他就行了，每求一次就是称一次
            while (n>=2){
                //对于这个公式，我们每次都要把要称的分为3份，如果只剩2个盘子时，我们分不了三份，但是依旧要遵循那个公式，本来n/3==0;
                // 但是在下面这个表达式中表达式就变成了1，就会退出循环，结束计数
                //这个表达式的意思就是每分三份向上取整就得出最重的那一份，相当于称了一次
                n=(int) Math.ceil((double)n/3);   //ceil函数向上取整+1，但参数是double类型，我们需要返回的是整数类型，所以需要两次类型转换
                //Math.floor是向下取整的函数
                count++;
            }
            System.out.println(count);
        }
    }
}
