import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * Created by L.jp
 * Description:小易来到了一条石板路前，每块石板上从1挨着编号为：1、2、3.......
 * 这条石板路要根据特殊的规则才能前进：对于小易当前所在的编号为K的 石板，小易单次只能往前跳K的一个约数(不含1和K)步，即跳到K+X(X为K的一个非1和本身的约数)的位置。 小易当前处在编号为N的石板，他想跳到编号恰好为M的石板去，小易想知道最少需要跳跃几次可以到达。
 * 例如：
 * N = 4，M = 24：
 * 4->6->8->12->18->24
 * 于是小易最少需要跳跃5次，就可以从4号石板跳到24号石板
 * User: 86189
 * Date: 2022-03-08
 * Time: 18:39
 */
public class Main {
    public static void main(String[] args) {
        Scanner scan=new Scanner(System.in);
        //动态规划做法，用一个数组存储跳到当前石板最少需要多少步
        while(scan.hasNext()){
            int n=scan.nextInt();
            int m=scan.nextInt();
            int[] dp=new int[m+1];
            //先设置数组所有的元素为整型最大值，这样才可以保证我们等会能去到最小值，如果置为0的话，那么我们的最小步数就没有意义了
            for(int i=0;i<dp.length;i++){
                dp[i]=Integer.MAX_VALUE;

            }
            //设置起点初始值为0；
            dp[n]=0;
            //开始求最小步数
            //第一层循环是遍历所有的石板
            for(int i=n;i<m;i++){
                //如果当前石板是初始值，表示达不到这块石板，跳过他，如果没有这段代码的话，那么就会导致能跳到的石板的值发生改变
                if(dp[i]==Integer.MAX_VALUE){
                    continue;
                }
                //获取到当前石板编号的所有约数
                List<Integer> list=getNum(i);
                //遍历所有约数，表示能到达的所有石板，并给石板的步数赋值
                for(int j:list){
                    if(i+j<=m && dp[i+j]!=Integer.MAX_VALUE){
                        //没有到达石板，并且下一个石板的初始值已经被改变，那么我们只取最小值
                        dp[i+j]=Math.min(dp[i+j],dp[i]+1);
                    }else if(i+j<=m){
                        dp[i+j]=dp[i]+1;
                    }

                }
            }
            //到达不了
            if(dp[m]==Integer.MAX_VALUE){
                System.out.println(-1);
            }else{
                //能到达
                System.out.println(dp[m]);
            }
        }
    }
    //求一个数的约数的函数
    public static List<Integer> getNum(int m){
        List<Integer> list=new ArrayList<>();
        for(int i=2;i*i<=m;i++){  //i*i<=m保证只加入一遍数据到列表，不会重复加入
            if(m%i==0){
                list.add(i);
                if(m/i!=i){
                    list.add(m/i);
                }
            }
        }
        return list;
    }
}
