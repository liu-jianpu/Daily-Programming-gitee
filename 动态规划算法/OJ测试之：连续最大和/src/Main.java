import java.util.Scanner;

/**
 * Created by L.jp
 * Description:
 * User: 86189
 * Date: 2022-01-21
 * Time: 19:49
 */
public class Main {
    public static void main(String[] args) {
        Scanner scaner=new Scanner(System.in);
        int n=scaner.nextInt();
        int [] array=new int[n];
        for(int i=0;i<n;i++){
            array[i]=scaner.nextInt();
        }
        //利用动态规划算法，首先确定状态转移方程：dp[i]表示以i下标结尾的连续最大和
        //那么状态转移方程就是dp[i]=Math.max(dp[i-1]+array[i],array[i]);
        //接下来确定状态初始化
        //int [] dp=new int[n];
        //dp[0]=array[0];

        //优化：利用一个变量代替dp数组
        int total=array[0];
        //最大和
        int max=array[0];
        //从数组中找连续最大和
        for(int i=1;i<n;i++){
            //dp[i]=Math.max(dp[i-1]+array[i],array[i]);
            //优化
            total=Math.max(total+array[i],array[i]);
            max=Math.max(total,max);

            //更新最大和
            //if(max<dp[i]){
            //max=dp[i];
            //}
        }
        System.out.println(max);
    }
}
