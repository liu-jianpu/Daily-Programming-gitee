

import java.util.Arrays;
import java.util.Comparator;
import java.util.Scanner;

/**
 * Created by L.jp
 * Description:搜狐员工小王最近利用假期在外地旅游，在某个小镇碰到一个马戏团表演，
 * 精彩的表演结束后发现团长正和大伙在帐篷前激烈讨论，小王打听了下了解到， 马戏团正打算出一个新节目“最高罗汉塔”，
 * 即马戏团员叠罗汉表演。考虑到安全因素，要求叠罗汉过程中，站在某个人肩上的人应该既比自己矮又比自己瘦，或相等。
 * 团长想要本次节目中的罗汉塔叠的最高，由于人数众多，正在头疼如何安排人员的问题。
 * 小王觉得这个问题很简单，于是统计了参与最高罗汉塔表演的所有团员的身高体重，
 * 并且很快找到叠最高罗汉塔的人员序列。
 * 现在你手上也拿到了这样一份身高体重表，请找出可以叠出的最高罗汉塔的高度，这份表中马戏团员依次编号为1到N。
 * 输入描述：
 * 首先一个正整数N，表示人员个数。 之后N行，每行三个数，分别对应马戏团员编号，体重和身高。
 * 输出描述：
 * 正整数m，表示罗汉塔的高度。
 * User: 86189
 * Date: 2022-04-23
 * Time: 18:39
 */
//继承Comparable类，定制比较身高的规则
//class Circus implements Comparable<Circus>{
//    public int w;
//    public int h;
//    public Circus(){
//
//    }
//    public Circus(int w,int h){
//        this.w = w;
//        this.h = h;
//    }
//    @Override
//    //实现比较规则，并没有排序的功能
//    public int compareTo(Circus o) {
//        int ret=w-o.w;
//        if(ret == 0){
//            //体重相等，身高降序排序规则
//            return o.h-h;
//        }
//        //其他的都是体重升序规则
//        return ret;
//    }
//}

//适用于comparator.compare的类构造
//class  Circus{
//        public int w;
//        public int h;
//        public Circus(int w,int h){
//            this.w=w;
//            this.h=h;
//        }
//}
public class Main {  //这个题对应着最长上升子序列（一） ，只用动态规划就可以解决问题，不用二分法求解
    /*  关键点：要求叠罗汉过程中，站在某个人肩上的人应该既比自己矮又比自己瘦，或相等。
        也就说如果体重不同，上面的人体重必须比下面的人轻，身高比下面矮
        如果体重相同，上面的人必须和上面的人身高相同才可以叠罗汉、
        也就是说从下面到上面，体重和身高是要递减的，当上下有体重相同时，那么身高也要相同才能叠加
        否则体重相等的两个，我们只能选择一个人
        这是一个求叠罗汉的高度的题，高度其实就是人的数
        所以这就可以看出一个求最长上升子序列的题
        对于递增子序列，我们这里有两个属性，一个是体重，一个是身高，我们先按体重升序排序，这样就先保证了体重的叠罗汉
        然后再求身高的递增子序列，当有体重相同的时候，我们要保证身高是递增的，且体重相同的如果身高不相同，那么我们只能选一个，
        所以体重相等时，我们取身高降序，这样就保证了身高不相等时只取一个，这样就是实现了身高的递增子序列
    *
    *
    * */
    //求最长上升子序列的模板
    //使用类数组
//    public static  int getMaxLen(int n,Circus[] arr){
//        //有了Circus类的比较规则之后，就可以使用sort函数对数组的Circus对象进行排序，不继承Comparable接口的话，也可以在这里使用comparator的compare方法构造比较规则
//        Arrays.sort(arr,new Comparator<Circus>() {
//            @Override
//            public int compare(Circus o1, Circus o2) {
//                //体重相同，对身高降序，否则是按体重升序
//                int ret=o1.w-o2.w;
//                return ret==0 ? o2.h-o1.h : ret;
//            }
//        });
//        //至少有一个人
//        int ret=1;
//        int[] dp=new int[n];
//        dp[0]=1;
//        for(int i=1;i<n;i++){
//            //从第二个开始比较
//            dp[i]=1;
//            for(int j = 0; j < i; j++){
//                //只有i与j身高相等或者i比j高才会加入递增子序列
//                if(arr[i].h>=arr[j].h) {
//                    //说明最长递增子序列长度+1，这个dp[j]既包含了长度，也包含着对应的递增子序列
//                    //所以在求dp[i]的时候，dp[i]取的是与前面的身高构成递增子序列的长度+1构成的最长长度
//                    dp[i] = Math.max(dp[i], dp[j] + 1);  //局部最大值
//                }
//            }
//            //不断更新整体最大值
//            ret=Math.max(dp[i],ret);
//        }
//        return ret;
//    }
    
    //使用二维数组
    public static  int getMaxLen(int n,int[][] arr){
        //首先实现比较规则，然后排序
        Arrays.sort(arr, new Comparator<int[]>() {
            @Override
            public int compare(int[] o1, int[] o2) {
                //默认按体重升序，体重相同时按身高降序，才可以保证体重相同时只选一个人
                int  ret=o1[0]-o2[0];
                return  ret==0 ? o2[1]-o1[1] :ret;
            }
        });
        //接下来对身高查找递增子序列
        int ret=1;
        int[] dp=new int[n];
        //第一个人没有办法比较，最长就是自己
        dp[0]=1;
        for(int i = 1; i < n; i++){  //从第二个人开始比较
            dp[i]=1; //每一个人默认的长度就是自己一个人，1
            for(int j=0;j<i;j++){
                if(arr[i][1]>=arr[j][1]){ //[i][1]表示当前人的身高属性，[j][1]表示前面的人的身高属性
                    dp[i]=Math.max(dp[j]+1,dp[i]);
                }
            }
            ret=Math.max(ret,dp[i]);
        }
        return  ret;
    }
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        while (scan.hasNext()) {
            int  n=scan.nextInt();
            //也可以使用二维整型数组存储身高和体重二个属性
            int[][] arr=new int[n][2];
            //使用类数组存储属性
            //Circus[] arr=new Circus[n];
            for(int i = 0; i < n; i++){
                //叠罗汉不要求id必须相邻，所以对id不用排序，随意取
                int id=scan.nextInt();
                //对每一个马戏团对象进行输入体重和身高
                //使用类数组存储
               // arr[i]=new Circus(scan.nextInt(), scan.nextInt());
                //使用整型数组存储输入
                arr[i]=new int[]{scan.nextInt(), scan.nextInt()};
            }
            System.out.println(getMaxLen(n, arr));
        }
    }
   
}
