import java.util.Scanner;

/**
 * Created by L.jp
 * Description: 查找两个字符串a,b中的最长公共子串。若有多个，输出在较短串中最先出现的那个。
 * 注：子串的定义：将一个字符串删去前缀和后缀（也可以不删）形成的字符串。请和“子序列”的概念分开！
 * 示例1
 * 输入：
 * abcdefghijklmnop
 * abcsafjklmnopqrstuvw
 * 复制
 * 输出：
 * jklmnop
 *
 * User: 86189
 * Date: 2022-03-15
 * Time: 12:41
 */

//对于这种两个字符串的关系问题，求最长公共子串可以采用动态规划求解，就是一个数组存放了字符串的长度，然后定义一个最长变量，实时更新最长长度
    //在这个过程中需要找到这个最长字符串的起始位置就是当前的下标减去最长长度，那么就可以根据这个下标使用subString找到在较短字符串中分割的子串，
    //或者使用indexOf根据下标和长度返回字符串
public class Main {
    //这里我们假设str1是较短的那个
    public static String maxSubstring(String str1,String str2){
        char[] s1=str1.toCharArray();
        char[] s2=str2.toCharArray();
        int len1=s1.length;
        int len2 = s2.length;
        int maxLen=0;//存储最长长度
        int  start=0;//最长长度字符串开始出现的起始位置
        //这里加1是借助了空串时的状态，因为我们要得出s1的前i个字符和s2的前j个字符的最长公共子串长度，在二维数组的定义上就要借助前一个的状态
        int [][] dp=new int[len1+1][len2+1];
        //构造数组
        for(int i=1;i<=len1; i++){
            for(int j = 1; j <= len2; j++){
                //如果s1的第i个字符个s2的第j个字符相等，那么分别这两个字符结尾的字符串的最长公共子串长度只需要在前面的基础上+1即可
                if(s1[i-1]==s2[j-1]){   //对于字符数组来说字符下标和数组下标的差值为1
                    dp[i][j]=dp[i-1][j-1]+1;  //以s1的第i个字符结尾和s2的第j个字符结尾的最长公共子串的长度就是前一个的状态+1
                    //更新长度
                    if(maxLen<dp[i][j]){
                        maxLen = dp[i][j];
                        //更新最长公共子串的起始位置
                        start=i-maxLen;
                    }
                }
            }
        }
        //返回以起始位置开始，以起始位置+最大长度的子字符串
        return str1.substring(start,start+maxLen);
    }
    public static void main(String[] args) {
         Scanner scanner = new Scanner(System.in);
         String str1 = scanner.nextLine();
         String str2 = scanner.nextLine();
         //因为结果需要是在较短的字符串中出现的最长的一个公共子串，所以需要判断谁更长
        //第一个参数是较短的那个
        if(str1.length()>str2.length()){
            System.out.println(maxSubstring(str1, str2));
        }else{
            System.out.println(maxSubstring(str2, str1));
        }
    }
}
