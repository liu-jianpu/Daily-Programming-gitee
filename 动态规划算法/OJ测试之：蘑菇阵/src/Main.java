import java.util.Scanner;
/**
 * Created by L.jp
 * Description:链接：https://www.nowcoder.com/questionTerminal/ed9bc679ea1248f9a3d86d0a55c0be10
 * 来源：牛客网
 *
 * 现在有两个好友A和B，住在一片长有蘑菇的由n＊m个方格组成的草地，A在(1,1),B在(n,m)。现在A想要拜访B，由于她只想去B的家，
 * 所以每次她只会走(i,j+1)或(i+1,j)这样的路线，在草地上有k个蘑菇种在格子里(多个蘑菇可能在同一方格),问：A如果每一步随机选择的话(若她在边界上，则只有一种选择)，那么她不碰到蘑菇走到B的家的概率是多少？
 * User: 86189
 * Date: 2022-04-12
 * Time: 15:37
 */
public class Main {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        while (scan.hasNext()) {
            int N = scan.nextInt();
            int M = scan.nextInt();
            int K = scan.nextInt();
            int[][] mg = new int[N + 1][M + 1];
            while (0 != K) {
                int x = scan.nextInt();
                int y = scan.nextInt();
                mg[x][y] = 1;
                --K;
            }
            //因为A是每一步都是随机走的，但是他只能有两个选择，要么往右走，要么往下走，不在最后一行或者最后一列走时，有两种走法，
            // 所以往下走和往右走的概率都是0.5
            //但是在最后一列不能往右走和最后一行走不能往下走的时候，只有一种可能，概率就是1

            //定义一个数组，用来存放每一步的概率
            //根据题目，A从（1,1）开始走，但是数组第一行第一列是（0,0）,所以要多加一列，保证和题目一致
            double[][] dp = new double[N + 1][M + 1];
            //初始化A（1,1）表示肯定可以走到，因为从这里开始走起
            dp[1][1] = 1.0;
            //开始构造数组
            for (int i = 1; i <= N; i++) {
                for (int j = 1; j <= M; j++) {
                    //从1,1开始走
                    if (!(i == 1 && j == 1)) {
                        dp[i][j] = dp[i - 1][j] * (j == M ? 1 : 0.5) + dp[i][j - 1] * (i == N ? 1 : 0.5);
                    }
                    if (mg[i][j] == 1) {
                        dp[i][j] = 0.0;
                    }
                }
            }
            System.out.printf("%.2f\n", dp[N][M]);
        }
    }
}
