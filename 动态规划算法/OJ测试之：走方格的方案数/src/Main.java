import java.util.Scanner;

/**
 * Created by L.jp
 * Description:请计算n*m的棋盘格子（n为横向的格子数，m为竖向的格子数）从棋盘左上角出发沿着边缘线从左上角走到右下角，总共有多少种走法，要求不能走回头路，即：只能往右和往下走，不能往左和往上走。
 *
 * 注：沿棋盘格之间的边缘线行走
 * User: 86189
 * Date: 2022-03-02
 * Time: 20:23
 */
public class Main {
    //法一：动归
    /*public static  int getNum(int n,int m){
        //根据分析可以采用动归的解法
        int[][] dp=new int[n+1][m+1];
        //初始化到第一行的每一个位置的个数为1
        for(int i=0;i<=n;i++){
            dp[i][0]=1;
        }
        //初始化到第一列的每一个位置的个数为1
        for(int j = 0; j <=m; j++){
            dp[0][j]=1;
        }

        for(int i=1;i<=n; i++){
            for(int j = 1; j <=m; j++){
                //状态定义方程
                dp[i][j]=dp[i-1][j]+dp[i][j-1];
            }
        }
        return dp[n][m];//结果
    }*/
   //法二：递归的解法，当n==1,m>=1时，ret=m+n;  当m==1,n>=1时，ret=m+n ;
       //当n>1 ,m>1时，ret=getNum(m-1,n)+getNum(n-1,m)
     //递归结束的条件就是当n==1或者m==1时，返回m+n即可
    public static int  getNum(int a,int b){
        if(a==1 || b==1){
            return a+b;
        }
        return getNum(a-1,b)+getNum(a,b-1);
    }
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        while(scan.hasNext()) {
            int a = scan.nextInt();//行
            int b = scan.nextInt();//列
            System.out.println(getNum(a, b));
        }
    }
}
