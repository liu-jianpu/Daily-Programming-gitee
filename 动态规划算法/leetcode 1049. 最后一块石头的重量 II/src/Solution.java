/**
 * Created by L.jp
 * Description:有一堆石头，用整数数组stones 表示。其中stones[i] 表示第 i 块石头的重量。
 *
 * 每一回合，从中选出任意两块石头，然后将它们一起粉碎。假设石头的重量分别为x 和y，且x <= y。那么粉碎的可能结果如下：
 *
 * 如果x == y，那么两块石头都会被完全粉碎；
 * 如果x != y，那么重量为x的石头将会完全粉碎，而重量为y的石头新重量为y-x。
 * 最后，最多只会剩下一块 石头。返回此石头 最小的可能重量 。如果没有石头剩下，就返回 0。
 * User: 86189
 * Date: 2022-06-06
 * Time: 22:55
 */
public class Solution {
    public static int lastStoneWeightII(int[] stones) {
        int sum = 0;
        for (int i : stones) {
            sum += i;
        }
        int target = sum >> 1;
        //初始化 dp 数组
        int[] dp = new int[target + 1];
        for (int i = 0; i < stones.length; i++) {
        //采用倒序
            for (int j = target; j >= stones[i]; j--) {
//两种情况，要么放，要么不放
                dp[j] = Math.max(dp[j], dp[j - stones[i]] + stones[i]);
            }
        }
        return sum - 2 * dp[target];
    }
    
    public static void main (String[] args) {
        int[] stones={2,7,4,1,8,1};
        System.out.println( lastStoneWeightII( stones ) );
    }
}

