
import java.util.Scanner;

/**
 * Created by L.jp
 * Description:给定两个只包含小写字母的字符串，计算两个字符串的最大公共子串的长度。
 *
 * 注：子串的定义指一个字符串删掉其部分前缀和后缀（也可以不删）后形成的字符串。
 * User: 86189
 * Date: 2022-03-15
 * Time: 22:59
 */
//这题和查找a,b的最长公共子串的方法是一样的，都是用动态规划做法，用一个数组存储存储公共串的长度
public class Main {
    public static void main(String[] args) {
        Scanner scanner=new Scanner(System.in);
        String str1=scanner.nextLine();
        String str2 = scanner.nextLine();
        char[] s1=str1.toCharArray();
        char[] s2 = str2.toCharArray();
        System.out.println(getmaxLen(s1, s2));
    
    }
    public static int getmaxLen(char[] s1,char[] s2) {
        int l1 = s1.length;
        int l2 = s2.length;
        int maxLen = 0;
        //我们需要借助空串的转态来构建后面的状态
        int[][] dp = new int[l1 + 1][l2 + 1];
        //对于[0,j]和[i][0]这些地方都表示空串没有和任何字符串有公共字符，所以长度是0
        for (int i = 1; i <= l1; i++) {
            for (int j = 1; j <= l2; j++) {
                //只有两个字符串的当前位置字符相等时，才能表示最长公共子串的长度
                // 表示以s1的第i个字符结尾和以s2第j个字符结尾的字符串的最长公共子串的长度
                if (s1[i - 1] == s2[j - 1]) {
                    dp[i][j] = dp[i - 1][j - 1] + 1;
                    if (maxLen < dp[i][j]) {
                        maxLen = dp[i][j];
                    }
                }
            }
        }
        return maxLen;
    }
}
