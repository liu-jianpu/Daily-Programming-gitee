import java.util.ArrayDeque;
import java.util.Arrays;
import java.util.Deque;
import java.util.Scanner;

/**
 * Created by L.jp
 * Description:一个袋子里面有n个球，每个球上面都有一个号码(拥有相同号码的球是无区别的)。如果一个袋子是幸运的当且仅当所有球的号码的和大于所有球的号码的积。
 * 例如：如果袋子里面的球的号码是{1, 1, 2, 3}，这个袋子就是幸运的，因为1 + 1 + 2 + 3 > 1 * 1 * 2 * 3
 * 你可以适当从袋子里移除一些球(可以移除0个,但是别移除完)，要使移除后的袋子是幸运的。现在让你编程计算一下你可以获得的多少种不同的幸运的袋子。
 * User: 86189
 * Date: 2022-03-09
 * Time: 18:17
 */
public class Main {
        public static void main(String[] args){
            Scanner scan=new Scanner(System.in);
            int n=scan.nextInt();
            int[] array=new int[n];
            for(int i=0;i<n;i++){
                array[i]=scan.nextInt();
            }
            //我们不能盲目的去枚举，这样容易乱，而对数组排序反而能减少时间
            Arrays.sort(array);
            System.out.println(getCount(array,n,0,0,1));

        }
        //这里我们可以发现累加和累乘的过程是逐渐往后的，这也是递归的思想，
        //当不满足的时候我们又需要从上一次的地方重新开始判断，我们需要回去判断，这里就利用了回溯的思想
        //利用递归和回溯来解决这个问题，当满足sum>multi时就在往后递归，如果不满足就回溯到上一次的地方
        //那怎么回溯呢，就是让加了的减去，乘了的除去
        public static int getCount(int[] array,int n,int pos,int sum,int multi){
            int count=0;
            //每一次不一定都是从0下标开始，而是从一个指定位置开始向下递归
            for(int i=pos;i<n;i++){
                //这里我们可以先加，先乘，再做判断
                sum+=array[i];
                multi*=array[i];
                //判断
                if(sum>multi){
                    //说明符合情况，那么就让计数器++，然后继续累加下一个，往下一个递归
                    count=count+1+getCount(array,n,i+1,sum,multi);
                }else if(array[i]==1){  //判断第一个数是1的情况
                    //如果这个数等于1，那么肯定是符和sum>mutli的因为1+任何数的结果都大于1*任何数的结果
                    //如果当前数为1，那么也可以往下一个元素递归
                    count=count+getCount(array,n,i+1,sum,multi);
                }else{
                    //如果sum<miutli，那么就退出整个循环，返回上一个累加的count
                    break;
                }
                //到了这里说明上一次是不合理的情况，经历了break返回了count,
                //然后又回到了if或者else if里面,接着就执行到了这个地方，表示当前这个情况是合法的
                //需要去回溯判断前面的值与当前位置的下一个值的sum和mutli是否合法
                sum-=array[i];
                multi/=array[i];
                //题目中说到拥有相同号码的球是无区别的，那么如果下一个位置和当前位置的值是一样的，
                //那么就需要跳过这个数因为前面已经判断过了
                while(i+1<n && array[i]==array[i+1]){
                    i++;
                }
            }
            return count;
        }
}
