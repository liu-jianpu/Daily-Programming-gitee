/**
 * Created by L.jp
 * Description:有一幅以二维整数数组表示的图画，每一个整数表示该图画的像素值大小，数值在 0 到 65535 之间。
 *
 * 给你一个坐标(sr, sc)表示图像渲染开始的像素值（行 ，列）和一个新的颜色值newColor，让你重新上色这幅图像。
 *
 * 为了完成上色工作，从初始坐标开始，记录初始坐标的上下左右四个方向上像素值与初始坐标相同的相连像素点，接着再记录这四个方向上符合条件的像素点与他们对应四个方向上像素值与初始坐标相同的相连像素点，……，重复该过程。将所有有记录的像素点的颜色值改为新的颜色值。
 *
 * 最后返回经过上色渲染后的图像。

 * User: 86189
 * Date: 2021-11-23
 * Time: 16:35
 */
public class ImageRendering {
    //把这个坐标周围的表示方法按照上右下左顺时针的顺序用二维数表示出来
    int[][] contactPos={{-1,0},{0,1},{1,0},{0,-1}};
    public  int[][] floodFill(int[][] image, int sr, int sc, int newColor) {
        int srcColor=image[sr][sc];
        if(srcColor!=newColor){//如果一开始起始坐标的颜色和要更改的颜色相同，那么不需要做任何修改，如果不相同就要先修改本坐标的颜色，然后再去遍历周围坐标
            dfs(image,sr,sc,srcColor,newColor);
        }
        return image;
    }
    public   void  dfs(int[][] image,int x,int y,int srcColor,int newColor){
        //法一：用四个dfs表示，速度较快
        /*
        if((x<0 || x>= image.length) || (y<0 || y>=image[0].length))
            return ;
        if (image[x][y] == srcColor) {
            image[x][y] = newColor;//改掉当前坐标的颜色
            // 遍历周围四个点，更改颜色
            dfs(image,x-1,y,srcColor,newColor);//上
            dfs(image,x,y+1,srcColor,newColor);//右
            dfs(image,x+1,y,srcColor,newColor);//下
            dfs(image,x,y-1,srcColor,newColor);//左
        }
        return ;

         */
        //法二：用一个dfs，速度慢
        if (image[x][y] == srcColor) {
            image[x][y] = newColor;//改掉当前坐标的颜色
            for (int i = 0; i < 4; i++) {//遍历四周，借助周围的坐标表示数组
                int newX = x + contactPos[i][0];
                int newY = y + contactPos[i][1];
                if ((newX < 0 || newX >= image.length) || (newY < 0 || newY >= image[0].length)) {
                    continue;
                }
                dfs(image, newX, newY, srcColor, newColor);
            }
        }
    }
}
