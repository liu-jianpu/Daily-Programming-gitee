import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by L.jp
 * Description:给定一个保存员工信息的数据结构，它包含了员工 唯一的 id ，重要度 和 直系下属的 id 。
 *
 * 比如，员工 1 是员工 2 的领导，员工 2 是员工 3 的领导。他们相应的重要度为 15 , 10 , 5 。那么员工 1 的数据结构是 [1, 15, [2]] ，员工 2的 数据结构是 [2, 10, [3]] ，员工 3 的数据结构是 [3, 5, []] 。注意虽然员工 3 也是员工 1 的一个下属，但是由于 并不是直系 下属，因此没有体现在员工 1 的数据结构中。
 *
 * 现在输入一个公司的所有员工信息，以及单个员工 id ，返回这个员工和他所有下属的重要度之和。

 * User: 86189
 * Date: 2021-11-22
 * Time: 23:43
 */
//边界: 下属为空
//     每次先加第一个下属的重要性
//     按照相同的操作再去加下属的第一个下属的重要性
//方法一：深度优先搜索
//深度优先搜索的做法非常直观。根据给定的员工编号找到员工，从该员工开始遍历，对于每个员工，将其重要性加到总和中，然后对该员工的每个直系下属继续遍历，直到所有下属遍历完毕，此时的总和即为给定的员工及其所有下属的重要性之和。
//实现方面，由于给定的是员工编号，且每个员工的编号都不相同，因此可以使用哈希表存储每个员工编号和对应的员工，即可通过员工编号得到对应的员工。
class Employee {
    public int id;
    public int importance;
    public List<Integer> subordinates;
}
public class EmployeeImportance {
    //建立Map哈希表存储员工id和每个员工的信息，通过员工id得到员工的信息
    Map<Integer, Employee> map = new HashMap<>();
    public int getImportance(List<Employee> employees, int id) {
        for (Employee employee : employees) {
            map.put(employee.id,employee);
        }
        //返回员工的重要度之和
        return dfs(id);
    }
    //深度优先搜索
    public int dfs(int id){
        //通过map get找到每个员工
        Employee employee=map.get(id);
        int ret=employee.importance;
        //每个员工的下属集合是一个list
        for (int i:employee.subordinates) {
            ret+=dfs(i);//通过list巧妙利用for循环递归每个子树，i就是list的元素即下属的id
        }
        return ret;
    }
}
