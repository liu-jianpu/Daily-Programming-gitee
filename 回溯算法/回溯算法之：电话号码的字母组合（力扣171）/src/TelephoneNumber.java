import java.util.ArrayList;
import java.util.List;

/**
 * Created by L.jp
 * Description:
 * User: 86189
 * Date: 2021-11-26
 * Time: 20:37
 */
public class TelephoneNumber {
    StringBuilder tmpStr=new StringBuilder();//存储临时拼接的字符串，比如“ad","ac".....,还不是最终结果
    List<String> list=new ArrayList<>();//存储最终的字符串结果
    public List<String> letterCombinations(String digits){
        if(digits==null || digits.length()==0) {
            return list;
        }
        String[] str={"","","abc","def","ghi", "jkl", "mno", "pqrs", "tuv", "wxyz"};
        dfs(digits,str,0);
        return list;
    }
    public void dfs(String digits,String[] str,int curIndex){
        if(tmpStr.length()==digits.length()){//每次拼接之前先判断字符串长度是不是和数字字符串的长度一样
            list.add(tmpStr.toString());//当拼接的长度等于数字字符串的长度时，那么就可以不用拼接了，先将拼接的结果存储到list中
            return ;//返回
        }
        String curStr=str[digits.charAt(curIndex)-'0'];//拿到当前数字对应的字符串str[i],而这里的i因为是整数，
                                                        // i就代表这字符串的对应1下标也代表这电话号数字
                                                        // 所以需要将数字字符串的字符数字转换成整数，利用digits.charat(curindex)-'0'得到
        for(int i=0;i<curStr.length();i++){
            tmpStr.append(curStr.charAt(i));//拼接字符串，最终长度只能跟数字字符串的长度相同
            dfs(digits,str,curIndex+1);//寻找下一个数字字符串的数字，递归，相当于数字字符串的长度时多长，就要循环拼接几次，没拿到一个字符串的一个字符就要去下一个数字字符串找对应的字符串
            tmpStr.deleteCharAt(tmpStr.length()-1);//要完成下一次拼接只能删除当前字符串的最后一个字母，因为在下一次拼接之前，前面的字符串都是相等的，只需拼接最后一个
        }
    }

}
