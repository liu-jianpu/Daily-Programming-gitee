/**
 * Created by L.jp
 * Description:
 * User: 86189
 * Date: 2021-11-25
 * Time: 23:30
 */
public class NumberIslands {
    //利用深度优先搜索的方法去寻找1的数量，但是岛屿是由1连成一片的，所以在遍历矩阵计算岛屿数量的时候其实是计算最后一个1的出现次数，最后一个1一定是只有一边与0相连
    public  void dfs(char[][] grid,int row,int col,int x,int y){
        if((x<0 || x>=row || y<0 || y>=col) || (grid[x][y]!='1'))
            return;//越界或者遇到了0，说明可以停止搜索，返回去计算岛屿数量了
        grid[x][y]='2';//标记已经遍历过的大陆
        //找其他的大陆
        dfs(grid,row,col,x-1,y);
        dfs(grid,row,col,x,y+1);
        dfs(grid,row,col,x+1,y);
        dfs(grid,row,col,x,y-1);
    }
    public int numIslands(char[][] grid) {
        if(grid==null || grid.length==0){
            return 0;
        }
        int row=grid.length;
        int col=grid[0].length;
        int count=0;
        for(int i=0;i<row;i++){
            for(int j=0;j<col;j++){
                if(grid[i][j]=='1'){
                    count++;
                    dfs(grid,row,col,i,j);
                }
            }
        }
        return  count;
    }
}
