import java.util.Scanner;

/**
 * Created by L.jp
 * Description:链接：https://www.nowcoder.com/questionTerminal/a811535fed784ea492b63622c28c75c5
 * 来源：牛客网
 *
 * NowCoder最近爱上了五子棋，现在给你一个棋局，请你帮忙判断其中有没有五子连珠（超过五颗也算）。
 *
 * 输入描述:
 * 输入有多组数据，每组数据为一张20x20的棋盘。
 *
 * 其中黑子用“*”表示，白子用“+”表示，空白位置用“.”表示。
 *
 *
 * 输出描述:
 * 如果棋盘上存在五子连珠（无论哪种颜色的棋子），输入“Yes”，否则输出“No”。
 * User: 86189
 * Date: 2022-05-01
 * Time: 13:42
 */
public class Main {
    public static void main(String[] args) {
        Scanner scanner=new Scanner(System.in);
        while (scanner.hasNext()){
            Character[][] chess=new Character[20][20];
            for(int i = 0; i <20;i++){
                String str=scanner.next();
                for(int j = 0; j < 20; j++){
                    chess[i][j]=str.charAt(j);
                }
            }
            if(dfs(chess)){
                System.out.println("Yes");
            }else {
                System.out.println("No");
            }
        }
    }
    public static  boolean dfs(Character[][] chess){
        //定义一个存储该坐标上下左右，左上，右上，左下，右下的周围数组
        int[][] around={{-1,0},{1,0},{0,-1},{0,1},{-1,-1},{-1,1},{1,-1},{1,1}};
        for(int i = 0;i<20;i++){
            for(int  j=0;j<20;j++){
                if(chess[i][j].equals('*') || chess[i][j].equals('+')){
                    // int count=1; //不能出现在这里的原因就是很可能在遍历周围的周围棋子的时候，可能与原来的棋子的位置已经脱节了，根本连不上了
                    for(int k=0;k<8;k++){
                        //这里为什么要重新赋值为1，因为这个计数不是一直叠加的，而是找到一个棋子之后，然后再遍历它的8个周围，代表着每一个棋子的周围是不是五连子
                        //我们要判断的是从每一个点为起点去判断他的周围的8个坐标，
                        int count=1;
                        //遍历i,j周围的八个坐标
                        int nx=i+around[k][0];
                        int ny=j+around[k][1];
                        while (nx>=0 && nx<20 && ny>=0 && ny<20 && chess[nx][ny]==chess[i][j]){
                            //如果周围8个坐标的棋和该坐标的棋是一样的，那么就加一个数，再遍历这个8个坐标的周围，递归去搜索
                            count++;
                            //这个跟外部那个nx是一样的方向，因为在搜索棋子的时候不能改变方向，如果改变了方向就属于新的棋子了
                            nx+=around[k][0];
                            ny+=around[k][1];
                        }
                        //如果越界了，或者没有找到相等的棋子了，那么就判断是不是五连子，是就返回true
                        if(count==5) {
                            return true;
                        }
                    }
                }
            }
        }
        return  false;
    }
}
