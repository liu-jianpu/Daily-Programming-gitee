import java.util.Scanner;

/**
 * Created by L.jp
 * Description:有一间长方形的房子，地上铺了红色、黑色两种颜色的正方形瓷砖。你站在其中一块黑色的瓷砖上，只能向相邻的（上下左右四个方向）黑色瓷砖移动。请写一个程序，计算你总共能够到达多少块黑色的瓷砖。
 * User: 86189
 * Date: 2022-04-12
 * Time: 14:44
 */
public class Main {
    //dfs+回溯
    // ‘.'为黑色   ‘#'为白色 ’@‘为起点
    //定义一个周围的数组
    static  int[][] around={{-1,0},{0,1},{1,0},{0,-1}};
    static int count=0;//记录走过多少黑色砖块
    public static void main(String[] args) {
        Scanner scanner=new Scanner(System.in);
        while (scanner.hasNext()) {
            //n为矩阵的行数,m为矩阵的列数
            int n = scanner.nextInt();
            int m = scanner.nextInt();
            scanner.nextLine();
            char[][] map=new char[n][m];
            //定义其实位置，这个位置必须是黑色瓷砖
            int sx=0;
            int sy=0;
            for(int i=0;i<n;i++){
                //在每一行输入一个字符串
                String str=scanner.nextLine();
                for(int j=0;j<m;j++){
                    map[i][j]=str.charAt(j);
                    if(map[i][j]=='@'){
                        sx =i;
                        sy = j;
                    }
                }
            }
            //然后开始深度优先搜索
            dfs(map,around,sx,sy,n,m);//处理count
            System.out.println(count); //直接对count打印
            count=0;//解决完一组数据之后要把count重置为0，因为count是类变量
        }
    }
    //深度优先搜索
    public static  void dfs(char[][] map,int[][] around,int x,int y,int n,int m){
        //判断是否是白色砖块
        if(map[x][y]=='#'){
            return;
        }
        //否则就不是白色砖块，那么就要计数
        ++count;
        //然后把这个遍历过的砖块改为白色，（也可以在定义一个布尔数组把这个砖块标记为已经遍历过了，就是改为visited[x][y]=true）
        map[x][y]='#';
        //从开始起点的上下左右四个方向开始搜索
        for(int i=0;i<around.length;i++){
            //依次获取上下左右四个方向的坐标
            int newX=x+around[i][0];
            int newY=y+around[i][1];

            //然后就是递归，这周围四个坐标也是同样的遍历计数方法
            //但是同时要保证周围坐标不越界
            if(newX>=0 && newX<=n-1 && newY>=0 && newY<=m-1){
                dfs(map,around,newX,newY,n,m);
            }
        }
    }
}
