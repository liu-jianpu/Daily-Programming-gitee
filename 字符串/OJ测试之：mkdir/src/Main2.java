import java.util.Scanner;

/**
 * Created by L.jp
 * Description:
 * User: 86189
 * Date: 2022-04-14
 * Time: 18:52
 */
public class Main2 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        while (sc.hasNext()) {
            StringBuffer s1=new StringBuffer(sc.next());
            StringBuffer s2=new StringBuffer(sc.next());
            int len1 = sc.nextInt();
            int len2 = sc.nextInt();
            for(int i=0;i<len2;i++){
                s1.append('a');
            }
            for(int i=0;i<len2;i++){
                s2.append('z'+1);
            }
            int [] array=new int[len2];
            for(int i=0;i<len2;i++){
                array[i]=s2.charAt(i)-s1.charAt(i);
            }
            long result=0;
            for(int i=len1;i<=len2;i++){
                for(int j=0;j<i;j++){
                    result+=array[j]*Math.pow(26,i-j-1);
                }
            }
            System.out.println((result+1)%1000007);
        }
    }
}
