import java.util.Arrays;
import java.util.Scanner;

/**
 * Created by L.jp
 * Description:
 * 链接：https://www.nowcoder.com/questionTerminal/433c0c6a1e604a4795291d9cd7a60c7a
 * 来源：牛客网
 *
 * 工作中，每当要部署一台新机器的时候，就意味着有一堆目录需要创建。例如要创建目录“/usr/local/bin”，就需要此次创建“/usr”、“/usr/local”以及“/usr/local/bin”。好在，Linux下mkdir提供了强大的“-p”选项，只要一条命令“mkdir -p /usr/local/bin”就能自动创建需要的上级目录。
 * 现在给你一些需要创建的文件夹目录，请你帮忙生成相应的“mkdir -p”命令。
 *
 * 输入描述:
 * 输入包含多组数据。
 *
 * 每组数据第一行为一个正整数n(1≤n≤1024)。
 *
 * 紧接着n行，每行包含一个待创建的目录名，目录名仅由数字和字母组成，长度不超过200个字符。
 *
 *
 * 输出描述:
 * 对应每一组数据，输出相应的、按照字典顺序排序的“mkdir -p”命令。
 *
 * 每组数据之后输出一个空行作为分隔。
 * 示例1
 * 输入
 * 3
 * /a
 * /a/b
 * /a/b/c
 * 3
 * /usr/local/bin
 * /usr/bin
 * /usr/local/share/bin
 * 输出
 * mkdir -p /a/b/c
 *
 * mkdir -p /usr/bin
 * mkdir -p /usr/local/bin
 * mkdir -p /usr/local/share/bin
 * User: 86189
 * Date: 2022-04-10
 * Time: 22:31
 */
public class Main {
    public static void main(String[] args) {
        Scanner scanner=new Scanner(System.in);
        while (scanner.hasNext()){
            int n = scanner.nextInt();
            String[] path=new String[n];
            for(int i = 0; i < n; i++){
                path[i] = scanner.next();
            }
            //首先对目录进行字典排序，以便剔除一些重复的目录，或者有包含的目录
            Arrays.sort(path);
            //定义一个布尔数组，如果是目录是相同的，或者含有包含的那么就置为true,表示它要被剔除
            boolean[] temp=new boolean[n];
            //判断目录
            for(int i = 0; i < path.length; i++){
                //相等的目录要剔除
                if(i+1< path.length && path[i].equals(path[i+1])){
                    temp[i] = true;
                }else if(i+1< path.length && path[i].length()<path[i+1].length() && path[i+1].contains(path[i])
                        && path[i+1].charAt(path[i].length())=='/'){
                    //要判断是否包含目录就要同时满足三个条件：
                    //对于字典序排好的目录，如果前面的长度小于后面的长度
                    // 而且后面的目录包含了前面的目录，‘
                    // 后面目录在前面目录长度的下一个位置是 ’/‘
                    temp[i]=true;
                }
            }
            for(int i = 0;i<path.length;i++){
                if(!temp[i]){
                    System.out.println("mkdir -p "+path[i]);
                }
            }
            System.out.println();
        }
    }
}
