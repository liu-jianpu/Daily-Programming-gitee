import java.util.Scanner;

/**
 * Created by L.jp
 * Description:
 * User: 86189
 * Date: 2022-04-14
 * Time: 18:49
 */
public class Main1 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        while (scanner.hasNext()) {
            String m=scanner.next();
            String n=scanner.next();
            System.out.println(maxLength(m,n));
        }
    }
    public static int maxLength(String m,String n){
        int[][] dp=new int[m.length()+1][n.length()+1];
        for(int i=0;i<=m.length();i++){
            for(int j=0;j<=n.length();j++){
                if(i==0||j==0){
                    dp[i][j]=0;
                }else if(m.charAt(i-1)==n.charAt(j-1)){
                    dp[i][j]=dp[i-1][j-1]+1;
                }else{
                    dp[i][j]=Math.max(dp[i-1][j],dp[i][j-1]);
                }
            }
        }
        return dp[m.length()][n.length()];
    }
}
