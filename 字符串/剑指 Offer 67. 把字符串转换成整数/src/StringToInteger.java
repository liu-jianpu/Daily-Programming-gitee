/**
 * Created by L.jp
 * Description:首先，该函数会根据需要丢弃无用的开头空格字符，直到寻找到第一个非空格的字符为止。
 * 当我们寻找到的第一个非空字符为正或者负号时，则将该符号与之后面尽可能多的连续数字组合起来，作为该整数的正负号；假如第一个非空字符是数字，则直接将其与之后连续的数字字符组合起来，形成整数。
 * 该字符串除了有效的整数部分之后也可能会存在多余的字符，这些字符可以被忽略，它们对于函数不应该造成影响。
 * 注意：假如该字符串中的第一个非空格字符不是一个有效整数字符、字符串为空或字符串仅包含空白字符时，则你的函数不需要进行转换。
 * 在任何情况下，若函数不能进行有效的转换时，请返回 0。
 * 说明：
 * 假设我们的环境只能存储 32 位大小的有符号整数，那么其数值范围为[−2^31, 2^31− 1]。如果数值超过这个范围，请返回INT_MAX (2^31− 1) 或INT_MIN (−2^31) 。
 * Date: 2021-11-01
 * Time: 15:46
 */
public class StringToInteger {
    public static int strToInt(String str) {
        int i = 0;
        int ret = 0;//记录最终结果
        int flg = 1;
        if(str.length()==0){
            return 0;
        }
        //去除空格
        while (str.charAt(i) == ' ') {
            if(i==str.length()){
                return 0;
            }
            i++;
        }
        //去除两边多余的空格，然后变成字符数组
        // char str1[]=str.trim().toCharArray();
        //找正负号
        if (str.charAt(i) == '-') {
            flg = -1;
            i++;
        }
        if ( str.charAt(i) == '+') {
            i++;
        }
        //拼接数字和解决溢出问题
        for (int j = i; j < str.length(); j++) {
            if (str.charAt(j) < '0' || str.charAt(j) > '9') {
                break;
            }
            if (ret > Integer.MAX_VALUE / 10 || (ret == Integer.MAX_VALUE / 10 && str.charAt(j) > '7')) {
                return flg == 1 ? Integer.MAX_VALUE : Integer.MIN_VALUE;
            }
            ret = ret * 10 + str.charAt(j)-'0';
        }
        return flg * ret;
    }
    public static void main(String[] args) {
        String str="    12312311111";
        System.out.println(strToInt(str));
    }
}
