import java.util.Scanner;

/**
 * Created by L.jp
 * Description:输入一串字符，请编写一个字符串压缩程序，将字符串中连续出现的重复字母进行压缩，并输出压缩后的字符串。
 * 例如： aac 压缩为 1ac xxxxyyyyyyzbbb 压缩为 3x5yz2b
 *
 * 输入：
 * xxxxyyyyyyzbbb
 *
 * 输出；
 * 3x5yz2b
 * User: 86189
 * Date: 2021-12-08
 * Time: 21:30
 */
public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        char[] str = scanner.nextLine().toCharArray();
        for (int i = 0; i < str.length; i++) {//遍历字符数组
            int cnt = 0;//记录重复字符的个数
            while (i <= str.length - 2 && str[i] == str[i+1]) {//当i可以遍历到倒数第二个，并且第i个字符等于第i+1个个数
                cnt++;//重复字符个数+1
                i++;//跳到下一个坐标去判断
            }
            //当i和i+1不相等时
            if (cnt != 0) {
                System.out.print(cnt);//先打印重复字符个数
            }
            System.out.print(str[i]);//再打印字符
        }
        System.out.println();
    }
}


