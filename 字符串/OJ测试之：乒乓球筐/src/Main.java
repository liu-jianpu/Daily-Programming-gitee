import java.util.Scanner;
/**
 * Created by L.jp
 * Description:
 * 链接：https://www.nowcoder.com/questionTerminal/bb4f1a23dbb84fd7b77be1fbe9eaaf32
 * 来源：牛客网
 *
 * nowcoder有两盒（A、B）乒乓球，有红双喜的、有亚力亚的……现在他需要判别A盒是否包含了B盒中所有的种类，并且每种球的数量不少于B盒中的数量，该怎么办呢？
 *
 * 输入描述:
 * 输入有多组数据。
 * 每组数据包含两个字符串A、B，代表A盒与B盒中的乒乓球，每个乒乓球用一个大写字母表示，即相同类型的乒乓球为相同的大写字母。
 * 字符串长度不大于10000。
 *
 *
 * 输出描述:
 * 每一组输入对应一行输出：如果B盒中所有球的类型在A中都有，并且每种球的数量都不大于A，则输出“Yes”；否则输出“No”。
 * User: 86189
 * Date: 2022-04-21
 * Time: 16:05
 */
public class Main {
    public static void main(String[] args) {
        //其实就是统计A，B字符串中的各个字符出现的次数，如果A字符串包含了B字符串，而且B字符串的各个字符出现的次数大于A字符串出现字符的个数，那么就返回false,否则返回true
        //输入只有一行，就可以使用字符串数组存储输入的字符串，并以空格分割
        Scanner scan=new Scanner(System.in);
        while(scan.hasNext()){
            String[] str=scan.nextLine().split(" ");
            String A=str[0];
            String B=str[1];
            //创建一个26个元素的数组，存放各个字符出现的次数
            int[] count=new int[26];
            //遍历A字符串，在计数数组中进行计数+1,那么对于B中的计数数组就是-1，最后遍历计数数组，如果有元素小于0，那么就表示不包含B字符串
            for(int i=0;i<A.length();i++){
                char ch=A.charAt(i);
                count[ch-'A']++;
            }
            for(int i=0;i<B.length();i++){
                char ch=B.charAt(i);
                count[ch-'A']--;
            }
            //遍历计数数组，如果有元素小于0，那么就表示不包含B字符串
            boolean flag=true;
            for (int num : count) {
                if (num < 0) {
                    flag = false;
                    break;
                }
            }
            if(flag){
                System.out.println("Yes");
            }else{
                System.out.println("No");
            }
        }
    }
}