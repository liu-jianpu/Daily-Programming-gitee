import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
/**
 * Created by L.jp
 * Description:
 * 找出字符串中第一个只出现一次的字符
 * 输入描述：
 * 输入一个非空字符串
 *
 * 输出描述：
 * 输出第一个只出现一次的字符，如果不存在输出-1
 * 输入：
 * asdfasdfo
 *
 * 复制
 * 输出：
 * o
 * User: 86189
 * Date: 2022-03-17
 * Time: 21:37
 */
public class Main {
    public  static void findChar(String string){
        //利用每个字符的ASCII码值数组作为计数
        int[] count=new int[128];
        char[] str=string.toCharArray();
        for(int i = 0;i<str.length; i++){
            count[str[i]]++;  //以每个字符的ASCII码作为每个字母的下标
        }
        //寻找第一个出现一次的字母
        boolean flag=false;
        for(int i = 0; i < count.length; i++){
            if (count[str[i]] == 1) {
                System.out.println(str[i]);
                flag = true;//找到了之后就置为真
                break;//找到了就退出
            }
        }
        if(!flag){
            System.out.println(-1);
        }
    }
    public static void main(String[] args) throws IOException {
        BufferedReader bufferedReader=new BufferedReader(new InputStreamReader(System.in));
        String string;
        while ((string=bufferedReader.readLine())!=null){
            findChar(string);
        }
    }
}
