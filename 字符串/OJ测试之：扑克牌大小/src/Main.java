import java.util.Scanner;

/**
 * Created by L.jp
 * Description:扑克牌游戏大家应该都比较熟悉了，一副牌由54张组成，含3~A，2各4张，小王1张，大王1张。牌面从小到大用如下字符和字符串表示（其中，小写joker表示小王，大写JOKER表示大王）:)
 * 3 4 5 6 7 8 9 10 J Q K A 2 joker JOKER
 * 输入两手牌，两手牌之间用“-”连接，每手牌的每张牌以空格分隔，“-”两边没有空格，如：4 4 4 4-joker JOKER
 * 请比较两手牌大小，输出较大的牌，如果不存在比较关系则输出ERROR
 *
 * 基本规则：
 * （1）输入每手牌可能是个子，对子，顺子（连续5张），三个，炸弹（四个）和对王中的一种，不存在其他情况，由输入保证两手牌都是合法的，顺子已经从小到大排列；
 * （2）除了炸弹和对王可以和所有牌比较之外，其他类型的牌只能跟相同类型的存在比较关系（如，对子跟对子比较，三个跟三个比较），不考虑拆牌情况（如：将对子拆分成个子）
 * （3）大小规则跟大家平时了解的常见规则相同，个子，对子，三个比较牌面大小；顺子比较最小牌大小；炸弹大于前面所有的牌，炸弹之间比较牌面大小；对王是最大的牌；
 * （4）输入的两手牌不会出现相等的情况。
 *
 * 答案提示：
 * （1）除了炸弹和对王之外，其他必须同类型比较。
 * （2）输入已经保证合法性，不用检查输入是否是合法的牌。
 * （3）输入的顺子已经经过从小到大排序，因此不用再排序了.
 *
 * 数据范围：保证输入合法
 * User: 86189
 * Date: 2022-03-24
 * Time: 20:57
 */
public class Main {
    public static void main(String[] args) {
        Scanner scanner=new Scanner(System.in);
        while (scanner.hasNext()){
            String string=scanner.nextLine();
            //把两手牌拆开
            String[] strArray = string.split("-");
            //把第一手牌按空格分开放入数组
            String[] str1=strArray[0].split(" ");
            //把第二手牌按空格分开放入数组
            String[] str2 = strArray[1].split(" ");
            String tmp="34567891JQKA2";
            //如果第一手牌或者第二手牌是一对王，那么就接打印出这对王
            if(strArray[0].equals("joker JOKER" )|| strArray[1].equals("joker JOKER")){
                System.out.println("joker JOKER");
            }else if(str2.length== str1.length) {//接下来是比较个子之间，对子之间，炸弹之间，三张牌之间，顺子之间的大小，只需要比较第一张牌的大小即可
                //这里我们需要一个方法来比较每一手牌第一张牌的大小，那么怎么比较呢
                //这里我们按照字符的ASCII码来比较，因为A比J,Q,K小，但是在牌上是大的
                //所以我们可以按照牌的顺序来比较牌的大小：34567891JQKA2"
               //indexOf:: 返回此字符串中第一次出现指定子字符串的索引.也就是说根据两手牌的第一张牌在给定字符串的索引的大小来比较牌的大小，因为字符串中是根据索引大小来排序牌的
                if(tmp.indexOf(str1[0].substring(0,1))>tmp.indexOf(str2[0].substring(0,1))){  //第一手牌大
                    //这里如果没有substring的话，那么如果有一手牌第一张为10，第二手牌第一张为3，
                    // 那么我们就在给定的tmp字符串中找不到10的索引会返回-1，反而返回的是3，但其实是10大，那么我们就需要再对第一张牌进行拆分
                    //拆为一个字符，这样10的牌就需要比较1就行了，这样1也能在tmp字符串中找到索引
                    System.out.println(strArray[0]);
                }else{   //第二手牌大
                    System.out.println(strArray[1]);
                }
            }else if(str1.length==4){  //这是比较两手牌之间，有一手是炸弹的情况。炸弹除了王之外大于任何牌，所以直接输出炸弹
                System.out.println(strArray[0]);
            }else if (str2.length == 4) {
                System.out.println(strArray[1]);
            }else {
                //其他不存在的比较关系输出错误
                System.out.println("ERROR");
            }
        }
    }
}
