/**
 * Created by L.jp
 * Description:给你一个字符串 date ，按 YYYY-MM-DD 格式表示一个 现行公元纪年法 日期。返回该日期是当年的第几天。
 * 输入：date = "2019-01-09"
 * 输出：9
 * 解释：给定日期是2019年的第九天。
 * User: 86189
 * Date: 2022-02-11
 * Time: 12:27
 */
public class Solution {
    public static int dayOfYear(String date) {
        //将字符串年月日分开,并且转化为整数
        int year=Integer.parseInt(date.substring(0,4));
        int month = Integer.parseInt(date.substring(5,7));
        int day = Integer.parseInt(date.substring(8,10));
        //从下标0开始，0号下标对应1月，11号下标对应12月
        int[] days={31,28,31,30,31,30,31,31,30,31,30,31};
        //判断平润年
        if(year%4==0 && year%100!=0 || year % 400 == 0){
            days[1]=29;
        }
        int ret=0;
        //计算是第几天，前几个月的总天数ret+days[i]+这个月的天数day
        for(int i=0;i<month-1;i++){  //不能是month，否则会直接加上这个月的总天数
            ret+=days[i];
        }
        ret+=day;
        return ret;
    }

    public static void main(String[] args) {
        System.out.println(dayOfYear("2019-01-29"));
    }
}
