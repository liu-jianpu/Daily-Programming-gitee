import java.util.Scanner;

/**
 * Created by L.jp
 * Description:密码按如下规则进行计分，并根据不同的得分为密码进行安全等级划分。
 *
 * 一、密码长度:
 * 5 分: 小于等于4 个字符
 * 10 分: 5 到7 字符
 * 25 分: 大于等于8 个字符
 *
 * 二、字母:
 * 0 分: 没有字母
 * 10 分: 密码里的字母全都是小（大）写字母
 * 20 分: 密码里的字母符合”大小写混合“
 *
 * 三、数字:
 * 0 分: 没有数字
 * 10 分: 1 个数字
 * 20 分: 大于1 个数字
 *
 * 四、符号:
 * 0 分: 没有符号
 * 10 分: 1 个符号
 * 25 分: 大于1 个符号
 *
 * 五、奖励（只能选符合最多的那一种奖励）:
 * 2 分: 字母和数字
 * 3 分: 字母、数字和符号
 * 5 分: 大小写字母、数字和符号
 *
 * 最后的评分标准:
 * >= 90: 非常安全
 * >= 80: 安全（Secure）
 * >= 70: 非常强
 * >= 60: 强（Strong）
 * >= 50: 一般（Average）
 * >= 25: 弱（Weak）
 * >= 0:  非常弱（Very_Weak）
 *
 * 对应输出为：
 *
 * VERY_SECURE
 * SECURE
 * VERY_STRONG
 * STRONG
 * AVERAGE
 * WEAK
 * VERY_WEAK
 *
 * 请根据输入的密码字符串，进行安全评定。
 *
 * 注：
 * 字母：a-z, A-Z
 * 数字：0-9
 * 符号包含如下： (ASCII码表可以在UltraEdit的菜单view->ASCII Table查看)
 * !"#$%&'()*+,-./     (ASCII码：0x21~0x2F)
 * :;<=>?@             (ASCII码：0x3A~0x40)
 * [\]^_`              (ASCII码：0x5B~0x60)
 * {|}~                (ASCII码：0x7B~0x7E)
 * User: 86189
 * Date: 2022-03-21
 * Time: 20:08
 */
public class Main {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        while (sc.hasNextLine()) {
            String str = sc.nextLine();
            int sum1 = getLen(str);
            int sum2 = getChar(str);
            int sum3 = getNum(str);
            int sum4 = getSym(str);
            int sum = 0;
            if (sum2 == 20 && sum3 >= 1 && sum4 >= 1) {
                sum = sum1 + sum2 + sum3 + sum4 + 5;
            } else if (sum2 == 10 && sum3 >= 1 && sum4 >= 1) {
                sum = sum1 + sum2 + sum3 + sum4 + 3;
            } else if (sum2 == 10 && sum3 >= 1 && sum4 == 0) {
                sum = sum1 + sum2 + sum3 + sum4 + 2;
            } else {
                sum = sum1 + sum2 + sum3 + sum4;
            }
            if (sum >= 90) {
                System.out.println("VERY_SECURE");
            } else if (sum >= 80) {
                System.out.println("SECURE");
            } else if (sum >= 70) {
                System.out.println("VERY_STRONG");
            } else if (sum >= 60) {
                System.out.println("STRONG");
            } else if (sum >= 50) {
                System.out.println("AVERAGE");
            } else if (sum >= 25) {
                System.out.println("WEAK");
            } else if (sum >= 0) {
                System.out.println("VERY_WEAK");
            }
        }
    }
    public static int getLen(String str) {
        if (str.length() <= 4) {
            return 5;
        } else if (7 >= str.length() && str.length() >= 5) {
            return 10;
        } else if (str.length() >= 8) {
            return 25;
        }
        return 0;
    }
    public static int getChar(String str) {
        int small = 0;
        int big = 0;
        for (int i = 0; i < str.length(); i++) {
            if (str.charAt(i) >= 65 && str.charAt(i) <= 90) {
                big++;
            } else if (str.charAt(i) >= 97 && str.charAt(i) <= 122) {
                small++;
            }
        }
        if (small > 0 && big > 0) {
            return 20;
        } else if (small > 0 || big > 0) {
            return 10;
        } else {
            return 0;
        }
    }
    public static int getNum(String str) {
        int num = 0;
        for (int i = 0; i < str.length(); i++) {
            if (str.charAt(i) - '0' >= 0 && str.charAt(i) - '0' <= 9) {
                num++;
            }
        }
        if (num > 1) {
            return 20;
        } else if (num == 1) {
            return 10;
        } else {
            return 0;
        }
    }
    public static int getSym(String str) {
        int num = 0;
        for (int i = 0; i < str.length(); i++) {
            if (!(str.charAt(i) >= 65 && str.charAt(i) <= 90) && !(str.charAt(i) >= 97 && str.charAt(i) <= 122) &&
                    !(str.charAt(i) - '0' >= 0 && str.charAt(i) - '0' <= 9)) {
                num++;
            }
        }
        if (num > 1) {
            return 25;
        } else if (num == 1) {
            return 10;
        } else {
            return 0;
        }
    }
}

