import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;
/**
 * Created by L.jp
 * Description：
 * NowCoder每天要处理许多邮件，但他并不是在收件人列表中，有时候只是被抄送。他认为这些抄送的邮件重要性比自己在收件人列表里的邮件低，因此他要过滤掉这些次要的邮件，优先处理重要的邮件。
 * 现在给你一串抄送列表，请你判断目标用户是否在抄送列表中。
 * 输入描述:
 * 输入有多组数据，每组数据有两行。
 * 第一行抄送列表，姓名之间用一个逗号隔开。如果姓名中包含空格或逗号，则姓名包含在双引号里。总长度不超过512个字符。
 *
 * 第二行只包含一个姓名，是待查找的用户的名字（姓名要完全匹配）。长度不超过16个字符。
 * 输出描述:
 * 如果第二行的名字出现在收件人列表中，则输出“Ignore”，表示这封邮件不重要；否则，输出“Important!”，表示这封邮件需要被优先处理。
 * User: 86189
 * Date: 2022-04-02
 * Time: 18:42
 */
public class Main {
    //这是一个字符串匹配的问题，我们可以利用string.indexOf函数来进行匹配和记忆下标
    //对于抄送列表需要分为两种情况，一个是只有 ','号 ，我们只需根据逗号把它们分隔开放入哈希表即可， 第二种情况是含有空格和逗号的名字被双引号包住，所以我们需要根据两个双引号的位置
    //找到名字，也就是利用indexOf函数，找到被双引号分割的子串，放入哈希表，然后拿着第二行的名字去哈希表里面查找即可
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        while (scanner.hasNext()) {
            String nameList=scanner.nextLine();
            int i=0; //索引
            int end=0; //第二个双引号的位置
            Set<String> set=new HashSet<>();
            //根据情况你放入哈希表
            while (i<nameList.length()){
                //姓名列表不含引号
                if(nameList.charAt(i) !='\"'){
                    end=nameList.indexOf(',',i+1); //如果没有双引号，那么从i+1的位置开始找逗号
                    //由于最后一个字符串是不需要逗号的
                    //所以end返回值可能是-1
                    if(-1==end){
                        //那么担单独就如最后一个字符串
                        set.add(nameList.substring(i,nameList.length()));
                        break;
                    }
                    //不是最后一个字符串
                    set.add(nameList.substring(i,end));
                    //找下一个,此使end在逗号的位置,下一个从逗号后面开始找
                    i=end+1;
                }else{  //包含引号
                    end = nameList.indexOf('\"',i+1);  //如果第i个下标是引号，那么需要从i+1开始找到下一个双引号
                    set.add(nameList.substring(i+1,end)); //subString左闭右开
                    //继续寻找下一个
                    i=end+2; //此时end下标是双引号，再后面是一个逗号，之后才是下一个名字的开始
                }
            }
            //输入名字
            String name=scanner.nextLine();
            //查找
            if(set.contains(name)){
                System.out.println("Ignore");
            }else {
                System.out.println("Important!");
            }
        }
    }
}
