import java.util.*;

/**
 * Created by L.jp
 * Description:在命令行输入如下命令：
 * xcopy /s c:\\ d:\\e，
 * 各个参数如下：
 * 参数1：命令字xcopy
 * 参数2：字符串/s
 * 参数3：字符串c:\\
 * 参数4: 字符串d:\\e
 * 请编写一个参数解析程序，实现将命令行各个参数解析出来。

 * 解析规则：
 * 1.参数分隔符为空格
 * 2.对于用""包含起来的参数，如果中间有空格，不能解析为多个参数。比如在命令行输入xcopy /s "C:\\program files" "d:\"时，参数仍然是4个，第3个参数应该是字符串C:\\program files，而不是C:\\program，注意输出参数时，需要将""去掉，引号不存在嵌套情况。
 * 3.参数不定长
 * 4.输入由用例保证，不会出现不符合要求的输入
 * User: 86189
 * Date: 2022-03-08
 * Time: 17:16
 */

//思路：
  /*  1.字符串拼接函数
    2.列表存储字符串
    3.区分引号里面的空格和引号外面的空格
    4.引号里面就是处于第一个引号的状态下，引号外面就是第二个引号已经遍历完，或者没有遇到引号，我们用一个布尔值区分第一个引号和第二个引号*/
public class Main {
    public static void main(String[] args) {
        Scanner scan=new Scanner(System.in);
        while(scan.hasNextLine()){
            String str=scan.nextLine();
            StringBuilder sb=new StringBuilder();//拼接字符串
            List<String> list=new ArrayList<>();//存储所有的字符串
            boolean flag=false;//没有遇到双引号之前设置为false,遇到了第一个的时候设置为true,遇到第二个的时候又重新设置为false;
            for(int i=0;i<str.length();i++){
                char ch=str.charAt(i);
                //首先处理特殊情况，就是遇到双引号的时候，如果是第一个双引号，那么就需要跳过他，然后正常拼接双引号里面的字符，
                //同时要注意判断双引号里面的空格的情况
                //如果是第二个双引号，那么就只需要跳过他，他后面一定是空格，如果是空格并且当前不属于双引号里面，
                //那么就需要把拼接好的字符串加入列表，然后构建一个新的拼接对象，准备下一次拼接
                //在这里空格有两种情况，一个是引号外面的，一个是引号里面的，那么我们可以通过一个布尔变量来处理
                if(ch=='"'){
                    //我们遇到第一个引号时设置为true,第二个引号设置为false，用来区分里外空格的两种情况
                    flag= flag ? false : true;
                    //遇到双引号就跳过当前字符，去判断下一个字符情况
                    continue;
                }else if(ch==' ' && !flag){
                    //如果遇到了空格，并且是双引号外面的空格，那么就表示这个字符串拼接完成，加入列表
                    list.add(sb.toString());
                    sb=new StringBuilder();
                }else{
                    //如果当前字符不是空格，或者当前还没有遇到第二个引号,此时flag应当为true
                    //那就拼接
                    sb.append(ch);
                }
            }
            //最后一个字符串遍历完会跳出循环，需要额外加入最后一个拼接的字符串
            list.add(sb.toString());
            //打印字符串个数即参数个数
            System.out.println(list.size());
            //打印列表元素，即每一个参数
            for(String strs:list){
                System.out.println(strs);
            }

        }
    }

}
