import java.util.Scanner;

/**
 * Created by L.jp
 * Description:
 * User: 86189
 * Date: 2022-03-01
 * Time: 17:10
 */
public class Main {
    public static void main(String[] args) {
        Scanner scan=new Scanner(System.in);
        int n=scan.nextInt();
        String[] str=new String[n];
        for(int i=0;i<n;i++){
            str[i]=scan.next();//nextLine方法可以读取到空格，会影响比较，为了能够正常比较，就可以采用next方法，自动消去空格
        }
        if(isDicsort(str) && isLensort(str)){
            System.out.println("both");
        }else if(isDicsort(str)){
            System.out.println("lexicographically");
        }else if(isLensort(str)){
            System.out.println("lengths");
        }else{
            System.out.println("none");
        }
    }
    public static boolean isDicsort(String[] str){
        for(int i=0;i<str.length-1;i++){
            if(str[i].compareTo(str[i+1])>0){
                return false;
            }
        }
        return true;
    }
    public static boolean isLensort(String[] str){
        for(int i=0;i<str.length-1;i++){
            if(str[i].length()>str[i+1].length()){
                return false;
            }
        }
        return true;
    }
}
