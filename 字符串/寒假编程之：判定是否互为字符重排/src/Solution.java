import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by L.jp
 * Description:给定两个字符串 s1 和 s2，请编写一个程序，确定其中一个字符串的字符重新排列后，能否变成另一个字符串。
 * User: 86189
 * Date: 2022-02-10
 * Time: 16:23
 */
public class Solution {
    public static boolean CheckPermutation(String s1, String s2) {
       //法一：对字符串变成数组然后排序之后再看两个字符串是否相等
/*
        char[] ch1=s1.toCharArray();
        char[] ch2=s2.toCharArray();
        Arrays.sort(ch1);
        Arrays.sort(ch2);
        return new String(ch1).equals(new String(ch2));
*/

        //法二：利用哈希表统计各字符串中出现的次数，如果是重排那么对应的字符出现的次数是一样的
       /* Map<Character,Integer> map1=new HashMap<>();
        Map<Character,Integer> map2=new HashMap<>();
        //分别统计各字符的次数
        for(char ch:s1.toCharArray()){
            map1.put(ch,map1.getOrDefault(ch,0)+1);//没有时默认是0，有多个的话就可以实现自增+1
        }
        for(char ch:s2.toCharArray()){
            map2.put(ch,map2.getOrDefault(ch,0)+1);
        }
        if(map1.size()!=map2.size()){
            return false;
        }
        //对于其中一个哈希表，遍历它，将它的映射值跟另一个哈希表的映射值对比
        for(char ch: map1.keySet()){//将map变成一个set
            if(map1.get(ch) != map2.get(ch)){
                return false;//次数不相等，返回false;
            }
        }
        return  true;*/

        //法三:利用数组来统计字符出现次数，只需要一个数组即可
        int[] chars=new int[26];//对于两个字符串中可能出现的字母都有其固定的位置，数组类型代表每个字母出现的次数
        for(char ch:s1.toCharArray()){
            chars[ch-'a']++;//对应的字符减去a字符就是对应在数组的位置，即‘a'是0下标，’b'是1小标，'z'是25下标
        }
        //对另一个数组出现的字符再加以计算，如果跟上一个数组出现了相同的次数，那么最终每个字符对应的位置就会变成0，如果没有这个字符的话，它的值就-1
        for(char ch:s2.toCharArray()){
                if(--chars[ch - 'a']<0){
                    return false;
                }
        }
        return  true;

    }

    public static void main(String[] args) {
        System.out.println(CheckPermutation("abc", "bca"));
    }
}
