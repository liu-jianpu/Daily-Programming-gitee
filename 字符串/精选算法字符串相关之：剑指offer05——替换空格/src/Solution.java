/**
 * Created by L.jp
 * Description:请实现一个函数，将一个字符串中的每个空格替换成“%20”。例如，当字符串为We Are Happy.
 * 则经过替换之后的字符串为We%20Are%20Happy。
 * User: 86189
 * Date: 2021-12-15
 * Time: 21:31
 */
public class Solution {
    public static String replaceSpace(StringBuffer str) {
       //遍历添加
//        StringBuilder s=new StringBuilder();//创建新的字符串拼接，用于存储新的字符串
//        for(int i=0;i<str.length();i++){
//            if(str.charAt(i)==' '){//如果str当前字符是空格，那么就拼接“%20”
//                s.append("%20");
//            }else{
//                s.append(str.charAt(i));//如果不是空格，那么就拼接当前字符
//            }
//        }
//        return s.toString();
        //原地添加
        int count=0;//记录空格的个数
        int len= str.length();
        for(int i=0;i<str.length();i++){
            if(str.charAt(i)==' '){
                count++;
            }
        }
        int newL=len+2*count;//假设有两个空格，需要替换为%20，一个空格替换一个%，所以需要额外的空间是剩下的两个字符*2个空格，也就是%号后面的字符个数*空格的个数
        //相当于这个前后两个指针，一个指针从新的末尾往前遍历，一个从旧的末尾往前遍历
        str.setLength(newL);
        int i=newL-1;
        int j=len-1;
        while(i>=0 && j>=0){
            if(str.charAt(j)==' '){
               str.setCharAt(i,'0');
               //str.setCharAt(i--,'0');
               str.setCharAt(i-1,'2');
               //str.setCharAt(i--,'2');
               str.setCharAt(i-2,'%');
               //str.setCharAt(i--,'%');
               j--;
               //i-=3;
            }else{
                str.setCharAt(i--, str.charAt(j));
                //i--;
                j--;
            }
        }
        return str.toString();
    }

    public static void main(String[] args) {
        String s="we are happy";
        StringBuffer stringBuffer=new StringBuffer(s);
        System.out.println(replaceSpace(stringBuffer));

    }
}
