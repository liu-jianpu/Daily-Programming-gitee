import java.util.Scanner;

/**
 * Created by L.jp
 * Description:MP3 Player因为屏幕较小，显示歌曲列表的时候每屏只能显示几首歌曲，
 * 用户要通过上下键才能浏览所有的歌曲。为了简化处理，假设每屏只能显示4首歌曲，光标初始的位置为第1首歌。

 * 现在要实现通过上下键控制光标移动来浏览歌曲列表，控制逻辑如下：
 *
 * 歌曲总数<=4的时候，不需要翻页，只是挪动光标位置。
 *
 * 光标在第一首歌曲上时，按Up键光标挪到最后一首歌曲；光标在最后一首歌曲时，按Down键光标挪到第一首歌曲。
 * 其他情况下用户按Up键，光标挪到上一首歌曲；用户按Down键，光标挪到下一首歌曲。
 *
 * 2. 歌曲总数大于4的时候（以一共有10首歌为例）：
 * 特殊翻页：屏幕显示的是第一页（即显示第1 – 4首）时，光标在第一首歌曲上，用户按Up键后，屏幕要显示最后一页（即显示第7-10首歌），同时光标放到最后一首歌上。同样的，屏幕显示最后一页时，光标在最后一首歌曲上，用户按Down键，屏幕要显示第一页，光标挪到第一首歌上。
 * 一般翻页：屏幕显示的不是第一页时，光标在当前屏幕显示的第一首歌曲时，用户按Up键后，屏幕从当前歌曲的上一首开始显示，光标也挪到上一首歌曲。光标当前屏幕的最后一首歌时的Down键处理也类似。
 * 其他情况，不用翻页，只是挪动光标就行。
 *
 * 输入描述：
 * 输入说明：
 * 1 输入歌曲数量
 * 2 输入命令 U或者D
 *
 * 输出描述：
 * 输出说明
 * 1 输出当前列表
 * 2 输出当前选中歌曲
 * User: 86189
 * Date: 2022-03-31
 * Time: 14:54
 */
public class Main{
    public static void printOrder(String order,int n){
        char[] orders=order.toCharArray();
        //记录当前列表的的第一首歌曲
        int first=1;
        //记录当前选中的歌曲，就是游标的位置
        int mouse=1;
        //分为两种情况，一个是歌曲数量不超过4首，一个是超过四首
        if(n<=4){
            for(int i=0;i<orders.length;i++){
                //光标在第一首歌曲上时，按Up键光标挪到最后一首歌曲；
                if(mouse==1 && first==1 && orders[i]=='U'){
                    first=1;
                    mouse=n;
                }else if(first==1 && mouse==n && orders[i]=='D'){ //光标在最后一首歌曲时，按Down键光标挪到第一首歌曲。
                    first=1;
                    mouse=1;
                }else if(orders[i]=='U'){//其他情况下用户按Up键，光标挪到上一首歌曲；
                    mouse--;
                }else if(orders[i]=='D'){//用户按Down键，光标挪到下一首歌曲。
                    mouse++;
                }
            }
            //打印列表
            for(int i=1;i<n;i++){
                System.out.print(i+" ");
            }
            //最后一个需要分开打印，因为不能打印空格
            System.out.println(n);
            //打印游标的位置
            System.out.println(mouse);
        }else{
            for(int i=0;i<orders.length;i++){
                //特殊翻页：屏幕显示的是第一页（即显示第1 – 4首）时，
                //光标在第一首歌曲上，用户按Up键后，屏幕要显示最后一页（即显示第7-10首歌），同时光标放到最后一首歌上。
                if(first==1 && mouse==1 && orders[i]=='U'){
                    first=n-3;
                    mouse=n;
                }else if(first==n-3 && mouse==n && orders[i]=='D'){//同样的，屏幕显示最后一页时，光标在最后一首歌曲上，用户按Down键，
                    //屏幕要显示第一页，光标挪到第一首歌上。
                    first=1;
                    mouse=1;
                }else if(first!=1 && mouse==first && orders[i]=='U'){//一般翻页：
                    //屏幕显示的不是第一页时，光标在当前屏幕显示的第一首歌曲时，
                    //用户按Up键后，屏幕从当前歌曲的上一首开始显示，光标也挪到上一首歌曲。
                    first--;
                    mouse=first;
                }else if(first!=n-3 && mouse==first+3 && orders[i]=='D'){//光标当前屏幕的最后一首歌时的Down键处理也类似。
                    first++;
                    mouse++;
                }else if(orders[i]=='U'){
                    mouse--;
                }else if(orders[i]=='D'){
                    mouse++;
                }
            }
            //打印从当前列表的第一首歌开始，但是屏幕和打印只能显示四首歌
            for(int i=first;i<first+3;i++){
                System.out.print(i+" ");
            }
            //特殊处理最后一首歌
            System.out.println(first+3);
            //打印游标的位置
            System.out.println(mouse);
        }
    }
    public static void main(String[] args){
        Scanner scanner=new Scanner(System.in);
        while(scanner.hasNext()){
            int n=scanner.nextInt();
            String order=scanner.next();
            printOrder(order,n);
        }
    }
}