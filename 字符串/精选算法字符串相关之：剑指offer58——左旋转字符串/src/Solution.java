/**
 * Created by L.jp
 * Description:字符串的左旋转操作是把字符串前面的若干个字符转移到字符串的尾部。
 * 请定义一个函数实现字符串左旋转操作的功能。比如，输入字符串"abcdefg"和数字2，该函数将返回左旋转两位得到的结果"cdefgab"。
 *

 * User: 86189
 * Date: 2022-01-15
 * Time: 21:35
 */
public class Solution {
    /**
     * @param str: 给定字符串
     * @param n: 旋转n次
     * [str, n]
     * @return String  返回左旋n次的字符串
     * @description TODO
     */
    public  static String LeftRotateString(String str,int n) {
        //法一：
        //每次拿出字符串第一个字符作为一个临时的值,然后从这个字符后面开始依次让后面的字符往前移,
        // 移完了就剩下一个空位,把临时的值放到那里
        if(str==null || n==0){
            return str;
        }
        //防止重复左移
        n%=str.length();
        char[] chars = str.toCharArray();
        //左旋n次
        for(int i = 0; i < n; i++){
            //每一次左旋的前移
            LeftRotateStringHelper(chars);
        }
        return new String(chars);
    }
    public static  void LeftRotateStringHelper(char[] chars){
        char tmp = chars[0];
        int i=0;
        //i只能走到len-2的位置，否则前移时会越界
        for(;i<chars.length-1; i++){
            chars[i] =chars[i+1];
        }
        //前移完了就剩下最后一个位置，那么就把剩下的最后一个数放到空位
        chars[i] = tmp;
    }

    public static void main(String[] args) {
        System.out.println(LeftRotateString("123abc", 3));
    }
}
