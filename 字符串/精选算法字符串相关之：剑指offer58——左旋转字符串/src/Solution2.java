/**
 * Created by L.jp
 * Description:
 * User: 86189
 * Date: 2022-01-15
 * Time: 22:44
 */
public class Solution2 {
    //法二：
//    字符串左旋的规律：
//    要左旋n次,就把字符串分为[0,n]和[n+1,len-1]逆置
//    然后再把字符串整体[0,len-1]逆置就是最终的结果
    public static void Reverse(char[] strs,int start,int end){
        while(start<end){
            char tmp=strs[start];
            strs[start] = strs[end];
            strs[end] = tmp;
            start++;
            end--;
        }
    }
    public  static String LeftRotateString(String str,int n) {
        if(str==null || n==0){
            return str;
        }
        int len=str.length();
        char[] strs = str.toCharArray();
        Reverse(strs,0,n-1);
        Reverse(strs,n,len-1);
        Reverse(strs,0,len-1);
        return  new String(strs);
    }

    public static void main(String[] args) {
        System.out.println(LeftRotateString("123abc", 3));
    }
}
