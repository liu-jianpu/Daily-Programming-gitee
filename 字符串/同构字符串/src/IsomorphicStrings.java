import java.util.HashMap;
import java.util.Map;

/**
 * Created by L.jp
 * Description:
 * User: 86189
 * Date: 2021-10-29
 * Time: 20:44
 */
public class IsomorphicStrings {
    //法三，另外创建一个函数，调用isIsomorphicHelp两次，分别传入（s,t），（t,s）,较快些
    public static boolean isIsomorphicHelp(String s,String t){
        Map<Character,Character> map=new HashMap<>();
        for(int i=0;i<s.length();i++){
            char ch=s.charAt(i);
            char ch1=t.charAt(i);
            if(map.containsKey(ch)){
                if(map.get(ch)!=ch1){//映射关系不相等
                    return false;
                }
            }else{
               map.put(ch,ch1);//不包含时，放入ch和ch1构成映射关系
            }
        }
        return true;

    }
    public static boolean isIsomorphic(String s, String t) {

        //法一：创建一个map存放映射关系，时间较快
        /*
        Map<Character,Character> map=new HashMap<>();//存放s和t的映射关系
        for(int i=0;i<s.length();i++){
            char ch=s.charAt(i);
            if(!map.containsKey(ch)){//第一次，如果map的key值不包含ch，也就是有多个ch
                if(map.containsValue(t.charAt(i))) {//但是map的value值已经存在了
                    return false;//也就是说出现了一对多的关系，返回false
                }
                map.put(s.charAt(i),t.charAt(i));//key和value都是只出现了一次
            }else{
                if(map.get(s.charAt(i))!=t.charAt(i)){
                    return false;//如果ch已经存在了，但是ch对应的映射关系字符不等于t中字符串的字符
                }
            }
        }
        return true;//没有发生冲突，返回false
    */

        //法二，创建两个map存放s->t和t->s的关系,时间较慢
        /*
        Map<Character,Character> st=new HashMap<>();
        Map<Character,Character> ts=new HashMap<>();
        for(int i=0,j=0;i<s.length();i++,j++){
            char ch=s.charAt(i);
            char ch1=t.charAt(j);
            if(!st.containsKey(ch)){
                st.put(ch,ch1);//存放s对t的关系
            }
            if(!ts.containsKey(ch1)){
                ts.put(ch1,ch);//存放t对s的关系
            }
            if((st.containsKey(ch) && st.get(ch)!=ch1 || (ts.containsKey(ch1) && ts.get(ch1)!=ch){
                return false ;//不符合映射关系
            }
        }
        return true;

         */
        //法三，调用isIsomorphicHelp两次
        return isIsomorphicHelp(s,t)  && isIsomorphicHelp(t,s);
    }
    public static void main(String[] args) {
        String s="paper";
        String t="title";
        System.out.println(isIsomorphic(s, t));
    }
}
