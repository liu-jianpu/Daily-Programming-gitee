/**
 * Created by L.jp
 * Description:给定一个长度为 n 的字符串，请编写一个函数判断该字符串是否回文。如果是回文请返回true，否则返回false。
 *
 * 字符串回文指该字符串正序与其逆序逐字符一致。
 * User: 86189
 * Date: 2022-02-01
 * Time: 18:00
 */
public class Solution {
    public boolean judge (String str) {
        // write code here
        int len=str.length();
        char[] chars = str.toCharArray();
        int start=0;
        int end=len-1;
        while(start < end){
            if(chars[start++] !=chars[end--]){
                return false;
            }
        }
        return true;
    }

}
