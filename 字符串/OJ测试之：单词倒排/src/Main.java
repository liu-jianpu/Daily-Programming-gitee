import java.util.*;
/**
 * Created by L.jp
 * Description:
 * 链接：https://www.nowcoder.com/questionTerminal/81544a4989df4109b33c2d65037c5836
 * 来源：牛客网
 *
 * 对字符串中的所有单词进行倒排。
 *
 * 说明：
 *
 * 1、构成单词的字符只有26个大写或小写英文字母；
 *
 * 2、非构成单词的字符均视为单词间隔符；
 *
 * 3、要求倒排后的单词间隔符以一个空格表示；如果原字符串中相邻单词间有多个间隔符时，倒排转换后也只允许出现一个空格间隔符；
 *
 * 4、每个单词最长20个字母；
 *
 * 数据范围：字符串长度满足 1 \le n \le 10000 \1≤n≤10000
 *
 * 输入描述:
 * 输入一行，表示用来倒排的句子
 *
 *
 *
 * 输出描述:
 * 输出句子的倒排结果
 *
 * 示例1
 * 输入
 * I am a student
 * 输出
 * student a am I
 * 示例2
 * 输入
 * $bo*y gi!r#l
 * 输出
 * l r gi y bo
 * User: 86189
 * Date: 2022-04-20
 * Time: 18:56
 */
public class Main {
    //首先判断字符是否合法
    public static boolean isWord(char ch){
        return (ch>='a' && ch<='z') || (ch>='A' && ch<='Z');
    }
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        while (scan.hasNext()) {
            String str = scan.nextLine();
            char[] strArr = str.toCharArray();
            //把整个字符串里不是字母的都换为空格
            for (int i = 0; i < strArr.length; i++) {
                if (!(isWord(strArr[i]))) {
                    strArr[i] = ' ';
                }
            }
            //然后重新包装成字符串
            str = new String(strArr);
            //变成一个字符串数组，然后按照空格分割，以便于倒序打印
            String[] strArr2 = str.split(" ");
            //使用StringBuilder进行拼接字符串
            StringBuilder sb = new StringBuilder();
            for (int i = strArr2.length - 1; i >= 0; i--) {
                sb.append(strArr2[i]).append(" ");
            }
            //对一些多余的空格进行处理
            String str2 = sb.toString().trim();
            System.out.println(str2);
        }
    }

}