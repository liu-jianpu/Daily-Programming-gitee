import java.util.Scanner;
/**
 * Created by L.jp
 * Description:读入一个字符串str，输出字符串str中的连续最长的数字串
 * User: 86189
 * Date: 2022-04-08
 * Time: 9:22
 */
public class Main{
    public static void main(String[] args){

        Scanner scan=new Scanner(System.in);
        String str=scan.nextLine();
        //定义两个字符串，一个是临时数字串的字符串，一个是存储最长数字串的字符串
        //当遍历到数字的时候，拼接数字字符
        //当遍历到非数字的时候，就要判断最长数字串长度是否超过上一次数字串的长度，如果没有超过，那么就要更新他为最长数字串
        //遍历结束就输出最长数字串
        String cur="";//临时数字串
        String max="";//最长数字串
        int i=0;
        for( i=0;i<str.length();i++){
            char ch=str.charAt(i);
            if(ch>='0' && ch<='9'){
                cur=cur+ch+""; //拼接“” 才算是字符串
            }else{      //如果最后是最长的数字串，那么这个条件不能够更新最长数字串，所以还需要在循环外面判断
                //不是数字，那么就需要判断是否需要更新最长数字串
                //如果临时字符串大于最长数字串，那么需要先更新最长串，再置空临时串
                if(cur.length()>max.length()){
                    //超过就更新最长数字串
                    max=cur;
                }
                //遇到把临时数字串置空
                cur="";
            }
        }
        //这个条件用于判断最后一个串时数字串的情况，如果最后一个串是最长数字串那么两个条件必须同时满足
        if(i==str.length() && cur.length()>max.length()){
            max=cur;
        }
        //这样既可以满足上面这个条件也可以满足通用条件
        System.out.println(max);
    }
}