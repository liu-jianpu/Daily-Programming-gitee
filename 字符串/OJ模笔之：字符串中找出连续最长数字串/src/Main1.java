import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
/**
 * Created by L.jp
 * Description:读入一个字符串str，输出字符串str中的连续最长的数字串
 * User: 86189
 * Date: 2022-04-08
 * Time: 10:53
 */
public class Main1 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String str = scanner.nextLine();
        char[] chars = str.toCharArray();
        int i = 0;
        StringBuilder s=new StringBuilder();
        //哈希表存放数字串的长度和对应数字串
        Map<Integer,String> map=new HashMap<Integer, String>();
        for (; i < chars.length; i++) {
            if(chars[i] >='0' && chars[i] <= '9'){
                s.append(chars[i]); //是数字就拼接数字字符
            }else{
                //不是数字字符，就把之前的数字字符串的长度和数字串放入哈希表
                map.put(s.length(),s.toString());
                //然后置空
                s=new StringBuilder();
            }
        }
        //判断最后一个串是数字串的情况，那么也把刚刚最后一个数字串加入哈希表
        if(i==chars.length){
            map.put(s.length(),s.toString());
        }
        //寻找最长长度
        int max=0;
        for(int len:map.keySet()){
            max=Math.max(len,max);
        }
        //打印出最长长度的数字串
        System.out.println(map.get(max));
    }
}
