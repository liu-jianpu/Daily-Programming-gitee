import java.util.Scanner;

/**
 * Created by L.jp
 * Description:链接：https://www.nowcoder.com/questionTerminal/aed1c7bbc2604e7c9661a2348b0541b8?pos=46&mutiTagIds=579
 * 来源：牛客网
 *
 * 从C/C++转到Java的程序员，一开始最不习惯的就是变量命名方式的改变。C语言风格使用下划线分隔多个单词，例如“hello_world”；而Java则采用一种叫骆驼命名法的规则：除首个单词以外，所有单词的首字母大写，例如“helloWorld”。
 * 请你帮可怜的程序员们自动转换变量名。
 * User: 86189
 * Date: 2022-04-20
 * Time: 18:39
 */
public class Main {
    public static void main(String[] args){
        Scanner scanner = new Scanner(System.in);
        while (scanner.hasNextLine()) {
            String str=scanner.nextLine();
            for(int i=0;i<str.length();i++){
                //是下划线就跳过判断，使用continue
                if(str.charAt(i)=='_'){
                    continue;
                }
                //不是就判断当前字符的前一个字符是不是下划线
                if(i>0  && str.charAt(i-1) =='_'){
                    //是就换为大写
                    System.out.print((char)(str.charAt(i)-32));
                } else {
                    System.out.print(str.charAt(i));
                }
            }
            System.out.println();
        }
    }
}

