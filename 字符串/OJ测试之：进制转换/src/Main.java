import java.util.Scanner;
import java.util.Stack;

/**
 * Created by L.jp
 * Description: 描述
 * 给定一个十进制数M，以及需要转换的进制数N。将十进制数M转化为N进制数
 * 输入描述：
 * 输入为一行，M(32位整数)、N(2 ≤ N ≤ 16)，以空格隔开。
 * 输出描述：
 * 为每个测试实例输出转换后的数，每个输出占一行。如果N大于9，则对应的数字规则参考16进制（比如，10用A表示，等等）
 * 示例1
 * 输入：
 * 7 2
 *
 * 输出：
 * 111
 * User: 86189
 * Date: 2022-01-20
 * Time: 15:54
 */
public class Main {
    //把十进制转换为N进制，就是不断除以N，然后求余，但是求余得到的结果跟目标结果是反的，所以最后需要逆置一下
    //对于逆置我们可以使用逆置函数，也可以使用栈的特性去解决
    // 这里由于涉及到16进制的字母表示，如果在求余是根据余数结果判断'A‘到'F'，那么最终直接打印的话，还是会变成字母的ASCII码值
    //而不是我们想要的字母表示
    //所以应该把能表示2进制到16进制的数放在一个字符串里
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int m = sc.nextInt();//十进制数
        int N = sc.nextInt();//要转换的进制
        if(m==0){
            System.out.println(0);
        }
        String str="0123456789ABCDEF";//下标就代表着要转换的字符
        boolean flag=false;//不是负数
        if(m<0){
            m=-m;
            flag = true;//是负数
        }
       /* StringBuilder sb = new StringBuilder();//利用它去拼接会比较好
        while(m!=0){
            sb.append(str.charAt(m%N));//如果只是拼接m%N,对于数字好使，但是当余数大于9的时候就不能是简单拼接余数，而是要拼接对应的字母表示，所以利用好这个str字符串很重要
            m=m/N;
        }
        if(flag){
            //如果flag还是真的，说明flag是负数，需要拼接上符号
            sb.append("-");
        }
        //之后逆置拼接的结果就是想要的答案
        sb.reverse();
        System.out.println(sb);*/

        //或者是利用栈的特性存储余数
        Stack<Integer> stack=new Stack<>();//存放余数
        while(m!=0){
            stack.push(m%N);
            m=m/N;
        }
        if(flag){
            System.out.print("-");
        }
        int len=stack.size();
        for(int i=len;i>0;i--){
            System.out.print(str.charAt(stack.pop()));
        }
    }
}
