/**
 * Created by L.jp
 * Description:
 * User: 86189
 * Date: 2022-01-22
 * Time: 15:52
 */
public class Main {
    public static  int StrToInt(String str) {
        char[] chars=str.toCharArray();
        if(chars==null || chars.length==0){
            return 0;
        }
        int i=0;
        int flag=1;//方便带上符号的结果
        if(chars[0]=='+'){
            chars[0]='0';//当数组第一个是‘+’时，我们还是需要拼接后面的
            //但是如果不加这个条件的话，那么根据下面的判断就会返回0
            //所以为了+号不影响后面的计算，那么就令0下标是字符0，这样if条件不满足，也就不会退出
            //也能正常拼接后面其他的字符，'0'-'0'=0，所以还是不影响结果
        }
        if(chars[0]=='-'){
            flag=-1;
            chars[0]='0';//同理
        }
        //i下标等于0时不一定就是+-/所以我们依旧需要从0下标开始转换整数
        //转换整数可以利用字符-‘0’得到整数
        //但是最后的结果是需要拼接起来的，用sum*10+chars[i]-'0';
        int sum=0;//用于拼接结果，也是最后的结果
        for( i=0;i<chars.length;i++){
            if(chars[i]<'0' ||  chars[i]>'9'){
                //不是合法的数字字符，返回0
                sum=0;//需要返回的是sum，所以事先置为0
                break;
            }
            //否则拼接结果
            sum=sum*10+(chars[i]-'0');
        }
        return flag*sum;
    }
    public static void main(String[] args) {
        String s="+124434322";
        System.out.println(StrToInt(s));
    }
}
