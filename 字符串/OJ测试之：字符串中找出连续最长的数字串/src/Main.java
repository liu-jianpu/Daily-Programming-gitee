import java.util.Scanner;

/**
 * Created by L.jp
 * Description:【字符串中找出连续最长的数字串】读入一个字符串str，输出字符串str中的连续最长的数字串
 * 输出描述：
 * 在一行内输出str中里连续最长的数字串。
 * 示例1：
 * 输入
 * abcd12345ed125ss123456789
 * 输出：
 * 123456789
 * User: 86189
 * Date: 2022-01-18
 * Time: 20:29
 */
//遍历字符串，使用cur去记录连续的数字串，如果遇到不是数字字符，则表示一个连续的数字串结束了，则将数
//        字串跟之前的数字串比较，如果更长，则更新更长的数字串更新到res。
public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String str = scanner.nextLine();
        //定义遍历到的当前数字字符串,令它为空串
        String cur="";
        //定义存储最长的数字串，只要当前数字串的长度大于最长数字串，那么就让最长数字串指向当前数字串
        String max="";
        int i=0;
        for(;i<str.length(); i++){
            char ch=str.charAt(i);
            if(ch>='0' && ch<='9'){
                cur=cur+ch+"";
            }else{
                //如果遇到的不是一个数字字符，说明前面的连续数字串已经遍历完了，可以判断当前数字串和最长数字串的关系了
                if(cur.length()>max.length()){
                    max = cur;//让最长数字串指向当前数字串
                }
                //不管是长还是短，都要清空原来的数字串，以便下一步cur的拼接
                cur="";
            }
        }
        ////处理123abc123456这种情况，如果不加这个的话，会导致这种情况的最后一个连续最长数字串赋值不到max中，所以要加上判断
        if (i == str.length() && cur.length() > max.length()) {
            max = cur;
        }
        System.out.println(max);

    }

}