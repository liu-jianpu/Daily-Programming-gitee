import java.util.Scanner;

/**
 * Created by L.jp
 * Description:
 * 数根可以通过把一个数的各个位上的数字加起来得到。如果得到的数是一位数，那么这个数就是数根；如果结果是两位数或者包括更多位的数字，
 * 那么再把这些数字加起来。如此进行下去，直到得到是一位数为止。
 * 比如，对于24 来说，把2 和4 相加得到6，由于6 是一位数，因此6 是24 的数根。
 * 再比如39，把3 和9 加起来得到12，由于12 不是一位数，因此还得把1 和2 加起来，最后得到3，这是一个一位数，因此3 是39 的数根。
 * 现在给你一个正整数，输出它的数根。
 *
 * 输入描述:
 * 输入包含多组数据。
 *
 * 每组数据数据包含一个正整数n（1≤n≤10E1000）。
 *
 *
 * 输出描述:
 * 对应每一组数据，输出该正整数的数根。
 * 示例1
 * 输入
 * 24
 * 39
 * 输出
 * 6
 * 3
 * User: 86189
 * Date: 2022-03-22
 * Time: 18:36
 */
public class Main {
    public static void main(String[] args) {
        //n的最大范围过大，所以使用int,和long都不行，可以只用string 类型，我们直接使用charAt函数就可以获得每个位上的字符，然后再减去0字符就是数字了，比整型数值计算方便
        Scanner scanner=new Scanner(System.in);
        while (scanner.hasNext()) {
            String n = scanner.nextLine();
            //超过两位数，需要一直循环，知道一维数为止
            while (n.length()>1){
                //累加字符并转为数字
                int sum=0;
                for(int i = 0; i <n.length(); i++) {
                    sum += n.charAt(i) - '0';
                }
                //累加完之后，再次把结果转化为字符串，进行判断‘
                n=String.valueOf(sum);
            }
            //当字符串长度小于1时，说明是一位数，直接打印结果,可以直接打印数字字符串，其实和数字是一样的效果
            System.out.println(n); //打印的不是字符，而是字符串，如果是字符的话，需要减去’0‘得到整数结果
        }

    }
}
