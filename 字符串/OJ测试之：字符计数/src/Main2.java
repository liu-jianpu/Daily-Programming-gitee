import java.util.Scanner;

/**
 * Created by L.jp
 * Description:
 * User: 86189
 * Date: 2022-04-27
 * Time: 16:15
 */
public class Main2 {
    public static void main(String[] args) {
       //还有一种做法就是把字符串的计数操作看成是像10进制的计数操作，只不过变成了26进制的计数操作，计算思想是一样的。
        // 思想就是计算方法的转化，由于a~z是有26个字母的，所以计算字符串的个数我们就是在利用Ascii码来计算字符串的个数
        // 所以做加减法的时候我们可以看出是26进制的数字的计算，就想我们计算十进制的数字一样要有高位和低位之分，高位在数组里面就是
        // 最前面的字符，低位就是最后面的字符，高位的次幂更高，我们计算十进制的时候就是   该位的数字*几进制的几次幂
        //次幂的计算是这么来的，首先他跟我们的长度有关，我们可以看成所有的数字或者字符串都是在一个数组里面的东西
        //那么次幂就等于（指定的长度-该位字符在数组里的位置-1），这是一个固定的规律，不管是对于几进制运算还是字符串的运算都是通用的
        
        //当s1和s2的长度不够的len1或者len2的时候，我们就可以采取补齐的方式，在s1后面补‘a',在s2后面补’z+1',
        // 因为只有这样，在计算两个字符串中间的字符串个数的时候才是26的倍数，然而z-a是不行的，他等于25，
        // 补齐的时候，其实补的字符串都算新的字符串，补了几个就算几个
        
        Scanner scan=new Scanner(System.in);
        while(scan.hasNext()){
            //把输入的字符串转化成可拼接的字符串，这样方便补齐字符
            StringBuilder s1=new StringBuilder(scan.next());
            StringBuilder s2=new StringBuilder(scan.next());
            int len1= scan.nextInt();
            int len2= scan.nextInt();
            //补齐字符‘a'到s1后面
            for(int i=s1.length();i<=len2;i++){
                s1.append('a');
            }
            //补齐字符‘z'到s2后面
            for(int i=s2.length();i<=len2;i++){
                s2.append('z'+1);
            }
            //把s1和s2对应位置下的之间的字符个数计算出来，以便以后计算
            int[] count=new int[len2]; //对应s1和s2的每一个坐标
            for(int i=0;i<len2;++i){
                count[i]=s2.charAt(i)-s1.charAt(i); //这里以s1=ab,s2=ce为例，当i=2的时候，count会多计算一个，会把e也算进去，所以最后需要减去他
            }
            //计算字典序在s1和s2中间的字符串个数
            //从len1到len2长度下的字符串的个数
            long ret=0L;
            //计算有多少个字符串
            for(int i=len1;i<=len2;i++){
                //在不同的长度下需要计算从字符串第一位到指定长度位也就是第i位的字符串个数，字符串里是从第0位到第i-1位
                for(int j=0;j<i;j++){
                    //不同长度下的计数方法是一样的，这个跟十进制的计数方法思想是一样的
                    ret+=count[j]*Math.pow(26,i-j-1);  //次幂就等于（指定的长度-该位字符在数组里的位置-1）
                }
            }
            System.out.println((ret-1)%1000007);
        }
        
    }
}
