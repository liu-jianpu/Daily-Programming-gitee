import java.util.Scanner;
/**
 * Created by L.jp
 * Description:求字典序在 s1 和 s2 之间的，长度在 len1 到 len2 的字符串的个数，结果 mod 1000007。
 * User: 86189
 * Date: 2022-04-14
 * Time: 22:42
 */
public class Main {
    /*  字典序就是s1和s2的对应字符位置从0开始依次比较，如果相等就比较下一个位置的字符。其实就是比较字符的ASCII码值
    ·   长度在len1到len2之间的，这个len1和len2可以超过两个字符串的长度，但是要保证s1对应字符的字典序不超过s2的
        比如  ab ，ce 长度在1~3之间的字符串个数就是，长度在为1的时候有b,c    长度为2的时候有ac~az,ba~bz,ca~cd这些
            长度为3的有aba~abz,aca~acz,.....baa~baz...caa~caz,cba~cdz,..cda~cdz 很多，然后就把他们加起来
            
            就是说长度为len1到len2的字典序在s1在s2的字符串的个数是长度为len1的个数+len+1个数+。。。len2的个数，是累加的
            所以就是下一步的结果是基于上一步的结果之上的，总的结果跟每一步都有关系，这就是动态规划的思想
            所以可以用动态规划的思想来解决
            
          
            
    *
    *
    * */
    public static void main(String[] args) {
        //法一：动态规划做法
        Scanner scanner = new Scanner(System.in);
        while (scanner.hasNext()) {
            //输入s1
            String s1 = scanner.next();
            //输入s2
            String s2 = scanner.next();
            //输入len1和len2
            int len1 = scanner.nextInt();
            int len2 = scanner.nextInt();
            //使用一个数组存储当前长度下字符串的个数
            int[] dp = new int[len2 + 1]; //最长能存储len2
            //由于字符串的个数是在字典序的情况下根据长度来计算的，这个长度不一定就是小于等于s1和s2的长度，所以我们以长度循环
            long ret = 0L;
            for (int i = len1; i <= len2; i++) {
                //这一长度下的字符串的个数等于上一次长度下的个数*26，但是这不是最终的结果，之后还需要再进行加减操作
                //以s1=ab,s2=ce为例子，当i=2的时候，这里的dp[i-1]就是长度为1的时候的字符串的个数，那么dp[i]=2*26=52
                //包括的是aa~az和ba~bz这些数一共有2*26=52个，但是这里有s1之外的数aa
                //当长度大于s1或者s2时，那么就要在原来的字符串上补一些字符使得达到对应的长度，这些补得字符构成的新字符串都是有效的，不用做减操作，直接累加最后的结果即可
                dp[i] = (dp[i - 1] * 26) % 1000007;
                //如果长度没有超过s1，那么还需要在刚刚的结果之上减去s1之外的字符串的个数，
                //也就是减去这些字符串指的是不在s1和s2之间的字符串，只与s1有关
                if (i <= s1.length()) {
                    //如果长度还在s1范围之内就表示没有新的字符不用补其他的字符，就是在原来的基础上减去一些字符串
                    //这里当i=2时，减去的就是aa这个字符==52-1=51
                    dp[i] = dp[i] - (s1.charAt(i - 1) - 'a');
                }
                if (i <= s2.length()) {
                    //如果长度还在s2范围之内就表示没有新的字符不用补其他的字符，就是在原来的基础上加去一些字符串
                    //这里计算的是s2之前剩余的字符串，只与s2有关，加上的是ce到ca之间的字符串，就是ca,cb,cc,cd 4个字符串==51+4=55
                    dp[i] = dp[i] + (s2.charAt(i - 1) - 'a');
                }
                if (i >= len1) {
                    ret += dp[i]; //那么最后的结果就是2+55=57个字符串
                }
            }
            System.out.println((ret - 1) % 1000007); //但是这里面包含了ab本身这个字符串，所以需要减去一个
        }
    }
}

