import com.sun.org.glassfish.gmbal.Description;

/**
 * Created by L.jp
 * Description:字符串压缩。利用字符重复出现的次数，编写一种方法，实现基本的字符串压缩功能。比如，字符串aabcccccaaa会变为a2b1c5a3。若“压缩”后的字符串没有变短，则返回原先的字符串。你可以假设字符串中只包含大小写英文字母（a至z）。
 *
 示例1:
 输入："aabcccccaaa"
 输出："a2b1c5a3"

 示例2:
 输入："abbccd"
 输出："abbccd"
 解释："abbccd"压缩后为"a1b2c2d1"，比原字符串长度更长。

 * User: 86189
 * Date: 2021-12-08
 * Time: 22:48
 */
public class Solution {
 /**
  * @param S:
  * [S]
  * @return String
  * @description TODO
  */
    public String compressString(String S) {
        /*双指针法
        令 i 指向字符串的「首个字符」， j 向前遍历，直到访问到「不同字符」时停止，此时j−i 便是「首个字符」的连续出现次数，
        即可完成首个字符的压缩操作。
        接下来，从下个字符开始，重复以上操作，直到遍历完成即可。
        根据题目要求，最终返回「原字符串」和「压缩字符串」中长度较短的那个。



        int i=0,j=0,len=S.length();//r
        StringBuilder s=new StringBuilder();
        while(i<len){
            while(j<len && S.charAt(i)==S.charAt(j)){
                j++;
            }
            s.append(S.charAt(i)).append(j-i);
            i=j;
        }
        return s.length()<len ? s.toString():S;

         */

        //模拟法
        int cnt=1;//本身一个字符的个数就是1
        if(S.length()==0){
            return "";
        }
        StringBuilder ret=new StringBuilder();//用于拼接
        char ch=S.charAt(0);//先拿到第一个字符
        for(int i=1;i<S.length();i++){//从第二个字符开始往后遍历
            if(S.charAt(i)==ch){//如果第i个字符等于ch
                cnt++;//那么cnt++
            }else{
                ret.append(ch);//如果不等，那么就把上次的ch加入到ret中
                ret.append(cnt);//把字符连续出现的次数也加入其中
                ch=S.charAt(i);//更新ch
                cnt=1;//更新次数为1
            }
        }
        //当遍历到最后时，后面没有字符判断了，那么就需要另外拼接字符和出现的连续次数
        ret.append(ch);
        ret.append(cnt);
        return ret.length()<S.length() ? ret.toString():S;
    }
}
