import java.util.Arrays;
import java.util.Scanner;

/**
 * Created by L.jp
 * Description:
 * 链接：https://www.nowcoder.com/questionTerminal/03ba8aeeef73400ca7a37a5f3370fe68
 * 来源：牛客网
 *
 * 定义一个单词的“兄弟单词”为：交换该单词字母顺序（注：可以交换任意次），而不添加、删除、修改原有的字母就能生成的单词。
 * 兄弟单词要求和原来的单词不同。例如： ab 和 ba 是兄弟单词。 ab 和 ab 则不是兄弟单词。
 * 现在给定你 n 个单词，另外再给你一个单词 x ，让你寻找 x 的兄弟单词里，按字典序排列后的第 k 个单词是什么？
 * 注意：字典中可能有重复单词。
 *
 * 数据范围：1 \le n \le 1000 \1≤n≤1000 ，输入的字符串长度满足 1 \le len(str) \le 10 \1≤len(str)≤10  ， 1 \le k < n \1≤k<n
 *
 * 输入描述:
 * 输入只有一行。
 * 先输入字典中单词的个数n，再输入n个单词作为字典单词。
 * 然后输入一个单词x
 * 最后后输入一个整数k
 *
 *
 * 输出描述:
 * 第一行输出查找到x的兄弟单词的个数m
 * 第二行输出查找到的按照字典顺序排序后的第k个兄弟单词，没有符合第k个的话则不用输出。
 * User: 86189
 * Date: 2022-04-21
 * Time: 16:40
 */
public class Main {
    //查找兄弟单词，没有经过删除，增加，替换的，只是交换了顺序，但是不能和原来的单词一样的单词就是兄弟单词
    public static boolean isBrother(String s1,String s2){
        char[] arr1=s1.toCharArray();
        char[] arr2=s2.toCharArray();
        //首先判断长度是否相等，如果长度不相等就不是兄弟单词
        if(arr1.length!=arr2.length){
            return false;
        }
        //如果长度相等就判断各个字符是否相等,如果内容相等，那么就不是兄弟单词
        String str1=new String(arr1);
        String str2=new String(arr2);
        if (str1.equals(str2)) {
            return false;
        }
        //如果内容不相等，那么对两个字符串进行字典排序，然后再判断是否相等，如果是兄弟单词的话，那么经过字典排序之后，两个单词的内容回事一样的
        Arrays.sort(arr1);
        Arrays.sort(arr2);
        str1=new String(arr1);
        str2=new String(arr2);
        return str1.equals(str2);
    }
    public static void main(String[] args) {
        //输入每组数据，注意每组数据只占一行
        Scanner sc=new Scanner(System.in);
        while (sc.hasNext()){
            //每一行数据看成一个字符串，然后按照空格分割成字符串数组
            String[] str=sc.nextLine().split(" ");
            //先拿到字符串数组第一个元素就是n,表示输入字典单词的个数
            int n=Integer.parseInt(str[0]);
            //然后拿到字典单词
            //使用另一个字符串数组进行存储，以便于后面按照指定单词查找兄弟单词计数
            String[] dic=new String[n];
            //这里可以使用一个循环赋值的方法进行指定区间数组内容的复制，注意这里的i+1，因为第一个元素是输入的n，所以从第二个元素开始
//            for(int i=0;i<n;i++){
//                dic[i]=str[i+1];
//            }
            
            //当然也可以使用arraycopy方法进行数组复制
            System.arraycopy(str, 1, dic, 0, n);
            //然后拿到待查找的单词x
            String x=str[n+1];
            //然后拿到一个数k,表示在查找到的兄弟单词的个数中的第k个
            int k=Integer.parseInt(str[n+2]);
            //开始查找兄弟单词
            //首先定义变量存储兄弟单词的个数
            int count=0;
            //然后定义一个字符串，用于存储查找到的兄弟单词的第k个
            String kWord="";
            //查找之前首先要把字典单词按照字典排序
            Arrays.sort(dic);
            for(int i=0;i<dic.length;i++){
                //然后判断是否是兄弟单词
                if(isBrother(x,dic[i])){
                    //如果是兄弟单词，那么就计数加1
                    count++;
                    //然后判断是否是第k个兄弟单词
                    if(count==k){
                        kWord=dic[i];
                    }
                }
            }
            System.out.println(count);
            //判断k是否符合情况，k必须是小于等于兄弟单词的个数即count
            if(k<=count){
                System.out.println(kWord);
            }
        }
    }
}