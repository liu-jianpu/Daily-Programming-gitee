import java.util.Scanner;
/**
 * Created by L.jp
 * Description:
 * 一块花布条，里面有些图案，另有一块直接可用的小饰条，里面也有一些图案。对于给定的花布条和小饰条，计算一下能从花布条中尽可能剪出几块小饰条来呢？
 * 输入描述:
 * 输入包含多组数据。
 * 每组数据包含两个字符串s,t，分别是成对出现的花布条和小饰条，其布条都是用可见ASCII字符表示的，可见的ASCII字符有多少个，布条的花纹也有多少种花样。花纹条和小饰条不会超过1000个字符长。
 * 输出描述:
 * 对应每组输入，输出能从花纹布中剪出的最多小饰条个数，如果一块都没有，那就输出0，每个结果占一行。
 * User: 86189
 * Date: 2022-04-05
 * Time: 22:07
 */
public class Main {
    public static  int getCount(String s,String t){
        //计算t串在s串中出现了几次，可以利用indexOf函数多次计算t串出现的次数
        int i = s.indexOf(t); // indexOf函数用字符串搜索功能，还有记忆功能
        //如果在s串中找到了t串，就返回他第一次出现的位置，没有找到就返回-1
        //i==-1表示没有找到
        if(i==-1){
            return 0;
        }
        return  1+getCount(s.substring(i+t.length()),t);
    }
    public static void main(String[] args) {
        Scanner scanner=new Scanner(System.in);
        while (scanner.hasNext()) {
            String s=scanner.next();
            String t=scanner.next();
            int ret=getCount(s,t);
            System.out.println(ret);
        }
    }
}
