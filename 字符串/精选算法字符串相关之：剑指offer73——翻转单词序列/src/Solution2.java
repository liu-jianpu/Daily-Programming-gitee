/**
 * Created by L.jp
 * Description:
 * User: 86189
 * Date: 2022-01-16
 * Time: 12:37
 */
//使用于复杂的情况，也适用于全部的情况
public class Solution2 {
    //逆置整个字符串
    public static void  Reverse(StringBuilder sb,int start,int end){
        while(start<end){
            char tmp=sb.charAt(start);
            sb.setCharAt(start, sb.charAt(end));
            sb.setCharAt(end, tmp);
            start++;
            end--;
        }
    }
    //翻转单词
    public static void ReverseWord(StringBuilder sb){
        int start=0;
        int end=1;
        int len = sb.length();
        //设置为start<len才能实现所有单词的翻转，end<len则不能
        while(start<len){
            //start为逆置的起始坐标，end为逆置的结尾坐标
            while(end<len && sb.charAt(end)!=' '){
                end++;
            }
            //、end到了空格，开始逆置逆置这个单词
            Reverse(sb,start,end-1);
            //逆置完之后，寻找下一个
            //此时end还在空格的位置，需要让它跳到不是空格的位置，因为我们已经对多余空格去重，所以每个单词之间只有一个空格了
            start=end+1;
            end=start + 1;

        }

    }
    //去除中间，开头结尾多余空格
    private static StringBuilder removeSpaceAll(String str) {
        int left=0;
        int right=str.length()-1;
        while(str.charAt(left)==' ') left++;
        while(str.charAt(right)==' ') right--;
        StringBuilder sb=new StringBuilder();
        while(left <=right){
            //拼接不包括空格的字符串
            if(str.charAt(left) !=' ' || sb.charAt(sb.length() - 1)!=' '){
                sb.append(str.charAt(left));
            }
            //拼接完了一个
            left++;
        }
        return sb;
    }
    public static String ReverseSentence(String str) {
        //首先删除中间以及开头结尾多余的字符
       StringBuilder sb=removeSpaceAll(str);
       //翻转单词
        ReverseWord(sb);
        //翻转整个字符串
        Reverse(sb,0,sb.length()-1);
        return sb.toString();
    }

    public static void main(String[] args) {
        System.out.println(ReverseSentence("  student.   a   am   I   "));
    }
}
