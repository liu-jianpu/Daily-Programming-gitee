/**
 * Created by L.jp
 * Description:
 * User: 86189
 * Date: 2022-01-16
 * Time: 15:27
 */
public class Solution3 {
    //方法三;
    // 利用类内部的函数实现
    public static String ReverseSentence(String s) {
        //思路：
        //首先要明白翻转的意思，即单词翻转，但是单词的字母不换位置
        //那么我们就可以从后往前拼接单词，怎么拼接呢，利用StringBuilder,只要单词不是无空格的字符串”“，就拼接
        //那么我们还要去除多余的空格，中间去除多余的空格简单，直接创建字符串数组，以空格分隔各个数组元素
        //拼接完后，我们需要把他转换为字符串，然后利用trim()去除首尾的字符串
        //按空格分割字符串单词，达到去除中间空格的方法
        String[] strs = s.split(" ");
        StringBuilder sb = new StringBuilder();
        //从后往前拼接
        for(int i=strs.length - 1; i >= 0; i--){
            //如果是有单词的，那么就拼接
            if(!strs[i].equals("")){
                sb.append(strs[i]).append(" ");//拿出来的单词不带空格，但是我们要求单词之间存在一个空格，就补上
            }
        }
        return sb.toString().trim();//去除首尾多余的空格
    }

    public static void main(String[] args) {
        System.out.println(ReverseSentence("   student.   a  am  I  "));
    }
}
