/**
 * Created by L.jp
 * Description:输入：
 * "nowcoder. a am I"
 * 
 * 返回值：
 * "I am a nowcoder."
 * User: 86189
 * Date: 2022-01-15
 * Time: 23:33
 */
public class Solution {
    //不要求去除开头结尾中间多余的空格，只是完成逆置
    public static void  Reverse(char[] strs,int start,int end){
        while(start<end){
            char tmp=strs[start];
            strs[start] = strs[end];
            strs[end]=tmp;
            start++;
            end--;
        }
    }
    public static String ReverseSentence(String str) {
        char[] strs = str.toCharArray();
        int i=0;//遍历字符串，作为逆置的结束位置
        int j=i;//逆置的起始位置
        int len=str.length();
        while(i<len){
            while(i<len && !Character.isWhitespace(strs[i])) i++;
            //遇到了空格，开始逆置局部字符
            Reverse(strs,j,i-1);//此时i位置是空格
            //逆置完毕，先判断当前i位置是否是空格，是的话就让i++，直到遇到不是空格的为止，让j作为下一个子字符串的起始下标
            while(i<len && Character.isWhitespace(strs[i])) i++;
            j=i;
        }
        //当判断到最后一个子串时，子串的最后一个位置不是空格而是结束标志，此时i也等于len就会退循环，所以最后一个子串没有逆置
        //逆置最后一个子串
        Reverse(strs,j,i-1);
        //整体逆置
        Reverse(strs,0,len-1);
        return new String(strs);
    }

    public static void main(String[] args) {
        System.out.println(ReverseSentence("   student. a am I    "));
    }
}
