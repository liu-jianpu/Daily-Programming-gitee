/**
 * Created by L.jp
 * Description:句子 是一个单词列表，列表中的单词之间用单个空格隔开，且不存在前导或尾随空格。每个单词仅由大小写英文字母组成（不含标点符号）。
 *
 * 例如，"Hello World"、"HELLO" 和 "hello world hello world" 都是句子。
 * 给你一个句子 s 和一个整数 k ，请你将 s 截断 使截断后的句子仅含 前 k 个单词。返回 截断 s 后得到的句子。
 *
 * User: 86189
 * Date: 2022-02-02
 * Time: 20:03
 */
public class Sloution {
    public  static  String truncateSentence(String s, int k) {
        String[] strs=s.split(" ");
        String str=new String();
        int i=0;
        for(;i<k; i++){
            if (i != k - 1) {
                str += strs[i]+" ";
            }
        }
        str+=strs[k-1];
        return str;
    }

    public static void main(String[] args) {
        System.out.println(truncateSentence("Hello how are you Contestant", 4));
    }
}
