/**
 * Created by L.jp
 * Description:给你一个字符串数组 words 和一个字符串 pref
 * 返回 words 中以 pref 作为 前缀 的字符串的数目
 * 字符串 s 的 前缀 就是  s 的任一前导连续字符串。
 * 示例 1
 * 输入：words = ["pay","attention","practice","attend"], pref = "at"
 * 输出：2
 * 解释：以 "at" 作为前缀的字符串有两个，分别是："attention" 和 "attend" 。
 *
 * 示例 2：
 *
 * 输入：words = ["leetcode","win","loops","success"], pref = "code"
 * 输出：0
 * 解释：不存在以 "code" 作为前缀的字符串。
 * User: 86189
 * Date: 2022-02-27
 * Time: 10:57
 */
/*
*   字符串模拟的方法，利用substring的方法，判断主字符串的一定区间是不是等于前缀字符串
*
* */
public class Solution {
    public static int prefixCount(String[] words, String pref) {
      int n=pref.length();
      int ret=0;
      for(String  word:words){
          //主字符串的长度都小于前缀字符串的长度，那么就跳过这个单词
          if(word.length()<pref.length()){
              continue;
          }
          //如果在前缀字符串长度的这个区间，主字符串这个区间的单词都等于前缀字符串的单词，那么个数就++;
          if(word.substring(0,n).equals(pref)){
              ret++;
          }
      }
      return  ret;
    }

    public static void main(String[] args) {
        String[] words={"pay","attention","practice","attend"};
        String pref="at";
        System.out.println(prefixCount(words, pref));
    }

}
