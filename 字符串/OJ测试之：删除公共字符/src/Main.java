import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;

/**
 * Created by L.jp
 * Description:描述
 * 输入两个字符串，从第一字符串中删除第二个字符串中所有的字符。例如，输入”They are students.”和”aeiou”，则删除之后的第一个字符串变成”Thy r stdnts.”
 * 输入描述：
 * 每个测试输入包含2个字符串
 * 输出描述：
 * 输出删除后的字符串
 * 示例1
 * 输入：
 * They are students.
 * aeiou
 * 输出：
 * Thy r stdnts.
 * User: 86189
 * Date: 2022-01-17
 * Time: 11:34
 */
public class Main {
    //思路：
    //将公共串入哈希表，然后再遍历主串，如果哈希表中没有就拼接
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        //输入主串
        String str = scanner.nextLine();
        //输入要删除的公共串
        String ss = scanner.nextLine();
        //用于新串的拼接
        StringBuilder sb = new StringBuilder();
        //哈希表去重，拼接公共串
        Set<Character> set = new HashSet<>();
        for (char ch : ss.toCharArray()) {
            set.add(ch);
        }
        //查找要删除的字符，拼接
        for (char ch : str.toCharArray()) {
            if (!set.contains(ch)) {
                sb.append(ch);
            }
        }
        System.out.println(sb.toString());
    }
}
