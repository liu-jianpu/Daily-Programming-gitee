/**
 * Created by L.jp
 * Description:给你一份『词汇表』（字符串数组）words和一张『字母表』（字符串）chars。
 *
 * 假如你可以用chars中的『字母』（字符）拼写出 words中的某个『单词』（字符串），那么我们就认为你掌握了这个单词。
 *
 * 注意：每次拼写（指拼写词汇表中的一个单词）时，chars 中的每个字母都只能用一次。
 *
 * 返回词汇表words中你掌握的所有单词的 长度之和。

 * User: 86189
 * Date: 2022-02-10
 * Time: 21:22
 */
/*      思路：拼写每一个单词时，字母表里面的字母只能用一次，对于字符串的这类题，我们可以通过计算字符的个数来解决问题
             问题转化为：只要字母表里面的每个字符出现的个数大于等于词汇表里每个字符出现的个数就可以完成拼接单词
*
* */
public class Solution {
    //计算字符串里各字符的个数
    public static  int[] count(String chars){
        int[] chars_count=new int[26];
        for(char ch:chars.toCharArray()){
            chars_count[ch-'a']++;
        }
        return chars_count;
    }
    //判断是不是可以拼写成功
    public static boolean contains(int[] chars_count,int[] word_count){
        for(int i = 0; i < word_count.length; i++){
            //只要字母表里的字符个数小于词汇表的字符个数就不行
            if(chars_count[i] < word_count[i]){
                return false;
            }

        }
        return true;
    }
    public static int countCharacters(String[] words, String chars) {
        //我们可以通过分别计算字母表和词汇表的字符出现的个数，可以放在一个26大小的数组里面，这是计算字符个数常用的方法
        int[] chars_count=count(chars);
        int ret=0;
        for(String word : words){
            //计算每一个字符串里的字符出现的个数放到数组里面
            int[] word_count=count(word);
            //用一个contains函数来判断字母表里面的字符个数是不是大于等于词汇表里每个单词的字符出现的个数
            if(contains(chars_count,word_count)){//是的话就说明可以拼写出这个单词，然后加上这个单词的长度就可
                ret+=word.length();
            }
        }
        return ret;
    }

    public static void main(String[] args) {
        String[] words={"cat","bt","hat","tree"};
        String chars = "atach";
        System.out.println(countCharacters(words, chars));
    }
}
