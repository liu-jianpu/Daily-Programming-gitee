
import java.util.Scanner;

/**
 * Created by L.jp
 * Description:跟（翻转单词序列）https://www.nowcoder.com/practice/3194a4f4cf814f63919d0790578d51f3?tpId=13&tqId=11197&rp=1&ru=/ta/coding-interviews&qru=/ta/coding-interviews/question-ranking
 * 是一样的道理。
 * 将一句话的单词进行倒置，标点不倒置。比如 I like beijing. 经过函数后变为：beijing. like I；
 * 示例1
 * 输入
 * I like beijing.
 * 输出
 * beijing. like I；
 * User: 86189
 * Date: 2022-01-19
 * Time: 17:50
 */
public class Main {
    public static void main(String[] args) {
        //方法一：直接倒着拼接字符串
       /* Scanner scanner = new Scanner(System.in);
        String str= scanner.nextLine();
        String[] strings=str.split(" ");
        StringBuilder stringBuilder = new StringBuilder();
        //使用字符串拼接，从后面往前拼接字符串数组的每一个字符串
        for(int i = strings.length - 1; i >= 0; i--){
            if(!strings[i].equals("")){
                stringBuilder.append(strings[i]).append(" ");
            }
        }
        System.out.println(stringBuilder.toString());*/

        //方法二：利用规律，先逆置整体再逆置每个的单词，就是最后的结果
        Scanner scanner = new Scanner(System.in);
        String str= scanner.nextLine();
        char[] strs=str.toCharArray();
        int start=0;
        int end=strs.length;
        //先逆置整体
        Reverse(strs,start,end-1);
        //再逆置每个单词
        int i=0;//遍历数组
        int j=i;
        while(i<end){
            while(j<end && strs[j]!=' '){
                j++;
            }
            //j遇到了空格或者是到了最后一个位置，开始逆置这个单词
            Reverse(strs,i,j-1);
            //逆置完，去找下一个单词逆置
            i=j+1;//此时j位置是空格或者当遍历到最后一个位置时，i不在strs[]里面就不会是越界，此时i已经大于end,才会退出循环，打印最终结果
            j=i;//让j从下一个单词的头开始
        }
        //存储逆置好的字符串数组
        System.out.println(new String(strs));
    }
    private  static void Reverse(char[] strs,int start,int end){
        while(start < end){
            char tmp=strs[start];
            strs[start] = strs[end];
            strs[end]=tmp;
            start++;
            end--;
        }
    }
}
