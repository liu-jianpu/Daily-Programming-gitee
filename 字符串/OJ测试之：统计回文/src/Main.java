import java.util.Scanner;

/**
 * Created by L.jp
 * Description:
 * 例如：
 * A = “aba”，B = “b”。这里有4种把B插入A的办法：
 * * 在A的第一个字母之前: "baba" 不是回文
 * * 在第一个字母‘a’之后: "abba" 是回文
 * * 在字母‘b’之后: "abba" 是回文
 * * 在第二个字母'a'之后 "abab" 不是回文
 * 所以满足条件的答案为2
 * 输入描述：
 * 每组输入数据共两行。 第一行为字符串A 第二行为字符串B 字符串长度均小于100且只包含小写字母
 * 输出描述：
 * 输出一个数字，表示把字符串B插入字符串A之后构成一个回文串的方法数
 * 示例1
 * 输入：
 * aba
 * b
 * 输出：
 * 2
 * User: 86189
 * Date: 2022-01-21
 * Time: 19:49
 */
public class Main {
    public static boolean isPalindrome(StringBuilder ab){
        int start=0;
        int end=ab.length()-1;
        while (start<end){
            if (ab.charAt(start++) !=ab.charAt(end--)){
                return false;
            }
        }
        return true;
    }
    public static void main(String[] args) {
        Scanner scan=new Scanner(System.in);
        //输入字符串A和B
        String strA=scan.nextLine();
        String strB=scan.nextLine();
        //然后选择A合适的位置插入B，由题意可知可以插入的位置个数等于A字符串的长度
        //插入的话不能在原有的A字符串上，因为会改变它的长度，所以必须利用拼接字符串来插入
        int len = strA.length();
        int count = 0;
        for(int i = 0; i <= len; i++){
            //法一：利用Insert函数
            //到每一个位置都要新创一个拼接字符串，把strA拷贝给它，然后再插入
            //StringBuilder str = new StringBuilder(strA);
            // str.insert(i,strB);//每个位置插入完毕，insert函数实际就是在当前位置插入新的字符串，原来的字符串后移

            //法二：每到一个位置新建一个拼接字符串，利用A字符串的subString方法，拼接A串不同的区间，共有三步：
            //拼接前面的部分，拼接B串，拼接后面部分
            //对于String和StringBuilder的substring方法都是左闭右开的，也就是从起始索引开始到结尾索引-1的区间
            //一开始的子串和最后的子串实际都是空串
            StringBuilder str = new StringBuilder();
            str.append(strA.substring(0,i));
            str.append(strB);
            str.append(strA.substring(i,len));

            //判断是否回文
            if(isPalindrome(str))
                count++;

        }
        System.out.println(count);

    }
}
