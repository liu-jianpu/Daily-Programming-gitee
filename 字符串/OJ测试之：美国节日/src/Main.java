import java.util.Scanner;

/**
 * Created by L.jp
 * Description:
 * 和中国的节日不同，美国的节假日通常是选择某个月的第几个星期几这种形式，因此每一年的放假日期都不相同。具体规则如下：
 * * 1月1日：元旦
 * * 1月的第三个星期一：马丁·路德·金纪念日
 * * 2月的第三个星期一：总统节
 * * 5月的最后一个星期一：阵亡将士纪念日
 * * 7月4日：美国国庆
 * * 9月的第一个星期一：劳动节
 * * 11月的第四个星期四：感恩节
 * * 12月25日：圣诞节
 * 现在给出一个年份，请你帮忙生成当年节日的日期。
 *
 * 输入描述:
 * 输入包含多组数据，每组数据包含一个正整数year（2000≤year≤9999）。
 *
 *
 * 输出描述:
 * 对应每一组数据，以“YYYY-MM-DD”格式输出当年所有的节日日期，每个日期占一行。
 *
 * 每组数据之后输出一个空行作为分隔。
 * 示例1
 * 输入
 * 2014
 * 2013
 * 输出
 * 2014-01-01
 * 2014-01-20
 * 2014-02-17
 * 2014-05-26
 * 2014-07-04
 * 2014-09-01
 * 2014-11-27
 * 2014-12-25
 *
 * 2013-01-01
 * 2013-01-21
 * 2013-02-18
 * 2013-05-27
 * 2013-07-04
 * 2013-09-02
 * 2013-11-28
 * 2013-12-25
 * User: 86189
 * Date: 2022-04-04
 * Time: 16:00
 */
public class Main {
    private static boolean isLeapYear(int y) {
        return (y % 400 == 0) || (y % 4 == 0 && y % 100 != 0);
    }

    private static final int[] DAYS = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};

    // 给定 y,m,d，返回这年过了多少天了
    private static int nDays(int y, int m, int d) {
        int n = d;
        for (int i = 0; i < m - 1; i++) {
            n += DAYS[i];
        }
        if (m > 2 && isLeapYear(y)) {
            n++;
        }
        return n;// m: 12 // [0, 10]int n = d;
    }

    // 传入 y,m,d，找到从公元前 1 年12月31日开始过了多久了。求出它的 MOD 7 的同余数
    private static int diff(int y, int m, int d) {
        return (y - 1) + (y - 1) / 4 - (y - 1) / 100 + (y - 1) / 400 + nDays(y, m, d);
    }

    // 根据 y,m,d 求出星期几
    private static int week(int y, int m, int d) {
        int w = diff(y, m, d) % 7;
        if (w == 0) {
            w = 7;
        }
        return w;
    }

    // 根据 1 日星期 w，求第 n 个星期 e 是几号
    private static int m1(int w, int n, int e) {
        return 1 + (n - 1) * 7 + (7 - w + e) % 7;
    }

    // 根据 6月1日星期 w，求5月的最后一个星期一
    private static int m2(int w) {
        int d = (w == 1 ? 7 : w - 1);
        return 32 - d;
    }

    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        while (s.hasNextInt()) {
            int y = s.nextInt();
            System.out.printf("%d-01-01\n", y);
            int w;
            w = week(y, 1, 1);
            System.out.printf("%d-01-%02d\n", y, m1(w, 3, 1));

            w = week(y, 2, 1);
            System.out.printf("%d-02-%02d\n", y, m1(w, 3, 1));

            w = week(y, 6, 1);
            System.out.printf("%d-05-%02d\n", y, m2(w));

            System.out.printf("%d-07-04\n", y);

            w = week(y, 9, 1);
            System.out.printf("%d-09-%02d\n", y, m1(w, 1, 1));

            w = week(y, 11, 1);
            System.out.printf("%d-11-%02d\n", y, m1(w, 4, 4));
            System.out.printf("%d-12-25\n", y);

            System.out.println();
        }
    }

}
