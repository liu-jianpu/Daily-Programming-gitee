
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

/**
 * Created by L.jp
 * Description:不分行从上往下打印出二叉树的每个节点，同层节点从左至右打印。例如输入{8,6,10,#,#,2,1}，
 *则依次打印8,6,10,2,1(空节点不打印，跳过)，请你将打印的结果存放到一个数组里面，返回。
 * User: 86189
 * Date: 2021-12-25
 * Time: 13:13
 */
//层次遍历其实就是一个bfs算法，可以直接他用bfs的模板，这道题意思就是模拟层次遍历实现
 class TreeNode {
    int val = 0;
    TreeNode left = null;
    TreeNode right = null;

    public TreeNode(int val) {
        this.val = val;
    }
}
public class Solution {
     //返回类型是链表类型
    /*public ArrayList<Integer> PrintFromTopToBottom(TreeNode root) {
        if(root==null){
            return new ArrayList<>();
        }
        //存放最终层序遍历的结果
        ArrayList<Integer> result=new ArrayList<>();
        //临时存储每个节点
        Queue<TreeNode> queue=new LinkedList<>();
        //先将根节点加入到队列
        queue.offer(root);
        //当队列不为空时
        while(!queue.isEmpty()){
            //将队头元素也就是每次遍历到的第一个节点弹出，然后获取他，把他的值加入到数组中
            TreeNode tmp=queue.poll();
            result.add(tmp.val);
            //如果弹出的节点左树和右树不为空，那么需要将他们的左右子树加入到队列中，之后循环继续加入节点和值
            if(tmp.left!=null){
                queue.offer(tmp.left);
            }
            if(tmp.right!=null){
                queue.offer(tmp.right);
            }
        }
        return result;*/


    //返回类型是数组类型
    public int []PrintFromTopToBottom(TreeNode root) {
        if(root==null){
            return new int[0];
        }
        //存放最终层序遍历的结果
        ArrayList<Integer> result=new ArrayList<>();
        //临时存储每个节点
        Queue<TreeNode> queue=new LinkedList<>();
        //先将根节点加入到队列
        queue.offer(root);
        //当队列不为空时
        while(!queue.isEmpty()){
            //将队头元素也就是每次遍历到的第一个节点弹出，然后获取他，把他的值加入到数组中
            TreeNode tmp=queue.poll();
            result.add(tmp.val);
            //如果弹出的节点左树和右树不为空，那么需要将他们的左右子树加入到队列中，之后循环继续加入节点和值
            if(tmp.left!=null){
                queue.offer(tmp.left);
            }
            if(tmp.right!=null){
                queue.offer(tmp.right);
            }
        }
        int [] ret=new int[result.size()];
        for(int i=0;i< ret.length;i++){
            ret[i]=result.get(i);
        }
        return ret;

    }

}