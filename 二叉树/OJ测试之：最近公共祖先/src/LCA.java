import java.util.ArrayList;

/**
 * Created by L.jp
 * Description:将一棵无穷大满二叉树的结点按根结点一层一层地从左往右编号，
 * 根结点编号为1。现给定a，b为两个结点。设计一个算法，返回a、b最近的公共祖先的编号。注意其祖先也可能是结点本身。
 * User: 86189
 * Date: 2022-03-04
 * Time: 20:28
 */
//给定一棵无穷大的满二叉树，已经两个节点编号，找到最近公共祖先的编号
public class LCA {
    public static int getLCA(int a, int b) {
       if(a == b){
           return a;
       }else{
           while (a!=b){
               if(a>b){
                   a/=2;
               }else{
                   b/=2;
               }
           }
           return a;
       }
    }
    public static void main(String[] args) {
        System.out.println(getLCA(14, 3));
    }

}
