import javax.swing.tree.TreeNode;

/**
 * Created by L.jp
 * Description:操作给定的二叉树，将其变换为源二叉树的镜像。
 * User: 86189
 * Date: 2021-12-23
 * Time: 21:25
 */
//解题思路：
//仔细观察可以发现，所谓的二叉树镜像本质是自顶向下(or自底向上)进行左右子树交换的过程
    //    8
 //    6        10
  // 5     7   9     11

    //镜像：
//         8
 //   10         6
 // 11   9     7    5
 class TreeNode1 {
    int val = 0;
    TreeNode1 left = null;
   TreeNode1 right = null;
    public TreeNode1(int val) {
      this.val = val;
    }
  }
public class Solution {
    public TreeNode1 Mirror (TreeNode1 pRoot) {
        //递归
        //当递归到节点为空时返回
        if(pRoot==null){
            return pRoot;//这里可以不用再加一条if(pRoot.left==null && pRoot.right==null) return pRoot;因为到后面还是要判断左右子树的情况
        }
        //不为空就交换左右子树节点
        TreeNode1 tmp=pRoot.left;
        pRoot.left=pRoot.right;
        pRoot.right=tmp;
        //交换完后递归交换左右子树节点
        Mirror(pRoot.left);//或者pRoot.left=Mirror(pRoot.left)
        Mirror(pRoot.right);//或者pRoot.right=Mirror(pRoot.right)
        return pRoot;
    }
}
