/**
 * Created by L.jp
 * Description:操作给定的二叉树，将其变换为源二叉树的镜像。
            8                                                8

   6                10                             10                    6

 5    7         9        11                   11       9            7          5
 * User: 86189
 * Date: 2022-03-11
 * Time: 23:42
 */
//仔细观察发现，除了根节点不交换，其余都是交换了左右节点，不是说单纯的交换节点值，我们可以利用递归来解决问题
class TreeNode {
    int val = 0;
    TreeNode left = null;
    TreeNode right = null;

    public TreeNode(int val) {
        this.val = val;

    }

}
public class Solution {
    public TreeNode Mirror(TreeNode pRoot) {
       //如果根节点为空，那么就返回
        if(pRoot == null){
            return pRoot;
        }
        //如果只有一个节点，那么就返回本身节点
        if(pRoot.left == null && pRoot.right == null){
            return pRoot;
        }
        //如果不是空，那就交换左右节点
        TreeNode tmp=pRoot.left;
        pRoot.left=pRoot.right;
        pRoot.right = tmp;
        //如果子节点的左右节点不为空那就递归交换左右节点
        if(pRoot.left!=null){
            Mirror(pRoot.left);
        }
        if (pRoot.right != null) {
            Mirror(pRoot.right);
        }
        return pRoot;
    }
}
