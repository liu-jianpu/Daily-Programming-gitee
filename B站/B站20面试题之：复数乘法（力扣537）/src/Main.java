import java.nio.charset.StandardCharsets;
import java.util.Scanner;

/**
 * Created by L.jp
 * Description:
 * User: 86189
 * Date: 2021-12-05
 * Time: 10:17
 */
//牛客网模式
public class Main {
    public static void main(String[] args) {
        //法一：将复数利用+号和i拆成实数和虚数两部分，根据运算规则写出对应的公式，然后再把它们转换成字符串
//        String[] a=s1.split("\\+|i");
//        String[] b=s2.split("\\+|i");
//        int areal=Integer.parseInt(a[0]);
//        int anoreal=Integer.parseInt(a[1]);
//        int breal=Integer.parseInt(b[0]);
//        int bnoreal=Integer.parseInt(b[1]);
//        return (areal*breal-anoreal*bnoreal)+"+"+(anoreal*breal+areal*bnoreal)+"i";//拼接回字符串
        String s1=null;
        String s2=null;
        Scanner scanner=new Scanner(System.in);
        s1=scanner.nextLine();
        s2=scanner.nextLine();
        //截取整数部分
        String str1=s1.substring(0,s1.indexOf('+'));
        String str2=s2.substring(0,s2.indexOf('+'));
        //截取复数部分
        String img1=s1.substring(s1.indexOf('+')+1);
        String img2=s2.substring(s2.indexOf('+')+1);
        //转换成整型计算
        int s1Int=strToInt(str1);
        int s2Int=strToInt(str2);
        int imgInt1=strToInt(img1);
        int imgInt2=strToInt(img2);

        //最后结果中的整数部分
        int ret=s1Int*s2Int-imgInt1*imgInt2;

        //虚数部分
        int ret2=s1Int*imgInt2+s2Int*imgInt1;

        //拼接结果成字符串
        System.out.println(ret+"+"+ret2+"i");

    }
    public static  int strToInt(String str){
        int num=0;
        int flag=1;
        for(int i=0;i<str.length();i++){
            if(str.charAt(i)=='-'){
                flag=-1;
            }else if(str.charAt(i)>='0' && str.charAt(i)<='9'){
                num=num*10+(str.charAt(i)-'0');//当整数部分是1位数时，可以不用*10，但是整数部分很可能是两位数甚至更多，那么对应的字符也就更多，此时需要拼接成一个整体的整数，就需要*10；
            }
        }
        return num*flag;
    }

}
