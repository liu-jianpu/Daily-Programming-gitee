import java.util.Scanner;

/**
 * Created by L.jp
 * Description:
 * User: 86189
 * Date: 2021-12-05
 * Time: 15:58
 */
public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String date = scanner.nextLine();
        //切割年份
        int year = Integer.parseInt(date.substring(0, 4));//subString参数的从起始下标延伸到结尾下标-1，返回值是这个区间的字符串
        //切割月份
        int month = Integer.parseInt(date.substring(5, 7));
        //切割天数
        int day = Integer.parseInt(date.substring(8));//这行subString的参数是从起始下标开始一直到字符串结尾的下标，返回这个区间的字符串
        int[] monthArray = {0,31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};//存储每个月的天数
        if ((year % 4 == 0 && year % 100 != 0) || (year % 400 == 0)) {
            monthArray[2] = 29;//闰年
        }
        int sum = 0;
        for (int i = 0; i < month; i++) {
            sum += monthArray[i];//先计算前面几个月的天数
        }
        sum += day;//然后再计算这个月的天数
        System.out.println(sum);//返回最终结果

    }
}
