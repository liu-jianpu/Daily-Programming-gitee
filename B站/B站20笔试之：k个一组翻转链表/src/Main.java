import java.util.Scanner;

/**
 * Created by L.jp
 * Description:
 * User: 86189
 * Date: 2021-12-05
 * Time: 20:44
 */
public class Main {
    public static void reserve(String[] str,int start,int end){
        while (start<end){
            String tmp=str[start];
            str[start]=str[end];
            str[end]=tmp;
            start++;
            end--;
        }
    }
    public static void main(String[] args) {
        Scanner scanner=new Scanner(System.in);
        String str=scanner.nextLine();
        String[] list=str.split(" ");
        int start=0;
        int k=scanner.nextInt();
        while(start+k-1<list.length-1){//由于最后一个还有一个“#”，所以需要翻转的只是#以前的，那么下标就是到len-2
            reserve(list,start,start+k-1);
            start=start+k;
        }
        for(int i=0;i< list.length-2;i++){
            System.out.print(list[i]+"->");
        }
        System.out.println(list[list.length-2]);

    }
}
