import jdk.internal.org.objectweb.asm.tree.IincInsnNode;

import java.util.HashSet;
import java.util.List;

/**
 * Created by L.jp
 * Description:给你一个链表的头节点 head ，判断链表中是否有环。
 *
 * 如果链表中有某个节点，可以通过连续跟踪 next 指针再次到达，则链表中存在环。
 * 为了表示给定链表中的环，评测系统内部使用整数 pos 来表示链表尾连接到链表中的位置（索引从 0 开始）。
 * 注意：pos 不作为参数进行传递。仅仅是为了标识链表的实际情况。
 *
 * 如果链表中存在环，则返回 true 。 否则，返回 false 。
 *

 * User: 86189
 * Date: 2023-03-21
 * Time: 10:46
 */
class ListNode {
      int val;
      ListNode next;
      ListNode(int x) {
          val = x;
          next = null;
      }
  }
public class Solution {
    public boolean hasCycle(ListNode head) {
       /* if(head==null || head.next==null){
            return false;
        }
        //双指针,快指针先在前面，然后快指针走两步，慢指针走一步，循环判断快慢指针是否相等
        ListNode slow=head;
        ListNode fast=head.next;
        while (fast != slow) {
            if(fast==null || fast.next==null){
                return false;
            }
            slow=slow.next;
            fast=fast.next.next;
        }
        return true;*/
        
        /*ListNode slow=head;
        ListNode fast=head;
        while ( fast!=null && fast.next != null ){
            slow=slow.next;
            fast=fast.next.next;
            if(fast==slow){
                return true;
            }
        }
        return false;*/
        
        
        //哈希表
        HashSet<ListNode> set=new HashSet<>();
        while (head!=null && head.next!=null){
            //如果不存在指定元素，就添加元素到哈希表，如果存在指定元素就不改变哈希表说明有环，然后返回true
            if(!set.add( head )){
                return true;
            }
            head=head.next;
        }
        return false;
    }
}
