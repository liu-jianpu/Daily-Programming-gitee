import java.util.LinkedList;
import java.util.Queue;

/**
 * Created by L.jp
 * Description:给定一个二叉树，找出其最大深度。
 *
 * 二叉树的深度为根节点到最远叶子节点的最长路径上的节点数。
 *
 * 说明: 叶子节点是指没有子节点的节点。
 * User: 86189
 * Date: 2023-03-21
 * Time: 1:07
 */
class TreeNode {
      int val;
      TreeNode left;
      TreeNode right;
      TreeNode() {}
      TreeNode(int val) { this.val = val; }
      TreeNode(int val, TreeNode left, TreeNode right) {
          this.val = val;
          this.left = left;
          this.right = right;
      }
  }
public class Solution {
    public int maxDepth(TreeNode root) {
        
        //迭代法
        /*if(root==null){
            return 0;
        }
        Queue<TreeNode> que=new LinkedList<TreeNode>();
        que.offer( root );
        int level=1;
        while ( !que.isEmpty()){
            int size=que.size();
            while ( size>0 ){
                TreeNode cur=que.poll();
                if(cur.left!=null){
                    que.offer( cur.left );
                }
                if(cur.right!=null){
                    que.offer(cur.right);
                }
                size--;
            }
            level++;
        }
    
        return level;*/
        
        //递归法
        if(root==null){
            return 0;
        }
        return Math.max( maxDepth( root.left ),maxDepth( root.right ))+1;
    }

}
