import java.util.HashMap;

/**
 * Created by L.jp
 * Description:给你一个 非空 整数数组 nums ，除了某个元素只出现一次以外，其余每个元素均出现两次。找出那个只出现了一次的元素。
 *
 * 你必须设计并实现线性时间复杂度的算法来解决此问题，且该算法只使用常量额外空间。
 * User: 86189
 * Date: 2023-03-21
 * Time: 10:23
 */
public class Solution {
    public static int singleNumber(int[] nums) {
       /* int ret=0;
        for(int i = 0; i < nums.length; i++){
            ret^=nums[i];
        }
        return ret;*/
        
        
        //哈希表
        HashMap<Integer, Integer> map=new HashMap<>();
        for(Integer num : nums){
            map.put( num,map.getOrDefault( num,0 )+1 );
        }
        for(int i = 0; i < nums.length; i++){
            if(map.get( nums[i] )==1){
                return nums[i];
            }
        }
        return 0;
    }
    
    public static void main (String[] args) {
        int[] nums={1,1,2,-1,3,3,4,4};
        System.out.println( singleNumber( nums ) );
    
    }
}
