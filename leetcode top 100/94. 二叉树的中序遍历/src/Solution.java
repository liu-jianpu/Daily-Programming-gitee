import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

/**
 * Created by L.jp
 * Description:
 * User: 86189
 * Date: 2023-03-18
 * Time: 23:05
 */

class TreeNode {
      int val;
      TreeNode left;
      TreeNode right;
      TreeNode() {}
      TreeNode(int val) { this.val = val; }
      TreeNode(int val, TreeNode left, TreeNode right) {
          this.val = val;
          this.left = left;
          this.right = right;
      }
}
public class Solution {
    List<Integer> list=new ArrayList<>();
    public List<Integer> inorderTraversal(TreeNode root) {
        //递归解法
       /* if(root==null){
            return new ArrayList<>();
        }
        if(root.left!=null) {
            inorderTraversal( root.left);
        }
        list.add( root.val );
        if(root.right!=null){
            inorderTraversal( root.right );
        }
        return list;*/
        
        //迭代法,借助栈来存储节点和维持左根右的顺序
        Stack<TreeNode> stack=new Stack<>();
        while ( stack.size()>0 || root!=null ){
            //左
            while ( root.left!=null ){
                stack.push( root );
                root=root.left;
            }
            //列表加入当前节点值也就是根值，保存当前节点
            TreeNode top=stack.pop();
            list.add( top.val );
            //去到刚刚保存节点的右边节点
            root=top.right;
        }
        return list;
    }
    
    //小明写了一段代码，想实现替换文件部分内容的目标，但是代码似乎写得有些问题，请你纠正并优化这段代码，并给出你的优化逻辑，优化点多多益善。
    public static void main(String[] args) throws IOException {
        File file = new File("some file path");
        //List需要指定为String泛型参数比较好，泛型是一种编译期间的擦除机制，可以有效提高编码效率
        List<String> list1 = new ArrayList<String>();
        FileInputStream fileInputStream = new FileInputStream(file);
        byte[] bytes = new byte[1024];
        int i = fileInputStream.read(bytes);
        if(i > 0) {
            do {
                i = fileInputStream.read(bytes);
                String s = new String(bytes, 0 , i);
                list1.add(s);
            } while(i != -1);
        }
        i = fileInputStream.read(bytes);
        String start = new String(bytes, 0, i);
        list1.add(start);
        for (int i1 = 0; i1 < list1.size(); i1++) {
            String s = list1.get(i1);
            if(s.contains("1")) {
                s = s.replace("1", "a");
            } else if(s.contains("2")) {
                s = s.replace("2", "b");
            } else if(s.contains("3")) {
                s = s.replace("3", "c");
            } else if(s.contains("4")) {
                s = s.replace("4", "d");
            }
            list1.remove(i1);
            list1.add(i1, s);
        }
        FileOutputStream fos = new FileOutputStream(file);
        for (Object o : list1) {
            fos.write(o.toString().getBytes());
        }
        //还有就是使用完输入输出流之后，必须及时关闭流文件
        fos.close();
        fileInputStream.close();
    }
}