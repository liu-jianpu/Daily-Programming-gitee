import java.util.Scanner;

/**
 * Created by L.jp
 * Description:输入一个自然数n，求表达式 f(n) = 1! * 2! * 3!.....n! 的结果末尾有几个连续的0？
 * 此题的要求是求解末尾0的个数，如果要产生0，则需要产生因子10，所以相当于需要计算出10的数量。
 * 但是10不是最小的因子，他还可以分解成2 * 5, 所以需要找出2和5的个数，最后取两者中最小的，即为0的个数。
 * 但是实际我们只需要求出因子中5的个数就可以了
 * User: 86189
 * Date: 2021-12-08
 * Time: 13:07
 */
//方法：
    //计算i的乘积因子中5的个数
    //计算从i!到n!中i的个数
    //两者相乘就是为阶乘因子中5的个数即末尾0的个数
public class Main {
    public static int count(int num,int n){
        int cnt=0;//记录num中乘积因子可以有多少个n
        while(num>=n && num%n==0){//必须是大于n，并且可以整除n
            cnt++;
            num/=n;//继续除以n,判断缩小n倍之后的数有多少个n乘积因子

        }
        return cnt;
    }
    public static void main(String[] args) {
        Scanner scanner=new Scanner(System.in);
        int n=scanner.nextInt();//输入n,代表从1！到n!的乘积
        int num5=0;//阶乘因子中5的个数
        for(int i=5;i<=n;i++){//直接从5开始判断
            num5+=count(i,5)*(n-i+1);//每一次循环都要计算i的乘积因子中包含5的个数再乘上从i!到n!的乘积因子中i的个数，这个乘积就是阶乘因子中5的个数
        }
        System.out.println(num5);
    }
}
