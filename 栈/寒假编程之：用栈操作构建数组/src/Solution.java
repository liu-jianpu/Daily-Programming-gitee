import java.util.ArrayList;
import java.util.List;

/**
 * Created by L.jp
 * Description:给你一个目标数组 target 和一个整数 n。每次迭代，需要从 list = {1,2,3..., n} 中依序读取一个数字。
 *
 * 请使用下述操作来构建目标数组 target ：
 *
 * Push：从 list 中读取一个新元素， 并将其推入数组中。
 * Pop：删除数组中的最后一个元素。
 * 如果目标数组构建完成，就停止读取更多元素。
 * 题目数据保证目标数组严格递增，并且只包含 1 到 n 之间的数字。
 *
 * 请返回构建目标数组所用的操作序列。
 *
 * 题目数据保证答案是唯一的。

 * Date: 2022-02-13
 * Time: 23:22
 */
public class Solution {
    public static  List<String> buildArray(int[] target, int n) {
        //不用辅助栈，直接用从1~n里面的数字依次对比，如果等于就做push操作，如果不等于就做push+pop操作，如果不涉及到的数字就不用管
        List<String> list=new ArrayList<>();//存放最终结果
        for(int num=1,i =0;num<=n && i<target.length;num++){  //这里只需要控制num严格++，但是数组下标不是一直++的
            if(num!=target[i]){   //注意num<=n && i<target.length这个条件，只要下标走完了数组，那么就表示所有操作完成，对于其他的num就不用管
                //但是有一种情况，就是这个num在n的范围，而且这个时候i还没有走完数组，那么就表示这个num是被push然后又被pop出来的
                //这个时候，数组下标还不用++,因为还没有处理当前下标元素
                list.add("Push");
                list.add("Pop");
            }else{
                //当数组元素跟数字相等时，表示做了push操作，然后就可以走下一个数组下标了
                list.add("Push");
                i++;
            }

        }
        return  list;
    }

    public static void main(String[] args) {
        int[] target={2,3,5};
        int n=7;
        System.out.println(buildArray(target, n));
    }
}
