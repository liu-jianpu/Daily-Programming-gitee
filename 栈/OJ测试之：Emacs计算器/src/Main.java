import jdk.internal.org.objectweb.asm.tree.IincInsnNode;

import java.util.Scanner;
import java.util.Stack;

/**
 * Created by L.jp
 * Description:
 * Emacs号称神的编辑器，它自带了一个计算器。与其他计算器不同，它是基于后缀表达式的，即运算符在操作数的后面。例如“2 3 +”等价于中缀表达式的“2 + 3”。
 * 请你实现一个后缀表达式的计算器。
 * User: 86189
 * Date: 2022-04-16
 * Time: 18:44
 */
public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        while (scanner.hasNext()) {
            int n = scanner.nextInt();
            //由于要输入的既包括字符又包括数字，
            // 如果要把他们放在一个数组里，那么不能是int的数组也不能是char的数组，因为要采用next输入，必须是字符串类型
//            String[] express=new String[n];
//            Stack<Integer> stack=new Stack<>();
//            for(int i = 0; i < n; i++){
//                express[i]=scanner.next();
//                if(!(express[i].equals("+")|| express[i].equals("-") || express[i].equals("*") || express[i].equals("/"))){
//                    //如果是数字就加入栈
//                    stack.push(Integer.parseInt(express[i]));
//                }else{
//                    //不是数字就要弹出两个操作数进行运算
//                    int right=stack.pop();//先弹出的
//                    int left=stack.pop();//后弹出的
//                    switch (express[i]) {
//                        case "+":
//                            stack.push(left + right); //先弹出的操作数在右边
//                            break;
//                        case "-":
//                            stack.push(left - right);
//                            break;
//                        case "*":
//                            stack.push(left * right);
//                            break;
//                        case "/":
//                            stack.push(left / right);
//                            break;
//                    }
//                }
//            }
//            System.out.println(stack.pop());
//        }
            String expr;
            Stack<Integer> stack = new Stack<>();
            for (int i = 0; i < n; i++) {
                expr = scanner.next();
                if (!(expr.equals("+") || expr.equals("-") || expr.equals("*") || expr.equals("/"))) {
                    stack.push(Integer.parseInt(expr));
                } else {
                    int right = stack.peek();
                    stack.pop();
                    int left = stack.peek();
                    stack.pop();
                    switch (expr.charAt(0)) {
                        case '+':
                            stack.push(left + right);
                            break;
                        case '-':
                            stack.push(left - right);
                            break;
                        case '*':
                            stack.push(left * right);
                            break;
                        case '/':
                            stack.push(left / right);
                            break;
                        default:
                            break;
                    }
    
                }
            }
            System.out.println(stack.peek());
        }
    }
}
