import java.util.Stack;

/**
 * Created by L.jp
 * Description:
 * User: 86189
 * Date: 2022-02-14
 * Time: 23:47
 */
public class MyQueue2 {
    Stack<Integer> stack1=new Stack<>();//实现队列
    Stack<Integer> stack2=new Stack<>();//辅助栈
    //这是时间复杂度的优化，我们只需要对一个push操作有O(N)的时间复杂度，其他的都是o(1)
    //原理是什么：
    //  我们只要保证新的元素在老的元素之后就可以了，这样就实现了队列的排布，那么怎么利用栈的排布实现队列的排
    public MyQueue2() {

    }
    public void push(int x) {
        //这个过程就是要把栈里的元素实现队列的排布，当加入新的元素时，要保证新的元素在旧的元素的后面
        //元素入队列
        if(stack1.isEmpty()){
            stack1.push(x);
        }else{
            //要加入新元素时，要变成队列的排布，就不能直接入到stack1里面，否则成了栈排布
            // 把主队列的元素倒出来到辅助栈
            while(!stack1.isEmpty()){
                stack2.push(stack1.pop());
            }
            //把新元素入到队列
            stack1.push(x);
            //然后要把之前的元素压倒新元素的上面
            //就又要把旧元素倒腾过来
            while (!stack2.isEmpty()) {
                stack1.push(stack2.pop());
            }

        }
    }
    public int pop() {
        return  stack1.pop();

    }
    public int peek() {
        return stack1.peek();

    }
    public boolean empty() {
        return stack1.isEmpty();

    }

}
