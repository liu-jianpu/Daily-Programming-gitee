import java.util.Stack;

/**
 * Created by L.jp
 * Description:请你仅使用两个栈实现先入先出队列。队列应当支持一般队列支持的所有操作（push、pop、peek、empty）：
 *
 * 实现 MyQueue 类：
 *
 * void push(int x) 将元素 x 推到队列的末尾
 * int pop() 从队列的开头移除并返回元素
 * int peek() 返回队列开头的元素
 * boolean empty() 如果队列为空，返回 true ；否则，返回 false
 * 说明：
 *
 * 你 只能 使用标准的栈操作 —— 也就是只有push to top,peek/pop from top,size, is empty操作是合法的。
 * 你所使用的语言也许不支持栈。你可以使用 list 或者 deque（双端队列）来模拟一个栈，只要是标准的栈操作即可。

 * User: 86189
 * Date: 2022-02-14
 * Time: 22:56
 */
public class MyQueue {

    //这种操作，peek和pop的时间复杂度是O(N),其他的为O（1)
    //两个栈实现队列元素的排布实际就是，让一个栈负责元素的栈，一个栈负责出栈
    Stack<Integer> stack1=new Stack<>();//负责入栈
    Stack<Integer> stack2=new Stack<>();//负责出栈
    public MyQueue() {

    }

    public void push(int x) {
        stack1.push(x);
    }

    public int pop() {
        //弹出的时候需要把入在stack1的元素倒过来放到stack2中就实现了队列的排布，然后直接pop就可以
        //首先弹出元素的时候要判断出的栈stack2是否为空，为空的话就把入栈的元素出栈的stack2中
        if(stack2.isEmpty()) {
            while (!stack1.isEmpty()) {
                stack2.push(stack1.pop());
            }
        }
        //入的栈弹的为空了，那么就删除并返回stack2中的栈顶元素
        return  stack2.pop();

    }

    public int peek() {
        //弹出的时候需要把入在stack1的元素倒过来放到stack2中就实现了队列的排布，然后直接pop就可以
        //首先弹出元素的时候要判断出的栈stack2是否为空，为空的话就把入栈的元素出栈的stack2中
        if(stack2.isEmpty()) {
            while (!stack1.isEmpty()) {
                stack2.push(stack1.pop());
            }
        }
        //入的栈弹的为空了，那么就返回stack2中的栈顶元素
        return  stack2.peek();
    }

    public boolean empty() {
        return  stack1.isEmpty() && stack2.isEmpty();

    }

}
