import java.util.Stack;

/**
 * Created by L.jp
 * Description:定义栈的数据结构，请在该类型中实现一个能够得到栈中所含最小元素的 min 函数，输入操作时保证 pop、top 和 min 函数操作时，栈中一定有元素。
 *
 * 此栈包含的方法有：
 * push(value):将value压入栈中
 * pop():弹出栈顶元素
 * top():获取栈顶元素
 * min():获取栈中最小元素
 *
 * User: 86189
 * Date: 2021-12-24
 * Time: 23:05
 */
//题目意思就是实现一个栈的结构，里面要包含求的栈里最小元素的函数
    //那么最先我们可以想到在栈内定义min变量，然后通过比较不断根性min变量，但是这样不好找第二小的，第三小的
    //所以可以利用一个辅助栈来一直存放栈内的最小值，注意要使最小栈的元素始终和元素栈的元素个数保持一致，当然可能最小栈的元素一致是重复的
public class Solution {
    Stack<Integer> stack=new Stack<>();
    Stack<Integer> minStack=new Stack<>();
    public void push(int node) {
        //放入元素到元素栈
        stack.push(node);
        //实现一个栈的插入操作，是一个一个插入的，没必要用循环
        //如果最小栈为空,直接插入元素
        if(minStack.isEmpty()){
            minStack.push(node);
        }else{
            //如果最小栈不为空，那么原则只有一个，那就是始终保证栈顶元素是最小值，这样才方便操作
            //如果最小栈不为空，而且插入的值小于最小栈的栈顶元素，那么就把这个值插入最小栈
            if(node<minStack.peek()){
                minStack.push(node);
            }else{
                //如果这个值比最小栈的栈顶元素大，那么最小栈继续插入最小栈的栈顶元素
                minStack.push(minStack.peek());
            }
        }
    }
  //弹出元素
    public void pop() {
        stack.pop();
        minStack.pop();
    }
 //获取栈顶元素
    public int top() {
        return  stack.peek();
    }

    public int min() {
        return minStack.peek();
    }
}
