import java.util.Stack;

/**
 * Created by L.jp
 * Description:设计一个支持 push ，pop ，top 操作，并能在常数时间内检索到最小元素的栈。
 *
 * push(x) —— 将元素 x 推入栈中。
 * pop()—— 删除栈顶的元素。
 * top()—— 获取栈顶元素。
 * getMin() —— 检索栈中的最小元素。

 * User: 86189
 * Date: 2022-02-14
 * Time: 16:20
 */
/*
*       我们需要两个栈，一个数据栈存放所有元素，一个最小栈存放当前数据栈的最小元素
*       要保证数据栈在弹出的同时也能求得最小栈，那么出栈时需要两个栈一起出栈顶元素，这样才能保证
*       最小栈的元素能在数据栈里找到，使得两个栈能同步
*
* */
public class MinStack {
    private Stack<Integer> elements=new Stack<>();//数据栈
    private Stack<Integer> minStack=new Stack<>();//最小栈
    public MinStack() {

    }

    public void push(int val) {
        //数据栈正常压入，主要是对最小栈的操作
        //两个栈要同步压入
        elements.push(val);
        if(minStack.isEmpty()){
            minStack.push(val);
        }else{
            //入栈顶元素和当前元素的最小值
           minStack.push(Math.min(minStack.peek(),val));
        }
    }

    public void pop() {
        //弹出也要一起弹出，保证同步最小值
        elements.pop();
        minStack.pop();

    }

    public int top() {
        //数据栈的栈顶元素
        return elements.peek();

    }

    public int getMin() {
        //最小栈栈顶就是最下元素
        return minStack.peek();
    }
}
