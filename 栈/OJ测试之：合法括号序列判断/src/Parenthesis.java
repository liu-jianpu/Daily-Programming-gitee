import java.util.ArrayDeque;
import java.util.Deque;
import java.util.Stack;

/**
 * Created by L.jp
 * Description:给定一个字符串A和其长度n，请返回一个bool值代表它是否为一个合法的括号串（只能由括号组成）。
 * User: 86189
 * Date: 2022-03-01
 * Time: 16:01
 */
public class Parenthesis {
    public static boolean chkParenthesis(String A, int n) {
        if(n%2==1){
            return false;
        }
        Deque<Character> que=new ArrayDeque<>();
        char[] s=A.toCharArray();
        for (char c : s) {
            if (c == '(') {
                que.push(c);
            } else if (!que.isEmpty() && c == ')') {
                que.pop();
            } else {
                return false;
            }
        }
        return que.isEmpty();
    }

    public static void main(String[] args) {
        String a="a";
        int n=6;
        System.out.println(chkParenthesis(a, n));
    }
}
