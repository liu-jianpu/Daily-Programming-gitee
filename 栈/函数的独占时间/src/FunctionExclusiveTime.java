import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Stack;

/**
 * Created by L.jp
 * Description:
 * User: 86189
 * Date: 2021-10-31
 * Time: 11:02
 */
public class FunctionExclusiveTime {

    public static int[] exclusiveTime(int n, List<String> logs){
        Stack<Integer> stack=new Stack<>();
        int[] arr=new int[n];//存放函数的独占时间
        String[] str=logs.get(0).split(":");//存放从logs链表得到的第一个字符串数组，通过":"分割成每个数组三个元素
        int prev= Integer.parseInt(str[2]);//记录上一个函数的时间戳，这里先记录第一个的
        stack.push(Integer.parseInt(str[0]));//把第一个函数的id入栈
        int i=1;//让链表从第二个元素开始遍历
        while(i<logs.size()){
            str=logs.get(i).split(":");//先拿到每个字符串数组
            if(str[1].equals("start")){//遇到start时，说明有下一个函数要开始运行了
                if(!stack.isEmpty()) {
                    //先把上一个函数在这个函数进来之前的运行时间计算
                    arr[stack.peek()] += Integer.parseInt(str[2]) - prev;//当两个start相遇时，直接用当前的运行时间减去上一个函数的时间
                }
                //再把这个这个函数入栈
                stack.push(Integer.parseInt(str[0]));
                //让前驱指向这个函数的时间戳，以便下一次计算这个函数的时间
                prev=Integer.parseInt(str[2]);
            }else{
                //遇到end,函数即将弹出栈，先计算这个函数或者上一个函数的运行时间
                arr[stack.peek()]+=Integer.parseInt(str[2])-prev+1;
                stack.pop();//弹出
                //此时下一个函数开始运行的时间就是本函数的是结束时间+1
                prev=Integer.parseInt(str[2])+1;
            }
            i++;
        }
        return arr;
    }
    public static void main(String[] args) {
       List<String> logs=new ArrayList<>();
       String str1="0:start:0";
       String str2="1:start:2";
       String str3="1:end:5";
       String str4="2:start:6";
       String str5="2:end:9";
       String str6="0:end:12";
       logs.add(str1); logs.add(str2); logs.add(str3); logs.add(str4); logs.add(str5); logs.add(str6);
       System.out.println(Arrays.toString(exclusiveTime(3, logs)));
    }
}
