import java.util.Arrays;
import java.util.PriorityQueue;
import java.util.Scanner;

/**
 * Created by L.jp
 * Description:
 * User: 86189
 * Date: 2022-04-26
 * Time: 14:49
 */
public class Main2 {
     /*
     ②：用数组存储n个桌子，对数组升序排序，使用堆存储客人类，继承comparable接口，在这个类里存储了客人的人数和客人的预计消费金额
    使用compareTo方法对客人的预计消费金额进行降序排序，
    在输入m批客人的时候，我们只将客人的人数小于等于桌子数组最大容纳人数的客人类加入堆，因为超过最大容纳人数的客人安排不了座位
    这样既优化了时间复杂度也实现了降序排序。
    定义一个被安排的数组，长度是n,防止桌子被重复安排，定义一个计数器，只要客人被安排一批，最大值就累加一次的同时计数器就+1
    然后就遍历桌子数组开始分配座位，依次弹出堆元素，如果堆元素的人数属性小于等于桌子容纳人数并且这个桌子没有被安排，那么就计入最大值
    桌子计数器+1，设置这个桌子为安排过，退出循环，看被安排的桌子数是否等于桌子数组的个数，接着再遍历下一批客人，也就是继续退出堆元素
    重复以上判断步骤，直到计数器累加为n，返回最大金额。
    */
    public static void main(String[] args) {
        //法二：
        Scanner scan = new Scanner(System.in);
        while(scan.hasNext()){
            //输入桌子数和客人批数
            int n=scan.nextInt();
            int m=scan.nextInt();
            
            int[] disks=new int[n];
            for(int i=0;i<n;i++){
                disks[i] = scan.nextInt();
            }
            Arrays.sort(disks);
            //定义一个被安排的数组，防止重复安排
            boolean[] visited=new boolean[n];
            int count=0; //对安排的桌子个数进行计数
            long max=0L;
            //输入每一批客人，在输入的时候，把每一批客人人数小于等于桌子容纳最大值的客人对象入堆，
            // 客人对象实现了Comparable接口的compareTo方法，对客人对象的预计消费金额进行排序
            PriorityQueue<Customer> priorityQue=new PriorityQueue<>();
            for(int i=0;i<m;i++){
                int people=scan.nextInt();
                int price=scan.nextInt();
                //优先级队列里只存储桌子能容纳的i批客人
                if(people<=disks[n-1]){
                    priorityQue.add(new Customer(people,price));
                }
            }
            //给客人分配桌子
            while(!priorityQue.isEmpty()){
                //弹出堆里的客人对象
                Customer cur=priorityQue.poll();
                //遍历每个桌子
                for(int i=0;i<n;i++){
                    //如果客人的人数小于桌子的容纳人数，并且没有被访问过，那么就计数，并把桌子标记为已经安排过
                    if(cur.people<=disks[i] && !visited[i]){
                        max+=cur.price;
                        count++;
                        visited[i] = true;
                        break; //安排了一批客人就安排下一批客人
                    }
                }
                if(count==n){
                    break; //如果桌子被安排完了，那么其他的客人就不同再从堆里弹出了，已经没有机会了
                }
            }
            System.out.println(max);
        }
}
static class Customer implements Comparable<Customer>{
        private final int people;
        private final int price;
        public Customer(int people, int price) {
            this.people = people;
            this.price = price;
        }
        @Override
        public int compareTo(Customer o) {
            return o.price-this.price;  //降序
        }
    }
}
