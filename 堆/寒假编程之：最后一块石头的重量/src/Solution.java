import java.util.Comparator;
import java.util.PriorityQueue;
import java.util.Queue;

/**
 * Created by L.jp
 * Description:有一堆石头，每块石头的重量都是正整数。
 *
 * 每一回合，从中选出两块 最重的 石头，然后将它们一起粉碎。假设石头的重量分别为x 和y，且x <= y。那么粉碎的可能结果如下：
 *
 * 如果x == y，那么两块石头都会被完全粉碎；
 * 如果x != y，那么重量为x的石头将会完全粉碎，而重量为y的石头新重量为y-x。
 * 最后，最多只会剩下一块石头。返回此石头的重量。如果没有石头剩下，就返回 0。

 * User: 86189
 * Date: 2022-02-10
 * Time: 20:54
 */
public class Solution {
    public static int lastStoneWeight(int[] stones){
        //建立大根堆
        PriorityQueue<Integer> priorityQue=new PriorityQueue<>(new Comparator<Integer>() {
            @Override
            public int compare(Integer o1, Integer o2) {
                return o2-o1;
            }
        });
        for(int num:stones){
            priorityQue.offer(num);
        }
        while(priorityQue.size()>1){
            int a=priorityQue.poll();
            int b=priorityQue.poll();
            if(a!=b){
                //如果a！=b,那么加入大根堆的就是他们的差值
                priorityQue.offer(Math.abs(a-b));
            }
            //a==b时，a和b都会被粉碎，大根堆不加入任何元素
        }
        //当大根堆只剩一个元素时，返回这个元素
        return priorityQue.size()==0 ? 0 : priorityQue.poll();
    }

    public static void main(String[] args) {
        int[] stones={2,7,4,1,8,1};
        System.out.println(lastStoneWeight(stones));
    }

}
