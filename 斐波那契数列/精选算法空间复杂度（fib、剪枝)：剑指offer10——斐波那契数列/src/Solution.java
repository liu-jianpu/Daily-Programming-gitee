import java.util.HashMap;
import java.util.Map;

/**
 * Created by L.jp
 * Description:
 * User: 86189
 * Date: 2021-12-17
 * Time: 16:55
 */
public class Solution {
    public static int Fibonacci(int n) {
        //迭代算法（动归算法）
        /*
        int f1=1;
        int f2=1;
        int f3=-1;
        if(n==1 || n==2){
            return f1;
        }
        while(n>2){//除了前两项之外，计算到n项需要计算的次数
            f3=f1+f2;
            f1=f2;
            f2=f3;
            n--;
        }
        return f3;

         */
        //递归算法(这样会有许多重复的值计算，消耗大量时间，效率变慢）
        /*
        if(n==0){
            return 0;
        }
        if(n==1 || n==2){
            return 1;
        }
        return Fibonacci(n-1)+Fibonacci(n-2);

         */
        //递归+map剪枝，对于前一项和前两项，我们可以先计算出来，然后再放到map中，以后计算的时候直接取就行了
        Map<Integer,Integer> prune=new HashMap<>();//存放第n个和第n个所对应的斐波那契值
        if(n==0 || n==1){
            return n;
        }
        int pre=0;//前一项值
        if(prune.containsKey(n-1)){//如果剪枝哈希表中包含前一项，那么直接取出前一项所对应的哈希值
            pre=prune.get(n-1);
        }else{
            pre=Fibonacci(n-1);//如果不包含前一项，那么就把前一项和其所对应的哈希值放到哈希表中
            prune.put(n-1,pre);
        }
        //前两项值
        int ppre=0;
        if(prune.containsKey(n-2)){
            ppre=prune.get(n-2);
        }else{
            ppre=Fibonacci(n-2);
            prune.put(n-2,ppre);
        }
        return pre+ppre;
    }
    public static void main(String[] args) {
        System.out.println(Fibonacci(4));
    }
}
