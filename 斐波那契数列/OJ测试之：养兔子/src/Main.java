import java.util.Scanner;

/**
 * Created by L.jp
 * Description:一只成熟的兔子每天能产下一胎兔子。每只小兔子的成熟期是一天。 某人领养了一只小兔子，请问第N天以后，他将会得到多少只兔子。
 * User: 86189
 * Date: 2022-04-01
 * Time: 18:36
 */
public class Main {
    public static void main(String[] args) {
        //构造斐波那契数组
        long[] fb=new long[91];
        fb[0]=1;
        fb[1]=1;
        for(int i = 2; i < fb.length; i++){
            fb[i]=fb[i-1]+fb[i-2];
        }
        Scanner sc = new Scanner(System.in);
        while (sc.hasNext()) {
            int n=sc.nextInt();
            System.out.println(fb[n]);

        }

    }
}
