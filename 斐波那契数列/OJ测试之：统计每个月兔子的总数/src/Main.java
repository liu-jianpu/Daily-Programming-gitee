import java.util.Scanner;

/**
 * Created by L.jp
 * Description:有一种兔子，从出生后第3个月起每个月都生一只兔子，小兔子长到第三个月后每个月又生一只兔子。
 * 例子：假设一只兔子第3个月出生，那么它第5个月开始会每个月生一只兔子。
 * 假如兔子都不死，问第n个月的兔子总数为多少？
 * User: 86189
 * Date: 2022-03-12
 * Time: 15:03
 */
//根据题意：
    //第一个月共有1只，第二个月共有1只，第三个月生了一只，共有2只，第四个月老兔子又生一只，加上上个月生的兔子，共有3只
    //第五个月就新兔子可以生一只兔子，加上上两个月生下的兔子共有2只，加上老兔子又生一只，加上老兔子公有5只
    //由此发现了这是斐波那契数列规律
public class Main {
    public static void main(String[] args) {
        Scanner scanner=new Scanner(System.in);
       while (scanner.hasNextInt()) {
           int m=scanner.nextInt();//输入月数
           int m1=1;//第一个月
           int m2=1;//第二个月
           int sum=0;
           if(m==1 || m==2){
               System.out.println(1);
           }
           for(int i = 3; i <= m; i++){
               sum =m1+m2;
               m1 = m2 ;
               m2=sum;
           }
           System.out.println(sum);
       }
    }
}
