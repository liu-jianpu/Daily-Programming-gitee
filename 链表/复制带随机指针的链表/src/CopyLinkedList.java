import java.util.HashMap;
import java.util.Map;

/**
 * Created by L.jp
 * Description:
 * User: 86189
 * Date: 2021-10-28
 * Time: 11:19
 */
class Node {
    int val;
    Node next;
    Node random;

    public Node(int val) {
        this.val = val;
        this.next = null;
        this.random = null;
    }
}
public class CopyLinkedList {
    public Node copyRandomList(Node head) {
        Map<Node,Node> map=new HashMap<>();
        Node cur=head;
        while(cur!=null){
            Node node=new Node(cur.val);
            map.put(cur,node);//把老的链表节点的每个地址和新的链表的节点的地址构成map关系
            cur=cur.next;
        }
        cur=head;//让cur回到head,继续遍历老的节点，准备复制next域和random域
        while(cur!=null){
            map.get(cur).next=map.get(cur.next);
            map.get(cur).random=map.get(cur.random);
            cur=cur.next;
        }
        return map.get(head);
    }
}
