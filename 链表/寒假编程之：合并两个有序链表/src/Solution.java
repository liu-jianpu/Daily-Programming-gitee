/**
 * Created by L.jp
 * Description:将两个升序链表合并为一个新的 升序 链表并返回。新链表是通过拼接给定的两个链表的所有节点组成的。
 * User: 86189
 * Date: 2022-02-12
 * Time: 23:09
 */
class ListNode {
      int val;
      ListNode next;
      ListNode() {}
      ListNode(int val) { this.val = val; }
      ListNode(int val, ListNode next) { this.val = val; this.next = next; }
  }
public class Solution {
    public ListNode mergeTwoLists(ListNode list1, ListNode list2) {
        //需要返回头节点的题目，就需要创建一个新的虚拟节点,方便返回新的头节点
        ListNode resNode=new ListNode(0);
        ListNode cur=resNode;//遍历新链表
        while(list1!=null && list2!=null){
            //构造新的链表
            //比较大小
            if(list1.val <= list2.val){
                cur.next=list1;
                cur=cur.next;
                list1=list1.next;
            }else{
                cur.next=list2;
                cur=cur.next;
                list2=list2.next;
            }
        }
        //其中一个链表遍历完了，那么新的链表接到另一个链表得后面
        if(list1==null){
            cur.next=list2;
        }
        if(list2==null){
            cur.next = list1;
        }
        return resNode.next;

    }

}
