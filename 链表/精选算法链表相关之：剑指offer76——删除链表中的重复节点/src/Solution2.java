/**
 * Created by L.jp
 * Description:存在一个按升序排列的链表，给你这个链表的头节点 head ，请你删除所有重复的元素，使每个元素 只出现一次 。
 *
 * 返回同样按升序排列的结果链表。
 * User: 86189
 * Date: 2021-12-24
 * Time: 22:53
 */
//这个题目是删除重复的节点，不是删除全部重复的，每个元素只出现一次
    //思路：
//    指定 cur 指针指向头部 head
//            当 cur 和 cur.next 的存在为循环结束条件，当二者有一个不存在时说明链表没有去重复的必要了
//            当 cur.val 和 cur.next.val 相等时说明需要去重，则将 cur 的下一个指针指向下一个的下一个，这样就能达到去重复的效果
//            如果不相等则 cur 移动到下一个位置继续循环


 class ListNode1 {
      int val;
      ListNode1 next;
      ListNode1() {}
      ListNode1(int val) { this.val = val; }
      ListNode1(int val, ListNode1 next) { this.val = val; this.next = next; }
 }
public class Solution2 {
    public ListNode1 deleteDuplicates(ListNode1 head) {
        if(head==null){
            return head;
        }
        ListNode1 cur=head;
        while(cur!=null){
            if(cur.next!=null && cur.val==cur.next.val){
                cur.next=cur.next.next;//对于多个存在多个重复的值时，同样适用
            }else{
                cur=cur.next;
            }
        }
        return head;

    }

}
