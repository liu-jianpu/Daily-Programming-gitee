/**
 * Created by L.jp
 * Description:
 * User: 86189
 * Date: 2021-10-07
 * Time: 18:16
 */
class ListNode {
    int val;
    ListNode next = null;

    ListNode(int val) {
        this.val = val;
    }
}
public class Division {
    public ListNode partition(ListNode pHead, int x) {
        ListNode bs = null;
        ListNode be = null;
        ListNode as = null;
        ListNode ae = null;
        ListNode cur = pHead;
        while (cur != null){
            if (cur.val < x){
                if (bs == null){
                    bs = cur;
                    be = cur;
                }else {
                    be.next = cur;
                    be = be.next;
                }

            }else {
                if (as == null){
                    as = cur;
                    ae = cur;
                }else {
                    ae.next = cur;
                    ae = ae.next;
                }
            }
            cur = cur.next;
        }
        //1>判断bs是否为空  如果bs == null 返回as
        if (bs == null){
            return as;
        }
        //2>如果bs不为空  进行拼接
        be.next = as;
        //3> 如果ae不为空   ae的next值需要置为空
        if (ae != null){
            ae.next = null;
        }
        return bs;
    }
}

