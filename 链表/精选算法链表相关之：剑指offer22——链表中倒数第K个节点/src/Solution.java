/**
 * Created by L.jp
 * Description:输入一个链表，输出该链表中倒数第k个结点。
 * User: 86189
 * Date: 2021-12-18
 * Time: 22:24
 */
//求倒数第k个节点，就利用数学思想，先让快的走k步，然后再让快的和慢的一起走，最后当快的走到终点的时候慢的地方就是倒数第k个的地方
    //当链表长度大于k那么就有解，当k大于链表的长度的时候就没有解，返回空指针
 class ListNode {
    int val;
    ListNode next = null;

    ListNode(int val) {
        this.val = val;
    }
}
public class Solution {
     /*
      * @param head:
      * @param k:
      * [head, k]
      * @return   ListNode  需要找倒数第k个节点，那么需要满足链表的长度小于k值才有解
      * @description    TODO
      */
    public ListNode FindKthToTail(ListNode head,int k) {
        if (k == 0 || head == null) {
            return null;
        }
        ListNode fast = head;
        ListNode slow = head;
        //先让前面的指针走K步
        while (k > 0 && fast != null) {
            fast = fast.next;
            k--;
        }
        //或者：
        /*
        while(k>0){
            if(fast==null){
                return null;
            }
            fast=fast.next;
            k--;
        }

         */


        //之后前面指针和后面指针一起走
        while(fast!=null){
            fast=fast.next;
            slow=slow.next;
        }

        return k>0 ? null:slow;//这个地方，意思就是走完了一个链表但是k依旧大于0，所以就找不到第k个节点，否则k等于0的话，是肯定有解的，那就是slow
        //也就是当k大于链表的长度时，是找不到这个倒数第k个节点的，返回空指针
        //或者
        //return slow;

    }

    public static void main(String[] args) {

    }
}
