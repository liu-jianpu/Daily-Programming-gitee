/**
 * Created by L.jp
 * Description:
 * User: 86189
 * Date: 2021-10-07
 * Time: 18:02
 */
class ListNode {
    int val;
    ListNode next = null;

    ListNode(int val) {
        this.val = val;
    }
}
public class Palindrome {
    public boolean chkPalindrome(ListNode A) {
        ListNode fast = A;
        ListNode slow = A;
        if (A == null) {
            return false;
        }
        //找中间节点
        while (fast != null && fast.next != null) {
            slow = slow.next;
            fast = fast.next.next;
        }
        //反转
        ListNode cur = slow.next;
        while (cur != null) {
            ListNode curNext = cur.next;
            cur.next = slow;
            slow = cur;
            cur = curNext;
        }
        //判断是否相等，一个从前往后，一个从后往前，如果相遇，就是回文数
        while (A != slow) {
            //注意两个if的顺序
            if (A.val != slow.val) {
                return false;
            }
            //节点的个数是偶数
            if (A.next == slow) {
                return true;
            }
            A = A.next;
            slow = slow.next;
        }
        return true;
    }

}
