import java.util.Arrays;
import java.util.Comparator;

/**
 * Created by L.jp
 * Description:输入一个非负整数数组numbers，把数组里所有数字拼接起来排成一个数，打印能拼接出的所有数字中最小的一个。
 * 例如输入数组[3，32，321]，则打印出这三个数字能排成的最小数字为321323。
 * 1.输出结果可能非常大，所以你需要返回一个字符串而不是整数
 * 2.拼接起来的数字可能会有前导 0，最后结果不需要去掉前导 0
 * User: 86189
 * Date: 2022-01-13
 * Time: 23:20
 */
/*      1、先将整型数组转化为字符串型数组

        2、定义特定排序规则

        3、用定义的排序规则对字符串型数组进行排序

        4/将字符串型数组中每个元素拼接起来

        5、得到最小的数*/
public class Solution {
    public String PrintMinNumber(int [] numbers) {
        if(numbers.length == 0 || numbers==null){
            return "";
        }
        int n = numbers.length;
        //首先要转换成字符串数组才能完成数字之间的排序
        String[] intoStr=new String[n];
        for(int i=0;i<n;i++){
            intoStr[i]=numbers[i]+"";
        }
        //对字符串数组，定义排序规则，升序,每一个字符跟每一个字符之间比较
        Arrays.sort(intoStr,new Comparator<String>() {

            @Override
            public int compare(String o1, String o2) {
                return (o1+o2).compareTo(o2+o1);//这个比较规则是两个或多个字符组成的字符串的一个整体的比较，应该用o1拼接o2跟o2拼接o1比较，只有这两种情况

            }
        });
        //排完序后拼接
        StringBuilder s=new StringBuilder();
        for(String str:intoStr){
            s.append(str);
        }
        //返回字符串
        return s.toString();
    }
}
