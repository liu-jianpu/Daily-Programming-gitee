import java.util.ArrayList;
import java.util.Comparator;
import java.util.PriorityQueue;

/**
 * Created by L.jp
 * Description:
 * User: 86189
 * Date: 2022-04-11
 * Time: 10:25
 */
public class Main2 {
    public static ArrayList<Integer> GetLeastNumbers_Solution(int [] input, int k) {
        //要求无序就可以使用大根堆比较好
        if(k ==0){
            return new ArrayList<Integer>();
        }
        //默认小根堆，改为大根堆
        PriorityQueue<Integer> pq=new PriorityQueue<>(new Comparator<Integer>(){
           @Override
           public int compare(Integer o1,Integer o2){
               return o2-o1;
           }
        });
        for(int i = 0; i < input.length; i++){
            if(i<k){
                pq.add(input[i]);
            }else {
                if(input[i]<pq.peek()){
                    pq.poll();
                    pq.add(input[i]);
                }
            }
        }
        return new ArrayList<>(pq);
    }
    
    public static void main(String[] args) {
        int[] input={4,5,1,6,2,7,3,8};
        int k=4;
        System.out.println(GetLeastNumbers_Solution(input, k));
    }
}
