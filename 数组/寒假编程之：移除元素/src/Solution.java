/**
 * Created by L.jp
 * Description:给你一个数组 nums和一个值 val，你需要 原地 移除所有数值等于val的元素，并返回移除后数组的新长度。
 *
 * 不要使用额外的数组空间，你必须仅使用 O(1) 额外空间并 原地 修改输入数组。
 *
 * 元素的顺序可以改变。你不需要考虑数组中超出新长度后面的元素。
 *
 * User: 86189
 * Date: 2022-02-03
 * Time: 10:55
 */
/*思路：
*      其实和那个删除有序数组的重复元素差不多，也是利用双指针，一个slow区作为新数组的最后一个元素，一个fast扫描数组
*      遇到等于val的直接跳过，遇到不是val的就让nums[slow]=nums[fast]
*      */
public class Solution {
    public static int removeElement(int[] nums, int val) {
        if(nums.length==0){
            return 0;
        }
        int slow=0;//新数组当前的最后元素
        int fast=0;//扫描数组，直到找到不是val的元素
        while(fast<nums.length){
            if(nums[fast]!=val){
                nums[slow]=nums[fast];//赋值
                slow++;
            }
            fast++;
        }
        return slow;//返回新数组长度
    }

    public static void main(String[] args) {
        int[] nums={0,1,1,2,3,2,4,5};
        int val=2;
        System.out.println(removeElement(nums, val));

    }
}
