import java.util.Arrays;
import java.util.Scanner;

/**
 * Created by L.jp
 * Description:链接：https://www.nowcoder.com/questionTerminal/248ccf8b479c49a98790db17251e39bb
 * 来源：牛客网
 *
 * 牛牛举办了一次编程比赛,参加比赛的有3*n个选手,每个选手都有一个水平值a_i.现在要将这些选手进行组队,一共组成n个队伍,即每个队伍3人.牛牛发现队伍的水平值等于该队伍队员中第二高水平值。
 * 例如:
 * 一个队伍三个队员的水平值分别是3,3,3.那么队伍的水平值是3
 * 一个队伍三个队员的水平值分别是3,2,3.那么队伍的水平值是3
 * 一个队伍三个队员的水平值分别是1,5,2.那么队伍的水平值是2
 * 为了让比赛更有看点,牛牛想安排队伍使所有队伍的水平值总和最大。
 * 如样例所示:
 * 如果牛牛把6个队员划分到两个队伍
 * 如果方案为:
 * team1:{1,2,5}, team2:{5,5,8}, 这时候水平值总和为7.
 * 而如果方案为:
 * team1:{2,5,8}, team2:{1,5,5}, 这时候水平值总和为10.
 * 没有比总和为10更大的方案,所以输出10.
 * User: 86189
 * Date: 2022-01-17
 * Time: 11:17
 */

//        输入描述:
//        输入的第一行为一个正整数n(1 ≤ n ≤ 10^5)
//
//        第二行包括3*n个整数a_i(1 ≤ a_i ≤ 10^9),表示每个参赛选手的水平值.
//
//
//        输出描述:
//        输出一个整数表示所有队伍的水平值总和最大值
public class Main {
    public static void main(String[] args) {
        Scanner scan=new Scanner(System.in);
        while(scan.hasNext()){
            //输入队数
            int n=scan.nextInt();
            //定义一个数组，用于存储这些水平值
            long[] numbers=new long[3*n];
            //向数组中输入水平值
            for(int i=0;i<3*n;i++){
                numbers[i] = scan.nextLong();
            }
            //然后对数组升序排序
            Arrays.sort(numbers);
            //队伍的水平值就是队伍里第二个人的值
            //开始分队，怎么分队呢，就是让升序后的第一个元素和倒数第一个、倒数第二个为一队，然后再是第二个数跟倒数第三四个为一队，以此类推
            //分完队后每个队的中位数相加就是队伍的水平值之和
            //遍历每个队伍，发现分完队伍的每个队的中位数的下标跟在升序排序的数组的下标的规则是：9-2*i,i表示第几队
            long sum=0;//记录水平值之和
            for(int i=1;i<=n;i++){
                sum+=numbers[numbers.length-2*i];//可以试着自己找找规律
            }
            System.out.println(sum);
        }

    }
}
