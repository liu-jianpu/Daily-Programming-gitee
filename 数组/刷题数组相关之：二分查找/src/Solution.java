/**
 * Created by L.jp
 * Description:给定一个n个元素有序的（升序）整型数组nums 和一个目标值target
 * 写一个函数搜索nums中的 target，如果目标值存在返回下标，否则返回 -1。
 * User: 86189
 * Date: 2022-02-25
 * Time: 19:14
 */
//思路：
    //本题采用二分查找的思想，不断利用中间的值和目标值比较，缩小范围，能找到就返回下标，
    //找不到就返回-1
public class Solution {
    public static int search(int[] nums, int target) {
        int left=0;
        int right=nums.length;
        while(left<right){
            int  mid=left+(right-left)/2;
            if(nums[mid]<target){
                left=mid+1;
            }else if(nums[mid]>target){
                right=mid;
            }else{
                return mid;
            }
        }
        return -1;
    }

    public static void main(String[] args) {
        int[] nums = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14};
        int target=10;
        System.out.println(search(nums, target));
    }
}
