/**
 * Created by L.jp
 * Description:在一个二维数组array中（每个一维数组的长度相同），每一行都按照从左到右递增的顺序排序，每一列都按照从上到下递增的顺序排序。请完成一个函数，输入这样的一个二维数组和一个整数，判断数组中是否含有该整数。
 * [
 * [1,2,8,9],
 * [2,4,9,12],
 * [4,7,10,13],
 * [6,8,11,15]
 * ]
 * 给定 target = 7，返回 true。
 * 给定 target = 3，返回 false。
 * User: 86189
 * Date: 2021-12-13
 * Time: 11:17
 */
public class Solution {
    public static  boolean Find(int target, int [][] array) {
        if(array==null){
            return false;
        }
        //要使效率变高，就可以利用这个数组的特性，从右上角哪个元素开始找或者从左下角哪个元素开始找
        int i=0;
        int j=array[0].length-1;
        while(i<array.length && j>=0){
            if(target>array[i][j]){
                i++;
            }else if(target<array[i][j]){
                j--;
            }else{
                return true;
            }
        }
        return false;
    }

    public static void main(String[] args) {
        int[][] array={{1,2,3,4},
                       {2,3,4,5},
                       {3,4,5,6},
                       {4,5,6,7}};
        int target=7;
        System.out.println(Find(target, array));
    }
}
