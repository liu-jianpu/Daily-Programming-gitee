import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * Created by L.jp
 * Description:
 * 所谓因子分解，就是把给定的正整数a，分解成若干个素数的乘积，即 a = a1 × a2 × a3 × ... × an,
 * 并且 1 < a1 ≤ a2 ≤ a3 ≤ ... ≤ an。其中a1、a2、...、an均为素数。 先给出一个整数a，请输出分解后的因子。
 *
 * 输入描述:
 * 输入包含多组数据，每组数据包含一个正整数a（2≤a≤1000000）。
 *
 *
 * 输出描述:
 * 对应每组数据，以“a = a1 * a2 * a3...”的形式输出因式分解后的结果。
 * 示例1
 * 输入
 * 10<br/>18
 * 输出
 * 10 = 2 * 5<br/>18 = 2 * 3 * 3
 * User: 86189
 * Date: 2022-03-29
 * Time: 18:39
 */
public class Main {
        public static List<String> fun(int  n){
            List<String> list=new ArrayList<>();
            //求因子，类似于求素数的过程
            //就是小学时求因数的短除法，只不过除数一直都是素数，从2开始，如果2不行就从后面的开始
           for(int i=2;n>1 && i<=Math.sqrt(n); i++) {
               while (n % i == 0) {  //假如可以被2整除，那么就不能被2的其他倍数整除，因为不是素数，能被3整除，就不能被3的其他倍数整除，一样的道理
                   list.add(String.valueOf(i));  //转换为字符串
                   //然后变换除数
                   n /= i;
               }
               //这个是不行的，因为要求是素数相乘，但是1不是素数，所以n必须大于1
//               if (n == 1) {
//                   list.add(String.valueOf(n));
//               }
           }
           //这里说明不能被整除了，如果这个除数也就是n还是大于1的，那么就也加入到列表
            if(n>1){
                list.add(String.valueOf(n));
            }
            return list;
        }
        public static void main(String[] args){
            Scanner scan=new Scanner(System.in);
            while(scan.hasNextInt()){
                int n=scan.nextInt();
                List<String> list=fun(n);
                //String.join方法，其实就是相当于一个拼接方法，把一个迭代容器里的元素按照指定格式连接起来
                System.out.printf("%d=%s",n,String.join("*",list));
            }
        }
    }

