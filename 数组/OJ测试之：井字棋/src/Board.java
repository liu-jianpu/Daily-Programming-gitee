/**
 * Created by L.jp
 * Description:给定一个二维数组board，代表棋盘，
 * 其中元素为1的代表是当前玩家的棋子，0表示没有棋子，-1代表是对方玩家的棋子。
 * 当一方棋子在横竖斜方向上有连成排的及获胜（及井字棋规则），返回当前玩家是否胜出。
 * User: 86189
 * Date: 2022-03-21
 * Time: 19:24
 */
public class Board {
    //通过判断行，列，两条对角线上的元素和是否
    public static boolean checkWon(int[][] board) {
        int n=board.length;
        int i = 0;
        int j=0;
        //判断行
        int sum = 0;
        for(; i < n; i++){
             sum=0;
            for(;j < n; j++){
                sum+=board[i][j];
            }
            if(sum==3){
                return true;
            }
        }
        //判断列
        for(;i<n;i++){
             sum=0;
            for(;j<n;j++){
                sum+=board[j][i];
            }
            if(sum==3){
                return true;
            }
        }
        //判断主对角线
        sum =0;
        for(;i < n; i++) {
            sum += board[i][i];
        }
        if(sum==3){
            return true;
        }
        //判断副对角线
        sum=0;
        for(;i<n;i++) {
            sum += board[i][n - 1 - i];
        }
        if(sum==3){
            return true;
        }
        return false;
    }

    public static void main(String[] args) {
        int[][]board={{1,1,0},{-1,1,0},{1,-1,1}};
        System.out.println(checkWon(board));
    }
}
