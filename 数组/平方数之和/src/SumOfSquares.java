/**
 * Created by L.jp
 * Description:
 * User: 86189
 * Date: 2021-10-31
 * Time: 10:58
 */
public class SumOfSquares {
    public static boolean judgeSquareSum(int c) {
        //法一：就算c不是数组，也不用产生随机数来构建0到c的数组，直接利用fou循环枚举a,b的情况来解题
        //要使a^2+b^2等于c,那么a和b的取值范围最大就是c的开平方，我们可以在枚举a的同时来检查符合b的情况
        /*
        for(long a=0;a<=Math.sqrt(c);a++){
          long b=(long)Math.sqrt(c-a*a);
          if(((a*a)+(b*b))==c){
              return true;
          }
       }
       return false;
       */
        //法二：双指针法，让a=0,b=sqrt(c),让a++，b--;
        long a=0;
        long b=(int)Math.sqrt(c);//a,b最大值都是sqrt(c)
        while(a<=b){//就好像数组一样，a初始值为0，b初始值是sqrt(c)
            long cur=(a*a)+(b*b);
            if(cur==c){
                return true;
            }
            if(cur>c){
                b--;//因为b的初始值比a的大，当cur大于c时，首先让b--
            }
            if(cur<c){
                a++;//cur<c时，让a++
            }
        }
        return false;
    }

    public static void main(String[] args) {
        System.out.println(judgeSquareSum(222));
    }
}
