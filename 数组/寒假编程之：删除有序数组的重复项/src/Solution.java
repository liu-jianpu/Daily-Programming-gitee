/**
 * Created by L.jp
 * Description:给你一个有序数组 nums ，请你 原地 删除重复出现的元素，使每个元素 只出现一次 ，返回删除后数组的新长度。
 *
 * 不要使用额外的数组空间，你必须在 原地 修改输入数组 并在使用 O(1) 额外空间的条件下完成。
 * User: 86189
 * Date: 2022-02-02
 * Time: 21:16
 */
/*由于给定的数组nums 是有序的，因此对于任意 i<j，如果 nums[i]=nums[j]则对任意 i≤k≤j，必有
nums[i]=nums[k]=nums[j]，即相等的元素在数组中的下标一定是连续的。利用数组有序的特点，可以通过双指针的
方法删除重复元素。*/
public class Solution {
    public  static int removeDuplicates(int[] nums) {
        if(nums.length == 0){
            return 0;
        }
        //利用双指针的解法，重复的元素，下标相同，定义slow指针去指向新数组（不重复数组）当前的最后一个元素
        //相当于slow用于拼接一个新的数组一样，定义一个fast去扫描数组，当slow和fast元素相等时，直接跳过，fast++,
        //当[slow]!=[fast]时，说明找到了一个新的数组元素，让slow+1=fast;然后slow++，fast++,这样循环
        int slow = 0;
        int fast=1;
        while(fast < nums.length){
            //遇到不相等的，加入新数组
            if(nums[fast]!=nums[slow]){
                nums[slow+1]=nums[fast];//无论slow和fast之间有多少个重复的，遇到不相等的时候直接把fast的值赋值给slow+1的值；
                slow++;
            }
            //不管有没有遇到相等的，都让fast继续遍历
            fast++;
        }
        return slow+1;
    }

    public static void main(String[] args) {
        int[] num={0,1,1,2,3,4,4,5,5,6};
        System.out.println(removeDuplicates(num));
    }
}
