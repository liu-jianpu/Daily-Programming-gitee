import java.util.Scanner;

/**
 * Created by L.jp
 * Description:描述
 * 有 n 个学生站成一排，每个学生有一个能力值，牛牛想从这 n 个学生中按照顺序选取 k 名学生，要求相邻两个学生的位置编号的差不超过 d，使得这 k 个学生的能力值的乘积最大，你能返回最大的乘积吗？
 * 输入描述：
 * 每个输入包含 1 个测试用例。每个测试数据的第一行包含一个整数 n (1 <= n <= 50)，表示学生的个数，接下来的一行，包含 n 个整数，按顺序表示每个学生的能力值 ai（-50 <= ai <= 50）。接下来的一行包含两个整数，k 和 d (1 <= k <= 10, 1 <= d <= 50)。
 * 输出描述：
 * 输出一行表示最大的乘积。
 * User: 86189
 * Date: 2022-04-23
 * Time: 18:48
 */
public class Main {
   /*   这是一个动态规划问题，因为前一个的状态能被后一个用到，所以我们采用动态规划的思想来解决问题
        从n个学生中选择k个学生的最大乘积作为n个学生的最大乘积，从第一个学生开始，假设当前学生i是选取的第k个学生，那么以i结尾的乘积的最大值
        分为两种情况，我们的能力值可能是负数也可能是正数，所以最大值不一定都是正数累积的，最小值也不一定都是负数累积的，所以最大值还需要去负数累积最大值和正数累积最大值中取最大值
        ①：以i结尾的乘积的最大值就是maxVal[i][j]=最大值 {当前没有更新的最大值，前面第k-1个学生的最大值*当前i学生的能力值，前面第k-1个学生的最小值*当前i学生的能力值 }
        ②：以i结尾的乘积的最小值就是minVal[i][j]=最小值{当前没有更新的最小值，前面第k-1个学生的最大值*当前i学生的能力值，前面第k-1个学生的最小值*当前i学生的能力值 }
        
   * */
    public static  long getMax(int n,int[] arr,int k,int d){
        //定义一个存储最大值和最小值的数组
        //我们以n个学生的各个学生的取值为横坐标，选中的第k个学生为纵坐标
        long[][] maxVal=new long[n+1][k+1];  //表示从n个学生中选的k个学生的乘积的最大值
        long[][] minVal=new long[n+1][k+1];  //表示从n个学生中选的k个学生的乘积的最小值
        //定义一个最终值存储所有学生中乘积的最大值
        long ret=0L;
        //先从数组中初始化最大值和最小值
        for(int i = 1; i <=n; i++){
            //对于最大值和最小值数组我们不表示第0个学生和选择0个学生，最起码是1开始，我们的最大值和最小值数组多借助了一行一列
            maxVal[i][1]=minVal[i][1]=arr[i-1];
        }
        //构造数组
        //n个学生
        for(int i = 1; i <= n; i++){
            //选择的第k个学生
            for(int j = 1; j <= k; j++){
                //选择下一个k学生的时候是从当前i位置往前面找，下一个k学生
                //每一个选中的k的下标是有限制的，相邻的k之间范围不超过d,如果给的d远远超过了第1个学生的编号，那么就从1和i-d中选择一个最大值
                for(int m=i-1;m>=Math.max(1,i-d);m--){
                    //更新最大值和最小值
                    maxVal[i][j]=Math.max(maxVal[i][j],Math.max(maxVal[m][j-1]*arr[i-1],minVal[m][j-1]*arr[i-1]));
                    minVal[i][j] = Math.min(minVal[i][j],Math.min(maxVal[m][j-1] * arr[i-1],minVal[m][j-1] * arr[i-1]));
                }
            }
            //选择一个k之后，需要更新最大值,就是从i个学生中选择k个学生的最大乘积，就是最后一个值maxVal[i][k]
            ret=Math.max(ret,maxVal[i][k]);
        }
        return  ret;
    
    }
    
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n=sc.nextInt();
        int[] arr=new int[n];
        for(int i = 0; i < n; i++){
            arr[i] = sc.nextInt();
        }
        int k= sc.nextInt();
        int d=sc.nextInt();
        System.out.println(getMax(n, arr, k, d));
    }
}
