import java.util.Scanner;

/**
 * Created by L.jp
 * Description:洗牌在生活中十分常见，现在需要写一个程序模拟洗牌的过程。 现在需要洗2n张牌，从上到下依次是第1张，第2张，第3张一直到第2n张。
 * 首先，我们把这2n张牌分成两堆，左手拿着第1张到第n张（上半堆），右手拿着第n+1张到第2n张（下半堆）。
 * 接着就开始洗牌的过程，先放下右手的最后一张牌，再放下左手的最后一张牌，接着放下右手的倒数第二张牌，再放下左手的倒数第二张牌，直到最后放下左手的第一张牌。
 * 接着把牌合并起来就可以了。 例如有6张牌，最开始牌的序列是1,2,3,4,5,6。首先分成两组，左手拿着1,2,3；右手拿着4,5,6。
 * 在洗牌过程中按顺序放下了6,3,5,2,4,1。把这六张牌再次合成一组牌之后，我们按照从上往下的顺序看这组牌，就变成了序列1,4,2,5,3,6。 现在给出一个原始牌组，请输出这副牌洗牌k次之后从上往下的序列。
 * 输入描述：
 * 第一行一个数T(T ≤ 100)，表示数据组数。对于每组数据，第一行两个数n,k(1 ≤ n,k ≤ 100)，接下来有2n行个数a1,a2,...,a2n(1 ≤ ai ≤ 1000000000)。表示原始牌组从上到下的序列。
 * 输出描述：
 * 对于每组数据，输出一行，最终的序列。数字之间用空格隔开，不要在行末输出多余的空格。
 * User: 86189
 * Date: 2022-03-31
 * Time: 13:31
 */
//n是左手和右手的牌数，k是洗牌的次数
public class Main {
    //k次洗牌过程
    //将每只牌放入数组，下标从0开始，就是第一张牌，
    //根据洗牌的规则，再加上数组下标的对应关系，比如  左手索引  0    1     2
    //                                        左手牌    1    2     3
    //
    //                                        右手索引  3    4      5
    //                                        右手牌    4    5      6


    //1 2 3 4 5 6   洗完一次之后变成了 6   3    5    2   4    1   从上往下看就是1 4 2 5 3 6
    //所以左手牌的下标变化关系为  i---->  2*i       右手对应变化关系为  i+3(对应着n) ----->2i+1
    public  static void shuffleCards(int[] cards,int n,int k){

        //洗牌k次
        for(int i =0;i<k;i++){
            //每洗一次牌都有新的牌产生
            int[] newCards=new int[2*n];
            //交换左右手的牌
            for(int j=0;j<n;j++){
                //左手
                newCards[2*j] = cards[j];
                //右手
                newCards[2*j+1]=cards[j+n];
            }
            cards=newCards;
        }
        printCards(cards);
    }
    public static void printCards(int[] cards){
        for(int i = 0; i < cards.length-1; i++){
            System.out.print(cards[i]+" ");
        }
        System.out.println(cards[cards.length - 1]);
    }
    public static void main(String[] args) {
        Scanner scanner=new Scanner(System.in);
        int count=scanner.nextInt();
        for(int i = 0; i < count; i++){
            int n=scanner.nextInt();
            int k=scanner.nextInt();
            int[] cards=new int[2*n];
            for(int j = 0; j <2* n; j++){
                cards[j] = scanner.nextInt();
            }
            shuffleCards(cards,n,k);
        }
    }
}
