import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by L.jp
 * Description:给一个长度为 n 的数组，数组中有一个数字出现的次数超过数组长度的一半，请找出这个数字。
 * 要求：空间复杂度：O(1)，时间复杂度 O(n)
 * User: 86189
 * Date: 2021-12-14
 * Time: 21:32
 */
//例如输入一个长度为9的数组[1,2,3,2,2,2,5,4,2]。由于数字2在数组中出现了5次，超过数组长度的一半，因此输出2
    //对于这个题目有常见的三种写法：
    //哈希表记录数字出现的次数
    //排序法，然后找中间的那个数
    //Boyer-Moore（BM）投票算法,也是本题的最优解，如果我们把众数记为 +1，把其他数记为 −1，
    // 将它们全部加起来，显然和大于 0，从结果本身我们可以看出众数比其他数多。
public class Solution {
    public static int MoreThanHalfNum_Solution(int [] array) {
        //哈希表法
        /*
        Map<Integer,Integer> map=new HashMap<>();
        int count=0;
        for(int i=0;i<array.length;i++){
            if(!map.containsKey(array[i])){
                map.put(array[i],1);
            }else{
                count=map.get(array[i]);
                count++;
                map.put(array[i],count);
            }
            if(map.get(array[i])>array.length/2){
                return array[i];
            }
        }
        return 0;

         */

        //排序法
//        Arrays.sort(array);
//        return array[array.length/2];

        //B-M算法
        //利用不同减1，相同加1的方法，最后的结果一定是正数
        /*
        int count=0;//记录票数
        int target=0;//记录目标众数
        for (int num:array) {
            if(count==0){
                target=num;//如果票数为0，那么就假设当前数为众数
            }
            //然后需要更新当前数出现的次数，如果当前数和目标众数一样，那么count+1,否则-1
            count+=target==num ? +1 :-1;
        }
        return target;//最后遍历完数组，target一定是要找的目标众数

         */
        int target=array[0];
        int times=1;
        for(int i=1;i<array.length;i++){
            if(times==0){//说明不同的数都被抵消了，剩下的就是众数
                target=array[i];//次数为0就更新众数
                times=1;//次数置为1
            }else if(target==array[i]){
                times++;
            }else{
                times--;
            }
        }
        //因为数组中的众数超过数组的一半，利用不同就相互抵消的思想，最后target一定会停在众数哪里
        times=0;//遍历完数组，次数重置为0
        //验证众数的存在
        for(int i=0;i<array.length;i++){
            if(target==array[i]){
                times++;
            }
        }
        return  times>array.length/2 ? target : 0;//返回刚刚的目标值
    }

    public static void main(String[] args) {
        int[] array={1,2,3,2,1,2,2,2};
        System.out.println(MoreThanHalfNum_Solution(array));

    }
}
