import java.util.Arrays;

/**
 * Created by L.jp
 * Description:输入一个整数数组，实现一个函数来调整该数组中数字的顺序，使得所有的奇数位于数组的前半部分，所有的偶数位
 * 于数组的后半部分，并保证奇数和奇数，偶数和偶数之间的相对位置不变。
 * User: 86189
 * Date: 2021-12-14
 * Time: 18:57
 */
public class Solution {
    public static int[] reOrderArray(int [] array) {
        //当不要求奇偶位置时
        /*
        int i=0;
        int j=nums.length-1;
        while(i<j){
            while(i<j && (nums[i]%2)==1){
                i++;
            }
            while(i<j && (nums[j]%2)==0){
                j--;
            }
            if(i<j){
                int tmp=nums[i];
                nums[i]=nums[j];
                nums[j]=tmp;
            }
        }

        return nums;

         */
        //插入排序法
        for(int i=0;i<array.length;i++){
            for(int j=i;j>0;j--) {
                //目的是让奇数位于前半部分，偶数位于后半部分，所以如果当前数为奇数而前面为偶数，则调换顺序
                if (array[j] % 2 == 1 && array[j - 1] % 2 == 0) {
                    int tmp = array[j];
                    array[j] = array[j - 1];
                    array[j - 1] = tmp;
                }
            }
        }
        return array;
    }

    public static void main(String[] args) {
        int[] array={6,2,1,7,5};
        System.out.println(Arrays.toString(reOrderArray(array)));

    }
}
