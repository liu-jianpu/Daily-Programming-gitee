/**
 * Created by L.jp
 * Description:给定一个非负整数 num，反复将各个位上的数字相加，直到结果为一位数。返回这个结果。
 * User: 86189
 * Date: 2022-02-05
 * Time: 22:42
 */
public class Solution {
    public static int addDigits(int num) {
        //需要一个大的循环，保证能一直有各位相加的情况，知道各位相加后小于10就可以退出循环
        while(num>=10){
            //给定一个变量存储每一轮求各位相加后得到的结果
            int tmp=0;
            //这个循环是保证把num每一位都做了运算
            while(num!=0){
                tmp+=num%10;
                num/=10;
            }
            //当把num每一位都算完求出相加结果之后，又要开始新一轮的判断
            num=tmp;
        }
        return num;

    }

    public static void main(String[] args) {
        System.out.println(addDigits(38));
    }

}
