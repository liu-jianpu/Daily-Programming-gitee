/**
 * Created by L.jp
 * Description:给你一个整数 n ，请你判断 n 是否为 丑数 。如果是，返回 true ；否则，返回 false 。
 *
 * 丑数 就是只包含质因数2、3 或5的正整数。

 * User: 86189
 * Date: 2022-02-03
 * Time: 14:48
 */
public class Solution {
    public static boolean isUgly(int n) {
        //判断丑数只需要判断这个数能不能一直被2/3/5整除，如果可以的话最后就剩下1，那么就是丑数，如果不是的话，那么就不是丑数
        //可以采用递归和迭代两种方式
        //迭代
        /*if(n<=0) return false;
        while(n%2==0) n/=2;
        while(n%3==0) n/=3;
        while(n%5==0) n/=5;
        return n==1;*/

        //递归
        if(n<0) return  false;
        if(n==1) return true;
        if(n%2==0) {
           return isUgly(n/2);
        }else if(n%3==0) {
            return isUgly(n/3);
        }else if(n%5==0) {
           return isUgly(n/5);
        }
        return false;
    }

    public static void main(String[] args) {
        System.out.println(isUgly(8));
    }
}

