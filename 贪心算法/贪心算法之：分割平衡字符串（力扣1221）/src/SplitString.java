/**
 * Created by L.jp
 * Description:在一个 平衡字符串 中，'L' 和 'R' 字符的数量是相同的。
 *
 * 给你一个平衡字符串s，请你将它分割成尽可能多的平衡字符串。
 *
 * 注意：分割得到的每个字符串都必须是平衡字符串，且分割得到的平衡字符串是原平衡字符串的连续子串。
 *
 * 返回可以通过分割得到的平衡字符串的 最大数量 。

 * User: 86189
 * Date: 2021-11-19
 * Time: 16:33
 */

/*
1.什么是贪⼼
贪⼼的本质是选择每⼀阶段的局部最优，从⽽达到全局最优;
这么说有点抽象，来举⼀个例⼦：
例如，有⼀堆钞票，你可以拿⾛⼗张，如果想达到最⼤的⾦额，你要怎么拿？
指定每次拿最⼤的，最终结果就是拿⾛最⼤数额的钱。
2.贪⼼⼀般解题步骤
贪⼼算法⼀般分为如下四步：
将问题分解为若⼲个⼦问题
找出适合的贪⼼策略
求解每⼀个⼦问题的最优解
将局部最优解堆叠成全局最优解
 */

//这里对于平衡的定义就是R和L出现的个数是一样的，那么怎么去计算R和L的数量呢？其实不好分别计算他们的数量，但是可以把0作为平衡的标准
//也就是说我们可以让R=-1，L=1，遇到R就-1，遇到L就+1，只要计数器一为0，那么就说明是平衡字符串，我们可以0出现的个数来统计平衡字符串的个数
public class SplitString {
    public static int balancedStringSplit(String s) {
        int cnt=0;//用来计算的
        int ret=0;//用来统计0出现的次数，也就是平衡字符串的个数
        for(int i=0;i<s.length();i++){
            if(s.charAt(i)=='R'){
                cnt--;
            }else{
                cnt++;
            }
            if(cnt==0){
                ret++;
            }
        }
        return ret;
    }

    public static void main(String[] args) {

        String str="RLRRLLRLRL";
        System.out.println(balancedStringSplit(str));

    }
}
