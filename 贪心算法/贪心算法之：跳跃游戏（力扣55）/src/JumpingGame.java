/*
 * Created by L.jp
 * Description:给定一个非负整数数组 nums ，你最初位于数组的 第一个下标 。
 *
 * 数组中的每个元素代表你在该位置可以跳跃的最大长度。
 *
 * 判断你是否能够到达最后一个下标。
 * User: 86189
 * Date: 2021-11-19
 * Time: 17:33

 */
//问题：判断能否到达最后一个下标
//每个下标元素代表能跳跃的最大长度，那么问题就显然转换为最大跳跃长度能否覆盖到最后一个下标
//转化为子问题就是每一次的最大跳跃长度能否覆盖最后一个下标
public class JumpingGame {
    public static boolean canJump(int[] nums) {
        int maxpos=0;//最远可以到达的下标
        for(int i=0;i<nums.length;i++){
            //判断是否可以到达该下标
            if(i<=maxpos){
                //更新最远到达的下标也就是最大覆盖范围
                maxpos=Math.max(maxpos,nums[i]+i);
                if(maxpos>=nums.length-1){
                    return true;//如果覆盖范围等于或者超过最后一个下标那么就是可以到达的
                }
            }
        }
        return false;

    }

    public static void main(String[] args) {
        int [] nums={2,3,1,1,4};
        System.out.println(canJump(nums));
    }
}
