/**
 * Created by L.jp
 * Description:给定一个数组 prices ，其值prices[i] 是一支给定股票第 i 天的价格。
 *
 * 设计一个算法来计算你所能获取的最大利润。你可以尽可能地完成更多的交易（多次买卖一支股票）。
 *
 * 注意：你不能同时参与多笔交易（你必须在再次购买前出售掉之前的股票）。

 * User: 86189
 * Date: 2021-11-19
 * Time: 17:09
 */
//问题：计算你所能获取的最大利润，在这些天中的总共的最大利润
//    分解为子问题就是每一天的最大利润，只取正值就是每天的最大利润
//    每天的利润公式就是price[i]-price[i-1];

public class Shares {
    public static int maxProfit(int[] prices) {
        int maxprofit=0;
        for(int i=1;i<prices.length;i++){//i=1表示第二天开始计算利润，第一天没有利润，i=5是表示第六天的利润
            maxprofit=maxprofit+Math.max(prices[i]-prices[i-1],0);//只取正值，方法是让price[i]-price[i-1]跟0比较
        }
        return maxprofit;

    }

    public static void main(String[] args) {
        int[] prices={7,1,5,3,6,4};
        System.out.println(maxProfit(prices));
    }

}
