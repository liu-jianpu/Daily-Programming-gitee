import java.util.LinkedList;
import java.util.Queue;
import java.util.Scanner;

/**
 * Created by L.jp
 * Description:描述
 * NowCoder最喜欢游乐场的迷宫游戏，他和小伙伴们比赛谁先走出迷宫。
 * 现在把迷宫的地图给你，你能帮他算出最快走出迷宫需要多少步吗？
 * 输入描述：
 * 输入包含多组数据。
 *
 * 每组数据包含一个10*10，由“#”和“.”组成的迷宫。其中“#”代表墙；“.”代表通路。
 *
 * 入口在第一行第二列；出口在最后一行第九列。
 *
 * 从任意一个“.”点都能一步走到上下左右四个方向的“.”点。
 * 输出描述：
 * 对应每组数据，输出从入口到出口最短需要几步。
 * User: 86189
 * Date: 2022-04-17
 * Time: 18:20
 */
class  Position{
    int x;
    int y;
    int step;
    public Position(int x,int y,int step){
        this.x=x;
        this.y=y;
        this.step = step;
    }
    public Position(){
    }
}
public class Main{
//    public static void main(String[] args) {
//        Scanner scan=new Scanner(System.in);
//        while (scan.hasNext()){
//            //输入的十行数据
//            String[] map=new String[10];
//            for(int i = 0;i<10;i++){
//                //输入的每一行
//                map[i] = scan.nextLine();
//            }
//            System.out.println(bfs(map, 10, 10));
//        }
//    }
//    public static int bfs(String[] map,int row,int col){
//        //定义周围数组
//        int[][] arounds={{-1,0},{0,1},{1,0},{0,-1}};
//        //bfs模板就是采用队列进行操作
//        Queue<Position> queue=new LinkedList<>();
//        //定义一个走过路径的布尔数组
//        boolean[][] visited=new boolean[10][10];
//        //给出入口
//        Position enter=new Position(0,1,0);
//        //给出出口
//        Position exit=new Position(9,8,0);
//        //接下来就是bfs的模板
//        //先把入口放进队列
//        queue.offer(enter);
//        while(!queue.isEmpty()){
//            //先弹出元素
//            Position cur=queue.poll();
//            //标记为遍历过，因为能进入队列的都是可以走通的点
//            visited[cur.x][cur.y]=true;
//            //如果遍历过的点就是出口，那就返回
//            if(cur.x==exit.x && cur.y == exit.y){
//                return  cur.step;
//            }
//            //定义一个周围的节点
//            Position next=new Position();
//            //遍历弹出节点的四周
//            for(int i = 0; i <4;i++){
//                next.x=cur.x+arounds[i][0];
//                next.y=cur.y+arounds[i][1];
//                //如果坐标合法且不是障碍，且没有被遍历过
//                if( next.x>=0 &&  next.x<row && next.y>=0 && next.y<col && map[next.x].charAt(next.y)=='.' && !visited[next.x][next.y]){
//                    //那么就让步数+1
//                    next.step=cur.step+1;
//                    //加入这个走通的点
//                    queue.offer(new Position(next.x,next.y,next.step));
//                }
//            }
//        }
//        return 0;
//    }
    
    //第二种更好理解的方法
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        while (scan.hasNext()){
            char[][] map=new char[10][10];
            for(int i = 0; i <10;i++){
                String string=scan.nextLine();
                for(int j = 0; j < 10; j++){
                    map[i][j]=string.charAt(j);
                }
            }
            bfs(map);
        }
    }
    public  static  void bfs(char[][] map){
        //定义被访问数组
        boolean[][] visited=new boolean[map.length][map[0].length];
        //定义周围数组
        int[][] around={{-1,0},{0,1},{1,0},{0,-1}};
        //入口点
        Position enter=new Position(0,1,0);
        //出口点
        Position exit=new Position(9,8,0);
        Queue<Position> queue=new LinkedList<>();
        //将入口点加入队列，以后去遍历他的周围，加入可以通过的点
        queue.offer(enter);
        //标记入口点为已经访问过
        visited[enter.x][enter.y]=true;
        while (!queue.isEmpty()){
            //弹出队头节点
            Position cur=queue.poll();
            //如果找到了出口
            if(cur.x==exit.x && cur.y== exit.y){
                System.out.println(cur.step);
                break;
            }
            //然后遍历弹出的节点的周围四个节点
            for(int i = 0; i<4;i++){
                //构造出周围的节点，中心的节点也就是该节点到周围每一个节点步数都是1
                //不管周围的节点通不通，步数都是1，如果不通后面就不会被加入队列，后面不会被访问到，如果通了那么就会被加入队列
                Position next=new Position(cur.x+around[i][0],cur.y+around[i][1],cur.step+1);
                //判断周围节点是否合法，看看是否被访问过，看看是不是通的
                if(next.x>=0 && next.y>=0 && next.x<10 && next.y < 10 && map[next.x][next.y]=='.' && !visited[next.x][next.y]){
                    //是的话就可以入队，等着后面再遍历它的四个周围
                    queue.offer(next);
                    //标记为访问过
                    visited[next.x][next.y]=true;
                }
            }
        }
    }
}
