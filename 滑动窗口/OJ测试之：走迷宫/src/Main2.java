import java.util.LinkedList;
import java.util.Queue;
import java.util.Scanner;

/**
 * Created by L.jp
 * Description:链接：https://www.nowcoder.com/questionTerminal/e88b41dc6e764b2893bc4221777ffe64?f=discussion
 * 来源：牛客网
 *
 * 给定一个n\times mn×m的网格，在网格中每次在不超过边界的情况下可以选择向上、向下、向左、向右移动一格。网格中的一些格子上放置有障碍物，放有障碍物的格子不能到达。
 * 求从(x_s,y_s)到(x_t,y_t)的最少的移动次数。若不能到达，输出-1。
 * 示例1
 * 输入
 * 5 5
 * 1 1 5 5
 * .....
 * ****.
 * .....
 * **.**
 * .....
 * 输出
 * 12
 * User: 86189
 * Date: 2022-05-01
 * Time: 23:39
 */
class  Position1 {
    int x;
    int y;
    int step;
    
    public Position1(int x, int y, int step) {
        this.x = x;
        this.y = y;
        this.step = step;
    }
    
    public Position1() {
    }
}
    public class Main2 {
        public static void main(String[] args) {
            Scanner scan = new Scanner(System.in);
            while (scan.hasNext()) {
                int n = scan.nextInt();
                int m = scan.nextInt();
                //注意这里输入的坐标都是大于0的，但是在数组里面我们坐标都是默认从0开始，所以我们后面在计算坐标的时候都需要减1
                int sx = scan.nextInt();
                int sy = scan.nextInt();
                int ex = scan.nextInt();
                int ey = scan.nextInt();
                char[][] map = new char[n][m];
                for (int i = 0; i < n; i++) {
                    String string = scan.next();
                    //如果输入的一行不包含出口，那么就说明迷宫不通，返回-1
                    if(!string.contains(".")){
                        System.out.println(-1);
                        break;
                    }
                    for (int j = 0; j < m; j++) {
                        map[i][j] = string.charAt(j);
                    }
                }
                bfs(map, sx, sy, ex, ey, n, m);
            }
        }
    
        //bfs模板
        public static void bfs(char[][] map, int sx, int sy, int ex, int ey, int n, int m) {
            //如果出口是*,那么就出不去，返回-1
            if(map[ex-1][ey-1]=='*'){
                System.out.println(-1);
                return;
            }
            //定义被访问数组
            boolean[][] visited = new boolean[map.length][map[0].length];
            //定义周围数组
            int[][] around = {{-1, 0}, {0, 1}, {1, 0}, {0, -1}};
            //入口点
            Position1 enter = new Position1(sx-1, sy-1, 0);
            //出口点
            Position1 exit = new Position1(ex-1, ey-1, 0);
            Queue<Position1> queue = new LinkedList<>();
            //将入口点加入队列，以后去遍历他的周围，加入可以通过的点
            queue.offer(enter);
            //标记入口点为已经访问过
            visited[enter.x][enter.y] = true;
            while (!queue.isEmpty()) {
                //弹出队头节点
                Position1 cur = queue.poll();
                //如果找到了出口
                if (cur.x == exit.x && cur.y == exit.y) {
                    System.out.println(cur.step);
                    break;
                }
                //然后遍历弹出的节点的周围四个节点
                for (int i = 0; i < 4; i++) {
                    //构造出周围的节点，中心的节点也就是该节点到周围每一个节点步数都是1
                    //不管周围的节点通不通，步数都是1，如果不通后面就不会被加入队列，后面不会被访问到，如果通了那么就会被加入队列
                    Position1 next = new Position1(cur.x + around[i][0], cur.y + around[i][1], cur.step + 1);
                    //判断周围节点是否合法，看看是否被访问过，看看是不是通的
                    if (next.x >= 0 && next.y >= 0 && next.x < n && next.y < m && map[next.x][next.y] == '.' && !visited[next.x][next.y]) {
                        //是的话就可以入队，等着后面再遍历它的四个周围
                        queue.offer(next);
                        //标记为访问过
                        visited[next.x][next.y] = true;
                    }
                }
            }
        }
    }

