import java.util.Scanner;

/**
 * Created by L.jp
 * Description:有1分，2分，5分，10分四种硬币，每种硬币数量无限，给定n分钱(n <= 100000)，有多少中组合可以组成n分钱？
 *
 * 输入描述:
 * 输入整数n.(1<=n<=100000)
 *
 * 输出描述:
 * 输出组合数，答案对1e9+7取模。
 * User: 86189
 * Date: 2021-12-06
 * Time: 20:28
 */
public class Main {
    public static void main(String[] args) {
        Scanner scanner=new Scanner(System.in);
        int n=scanner.nextInt();
        int[] coins={1,2,5,10};
        int coinLen= coins.length;
        int[][] dp=new int[5][n+1];//前i个硬币中组成j的方法
        for(int i=0;i<=coinLen;i++){
            dp[i][0]=1;//状态初始化，前i张组成0的种数是1，那就是不用，也算一种
        }
        for(int i=1;i<=4;i++){
            for(int j= 1;j<=n;j++){
                if(j<coins[i-1]){
                    dp[i][j]=dp[i-1][j];//如果目标值j小于第i个硬币的面值，那么种数就是前i-1个硬币构成j的种数
                }else{
                    dp[i][j]=(dp[i-1][j]+dp[i][j-coins[i-1]])% 1000000007;//当目标值大于i的面值时，那么就要分情况，一个是不用第i个硬币的种数就是前i-1个硬币构成j的种数，
                                                            // dp[i][j - coins[i]) 表示当前硬币选了，那么还需要组成面额为 j - coins[i]的种数
                }
            }
        }
        System.out.println(dp[coinLen][n]);

    }
}