import java.util.Scanner;

/**
 * Created by L.jp
 * Description:判断一个无序数组中是否存在长度为3的递增子序列。（不要求连续）（满足O(n)的时间复杂度和O(1)的空间复杂度。）
 *
 * 输入描述:
 * 第一行一个正整数 1 <= n <= 100000
 *
 * 第二行n个整数a1,a2,...,an，(1<=ai<=1e9)
 *
 * 输出描述:
 * 如果存在，输出"true",否则输出"false"。（不含引号）。
 * User: 86189
 * Date: 2021-12-06
 * Time: 16:19
 */
/*
    思路详解
        核心想法：遍历一遍数组，希望遍历到的这个数three，前面已经有一个比他小的数two，再前面有一个比two小的数one。
        我们需要维护两个变量：one和two。代表递增子序列的第一个数和第二个数。
        假设我们已经有了这两个数，那么three的大小有以下三种情况：
        three大于two
        three介于two和one之间（three<=two && three>one）
        three小于one

        */
public class Main {
    public static void main(String[] args) {
        Scanner scan=new Scanner(System.in);
        int n=scan.nextInt();
        int[] num=new int[n];
        for(int i=0;i<n;i++){
            num[i]=scan.nextInt();
        }
        int first=num[0];//假设为递增序列的第一个数
        int second=Integer.MAX_VALUE;//先让递增序列的第三个数为整型最大值，这样才更方便计算
        for(int i=1;i<n;i++){//从第二个元素开始找符合条件的第三个数，但前提是前面的两个数都是符合递增的
            if(num[i]<first){
                first=num[i];//当找到一个更小的值时，需要更新他作为递增序列一个数
            }else if(num[i]>first && num[i]<second){
                second=num[i];//如果他比递增序列第一个数大，但是要比给定的第二个数小，那么他就可以作为递增序列的第二个数，需要更新一下
            }else if(num[i]>first && num[i]>second){
                System.out.println("true");//如果这个数比第一个数和第二个数都大，那么就找到了这个递增序列的第三个数，说明符合题目要求，返回true
                return;
            }
        }
        System.out.println("false");//其他的情况。返回false;
    }
}
