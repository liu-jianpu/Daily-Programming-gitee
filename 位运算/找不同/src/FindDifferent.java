import java.util.HashSet;
import java.util.Set;

/**
 * Created by L.jp
 * Description:
 * User: 86189
 * Date: 2021-11-05
 * Time: 22:21
 */
public class FindDifferent {
    public static char findTheDifference(String s, String t) {
       //法一：计数法，计算s和t中每个字符出现的次数
       /*
        int [] ret=new int[26];//统计每个字符出现的次数
       for(int i=0;i<s.length();i++){
           char ch=s.charAt(i);
           ret[ch-'a']++;//ch-'a'对应每个字符出现的下标
       }
        for(int i=0;i<t.length();i++){
            char ch=t.charAt(i);
            ret[ch-'a']--;//由于t中的字符只有一个与s中的字符不同，所以再把t中每个字符出现的次数放入数组
            if(ret[ch-'a']<0){
                return ch;//当数组的某个下标的元素为负数时，那么这个下标对应的元素就是多出的元素
            }
        }
        return ' ';

        */

        //法二：异或运算法：计算异或值，把所有的字符异或上ret,最终的结果不是0，而是多出的元素
        /*
        char ret=0;
        char[] str=s.concat(t).toCharArray();//使用拼接函数
        for(char ch:str){
            ret^=ch;
        }
        return ret;
        /*
        for(int i=0;i<s.length();i++){
            ret^=s.charAt(i);
        }
        for(int i=0;i<t.length();i++){
            ret^=t.charAt(i);
        }
        return ret;

         */
        //法三：ascii码值相减，最终得到的结果就是多出的那个元素
        /*
        int ret=0;
        int ret1=0;
        for(int i=0;i<s.length();i++){
            char ch=s.charAt(i);
            ret+=ch;
        }
        for(int i=0;i<t.length();i++){
            char ch=t.charAt(i);
            ret1+=ch;
        }
        return (char)(ret1-ret);

         */
        //法四：使用哈希表把两个字符串拼接起来放入set中,由于set中不能放重复的元素，所以set中最后放的就是多余的字符
        Set<Character> set=new HashSet<>();
        char[] ch=s.concat(t).toCharArray();
        for(char c:ch){
            if(set.contains(c)){
                set.remove(c);
            }else{
                set.add(c);
            }
        }
        return (char)set.toArray()[0];//set.toarray返回值是一个objet类型的数组，所以要转换成char类型
    }
    public static void main(String[] args) {
        String s="ae";
        String t="aea";
        System.out.println(findTheDifference(s, t));


    }
}
