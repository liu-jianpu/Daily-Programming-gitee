import javax.print.DocFlavor;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Set;

/**
 * Created by L.jp
 * Description:
 * User: 86189
 * Date: 2021-10-28
 * Time: 11:03
 */
public class Solution {
    public int singleNumber(int[] nums) {
        /*
        //位运算

        int num=0;
        for(int i:nums){
            num^=i;
        }
        return num;
        */
        //set去重方法
        HashSet<Integer> set = new HashSet<>();
        for (int val : nums) {
            if (set.contains(val)) {
                set.remove(val);
            }
            set.add(val);
        }
        for(int val:nums){
            if(set.contains(val)){
                return val;//是要返回数字而不是返回set,所以把val放进set后，还要回到set中去找到val,然后打印
            }
            return -1;
        }
    }
}
