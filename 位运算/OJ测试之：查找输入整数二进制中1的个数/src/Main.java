import java.util.Scanner;

/**
 * Created by L.jp
 * Description:输入一个正整数，计算它在二进制下的1的个数。
 * 注意多组输入输出！！！！！！
 * User: 86189
 * Date: 2022-03-10
 * Time: 17:30
 */
public class Main {
    private static int  getNum(int num){
        int ret=0;
        while(num!=0){
            num&=(num-1);
            ret++;
        }
        return ret;
    }
    public static void main(String[] args) {
        Scanner scan=new Scanner(System.in);
        while(scan.hasNext()){
            int n = scan.nextInt();
            System.out.println(getNum(n));
        }
    }
}
