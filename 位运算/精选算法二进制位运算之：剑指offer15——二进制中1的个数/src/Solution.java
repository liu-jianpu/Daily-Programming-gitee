/**
 * Created by L.jp
 * Description:
 * User: 86189
 * Date: 2021-12-18
 * Time: 21:34
 */
public class Solution {
    public static  int NumberOf1(int n) {
        //可以用n&n-1的做法，n&n-1=n-1，这样每&一次，就可以减少一个1，就让计数器+1
        int count=0;
        while(n!=0){
            n=n&n-1;
            count++;
        }
        return count;


    }

    public static void main(String[] args) {
        System.out.println(NumberOf1(-1));

    }
}
