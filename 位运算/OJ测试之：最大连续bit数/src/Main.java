import java.util.Queue;
import java.util.Scanner;

/**
 * Created by L.jp
 * Description:求一个int类型数字对应的二进制数字中1的最大连续数，例如3的二进制为00000011，最大连续2个1
 * User: 86189
 * Date: 2022-03-04
 * Time: 21:24
 */
public class Main {
    public static void main(String[] args) {
        Scanner scanner=new Scanner(System.in);
        while(scanner.hasNext()) {
            int count = 0;
            int maxCount = 0;
            int num = scanner.nextInt();
            while (num != 0) {
                if ((num & 1) == 1) {
                    count++;
                    maxCount = Math.max(count, maxCount); //记录最大连续值
                } else {
                    count = 0;//如果当前位是0，那么就重新寻找下一个1的位置，并把count置为0
                }
                num >>= 1;//右移判断下一个位置
            }
            System.out.println(maxCount);
        }
    }
}
