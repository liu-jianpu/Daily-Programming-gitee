/**
 * Created by L.jp
 * Description:写一个函数，求两个整数之和，要求在函数体内不得使用+、-、*、/四则运算符号。
 * User: 86189
 * Date: 2022-03-24
 * Time: 18:33
 */
public class Sloution {
    //这题和另类加法是一样的，当不需要进位时，两个数的二进制相异或就是加的结果，当两个数需要进位时，通过与运算得出哪位上有1，
    // 也就是需要进位的位置，再通过左移1位就完成了进位，我们通过不断的异或，与运算，直到进位的那位是0的时候就不用进位了，直接异或运算得出结果
    public static int Add(int num1,int num2) {
        int a=0;
        int b=0;
        while(num2!=0){
            a=num1^num2;
            b=(num1&num2)<<1;
            num1=a;
            num2=b;
        }
        
        return num1; //返回异或的结果
    }
    
    public static void main (String[] args) {
        int num1=3;
        int num2=6;
        System.out.println( Add( num1 , num2 ) );
    }
}
