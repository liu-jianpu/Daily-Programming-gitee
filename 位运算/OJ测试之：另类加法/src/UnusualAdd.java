/**
 * Created by L.jp
 * Description:给定两个int A和B。编写一个函数返回A+B的值，但不得使用+或其他算数运算符。

 * User: 86189
 * Date: 2022-03-02
 * Time: 20:04
 */
public class UnusualAdd {
    //另类加法要求不能使用’+‘号和其他运算符

    //本题考查的是位运算，这里运用了位运算的两个原理：
    // 1.二进制相异或的结果就是两个数相加的结果（不考虑进位）
    // 2.二进制相与后左移一位的结果就是两个数相加后进位的结果
    //3.两个数相加，如果不需要进位的话，那么这两个数异或的结果就是相加的结果

    //两个数相加，如果在二进制位上同时有1，那么就需要进位，那么就可以通过与运算找到这个进位的位置，与运算之后左移一位就完成了进位

    //方法：A+B=A^B+(A&B)<<1,当(A&B)<<1等于0时，就只需要计算两个数异或的结果，这个过程可以通过循环实现
    public static int addAB(int A, int B) {
         int sum=0;
         int tmp=0;
         while (B!=0){
             sum=A^B;
             tmp=(A&B)<<1;
             A=sum;
             B=tmp;
         }
         return A;
    }

    public static void main(String[] args) {
        System.out.println(addAB(17, 12));
    }

}
