/**
 * Created by L.jp
 * Description:给定两个32位整数n和m，同时给定i和j，
 * 将m的二进制数位插入到n的二进制的第j到第i位,保证n的第j到第i位均为零，
 * 且m的二进制位数小于等于i-j+1，其中二进制的位数从0开始由低到高。
 * User: 86189
 * Date: 2022-03-06
 * Time: 18:22
 */
public class BinInsert {
    //题目意思就是在n的j到i位插入m,这个n的j到i为一定都是0的，不是说插入之后的结果的j到i都是0
    public static int binInsert(int n, int m, int j, int i) {
        // write code here
        m<<=j;//m左移j位，对齐n的每一位，方便插入
        return m|n; //最后结果就是m和n的或运算

    }

    public static void main(String[] args) {
        int  n=1024;
        int m=19;
        int j=2;
        int i=6;
        System.out.println(binInsert(n, m, j, i));
    }
}
